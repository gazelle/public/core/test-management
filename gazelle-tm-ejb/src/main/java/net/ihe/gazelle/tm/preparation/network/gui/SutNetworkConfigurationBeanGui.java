package net.ihe.gazelle.tm.preparation.network.gui;

import net.ihe.gazelle.menu.Pages;
import net.ihe.gazelle.tm.preparation.network.SutNetworkConfigurationService;
import net.ihe.gazelle.tm.preparation.network.SutNetworkConfigurationSummary;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.io.Serializable;
import java.util.List;

@Name("sutNetworkConfigurationBeanGui")
@Scope(ScopeType.EVENT)
public class SutNetworkConfigurationBeanGui implements Serializable {

    private static final long serialVersionUID = -1667730152367201347L;
    @In(value = "sutNetworkConfigurationService")
    private transient SutNetworkConfigurationService sutNetworkConfigurationService;

    public List<SutNetworkConfigurationSummary> getSutNetworkConfigurationSummaries() {
        return sutNetworkConfigurationService.getSutNetworkConfigurationSummaries();
    }

    public String getApprovedStyle(SutNetworkConfigurationSummary sutNetworkConfigurationSummary) {
        double totalConfigurations = (sutNetworkConfigurationSummary.getNumberApproved() + sutNetworkConfigurationSummary.getNumberNotApproved());
        double percentageApproved = Math.round((sutNetworkConfigurationSummary.getNumberApproved() / totalConfigurations) * 100);
        return "width:" + percentageApproved + "%";
    }

    public String getNotApprovedStyle(SutNetworkConfigurationSummary sutNetworkConfigurationSummary) {
        double totalConfigurations = (sutNetworkConfigurationSummary.getNumberApproved() + sutNetworkConfigurationSummary.getNumberNotApproved());
        double percentageNotApproved = Math.round((sutNetworkConfigurationSummary.getNumberNotApproved() / totalConfigurations) * 100);
        return "width:" + percentageNotApproved + "%";
    }

    public boolean isThereConfiguration(SutNetworkConfigurationSummary sutNetworkConfigurationSummary){
        if (sutNetworkConfigurationSummary == null){
            return false;
        }
        return (sutNetworkConfigurationSummary.getNumberApproved() + sutNetworkConfigurationSummary.getNumberNotApproved())>0;
    }
    public int getNumberOfNetworkConfigurationsHosts() {
        return sutNetworkConfigurationService.getNumberOfNetworkConfigurationsHosts();
    }
    public int getNumberOfNetworkConfigurationsHostsNoIp(){
        return sutNetworkConfigurationService.getNumberOfNetworkConfigurationsHostsNoIp();
    }
    public String redirectToSUTNodesPage() {
        return Pages.CONFIG_LIST_VENDOR.getMenuLink();
    }
}
