package net.ihe.gazelle.tm.messages;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.tm.messages.model.Message;
import net.ihe.gazelle.tm.messages.model.MessageParameter;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.international.LocaleSelector;
import org.jboss.seam.international.StatusMessage;

import java.util.Locale;

@Name("messageFormatter")
@AutoCreate
@Scope(ScopeType.EVENT)
public class MessageFormatter {

   private final Locale userLocaleBackup;
   @In(value = "applicationPreferenceManager")
   private ApplicationPreferenceManager applicationPreferenceManager;

   public MessageFormatter() {
      userLocaleBackup = LocaleSelector.instance().getLocale();
   }

   /**
    * Format the type to be readable by humans
    *
    * @param message
    *
    * @return
    */
   public String getFormattedType(Message message) {
      return getFormatted(message, message.getType());
   }

   /**
    * Format the message in a phrase readable by humans
    *
    * @param message
    *
    * @return
    */
   public String getFormattedMessage(Message message) {
      return getFormatted(message, message.getType() + ".message");
   }

   private String getFormatted(Message message, String key) {
      String translated = StatusMessage.getBundleMessage(key, key);
      if ((message.getMessageParameters() != null) && (!message.getMessageParameters().isEmpty())) {
         for (int i = 0; i < message.getMessageParameters().size(); i++) {
            MessageParameter messageParameter = message.getMessageParameters().get(i);
            String stringMessageParameter = messageParameter.getMessageParameter();
            stringMessageParameter = StatusMessage.getBundleMessage(stringMessageParameter, stringMessageParameter);
            translated = StringUtils.replace(translated, "{" + i + "}", stringMessageParameter);
         }
      }
      return translated;
   }

   public String getPreferredLangFormattedMessage(Message message) {
      LocaleSelector.instance().setLocale(
            Locale.forLanguageTag(
                  applicationPreferenceManager.getStringValue("default_gazelle_language")
            )
      );
      String translated = getFormatted(message, message.getType() + ".message");
      LocaleSelector.instance().setLocale(userLocaleBackup);
      return translated;
   }
}
