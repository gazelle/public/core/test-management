package net.ihe.gazelle.tm.statistics.common.gui;

import net.ihe.gazelle.menu.Pages;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.gum.client.application.Role;
import net.ihe.gazelle.tm.gazelletest.model.instance.MonitorInSession;
import net.ihe.gazelle.tm.session.TestingSessionService;
import net.ihe.gazelle.tm.statistics.common.StatisticsCommonService;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.security.Identity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Scope(ScopeType.EVENT)
@AutoCreate
@Name("statisticsCommonBeanGui")
public class StatisticsCommonBeanGui {
    public static final String COLOR_GREY_STRING = "\"#C1C1C1\"";
    public static final String COLOR_GREEN_STRING = "\"#5cb85c\"";
    public static final String COLOR_BLUE_GAZELLE_STRING = "\"#4ca8de\"";
    public static final String COLOR_RED_STRING = "\"red\"";
    @In(value = "statisticsCommonService")
    StatisticsCommonService statisticsCommonService;

    @In
    private GazelleIdentity identity;

    @In(value = "testingSessionService")
    TestingSessionService testingSessionService;

    public List<Integer> getProfileInTestingSessionStats() {
        StatisticsCommonService.ProfileInTestingSessionStats profileInTestingSessionStats = statisticsCommonService.getProfileInTestingSessionStatsForCurrentSession();
        return Arrays.asList(
                profileInTestingSessionStats.getNbDecisionPending(),
                profileInTestingSessionStats.getNbTestable(),
                profileInTestingSessionStats.getNbFewPartners(),
                profileInTestingSessionStats.getNbDropped()
        );
    }

    public int getNumberOfIntegrationProfilesForCurrentSession() {
        return statisticsCommonService.getNumberOfIntegrationProfilesForCurrentSession();
    }

    public int getNumberOfDomainsForCurrentSession() {
        return statisticsCommonService.getNumberOfDomainsForCurrentSession();
    }

    public boolean isAdminOrTSM() {
        return identity.hasRole(Role.ADMIN) || Identity.instance().hasRole(Role.TESTING_SESSION_ADMIN);
    }
    public boolean isMonitor(){
        return identity.hasRole(Role.MONITOR) ;
    }
    public boolean isVendor(){
        return identity.hasRole(Role.VENDOR) || identity.hasRole(Role.VENDOR_ADMIN) ;
    }
    public boolean isMonitorOfCurrentSession() {
        return  identity.hasRole(Role.MONITOR)
                && (MonitorInSession.getActivatedMonitorInSessionForATestingSessionByUser(
                        testingSessionService.getUserTestingSession(), identity.getUsername()) != null);
    }

    public String getTestingSessionScopeLink() {
        return Pages.ADMIN_REGISTRATION_OVERVIEW.getMenuLink();
    }

    public List<String> getColorsForProfileInTestingSessionChart() {
        return new ArrayList<>(Arrays.asList(
                COLOR_GREY_STRING,
                COLOR_GREEN_STRING,
                COLOR_BLUE_GAZELLE_STRING,
                COLOR_RED_STRING
        ));
    }
}
