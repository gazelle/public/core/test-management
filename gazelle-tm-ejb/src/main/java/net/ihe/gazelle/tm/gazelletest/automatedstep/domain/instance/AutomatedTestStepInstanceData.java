package net.ihe.gazelle.tm.gazelletest.automatedstep.domain.instance;

import java.util.Objects;

public class AutomatedTestStepInstanceData {


    private Integer testStepInstanceInputId;
    private Integer testStepInstanceId;
    private String key;
    private String label;
    private String description;
    private boolean required;
    private Integer testStepDataId;
    private String fileName;
    private String userId;
    private String lastUpdate;

    public AutomatedTestStepInstanceData() {
    }

    public AutomatedTestStepInstanceData(Integer testStepInstanceInputId, Integer testStepInstanceId, String key, String label, String description, boolean required) {
        this.testStepInstanceInputId = testStepInstanceInputId;
        this.testStepInstanceId = testStepInstanceId;
        this.key = key;
        this.label = label;
        this.description = description;
        this.required = required;
    }

    public Integer getTestStepInstanceInputId() {
        return testStepInstanceInputId;
    }

    public void setTestStepInstanceInputId(Integer testStepInstanceInputId) {
        this.testStepInstanceInputId = testStepInstanceInputId;
    }

    public Integer getTestStepInstanceId() {
        return testStepInstanceId;
    }

    public void setTestStepInstanceId(Integer testStepInstanceId) {
        this.testStepInstanceId = testStepInstanceId;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public Integer getTestStepDataId() {
        return testStepDataId;
    }

    public void setTestStepDataId(Integer testStepDataId) {
        this.testStepDataId = testStepDataId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AutomatedTestStepInstanceData)) return false;
        AutomatedTestStepInstanceData that = (AutomatedTestStepInstanceData) o;
        return required == that.required
                && Objects.equals(testStepInstanceInputId, that.testStepInstanceInputId)
                && Objects.equals(testStepInstanceId, that.testStepInstanceId)
                && Objects.equals(key, that.key)
                && Objects.equals(label, that.label)
                && Objects.equals(description, that.description)
                && Objects.equals(testStepDataId, that.testStepDataId)
                && Objects.equals(fileName, that.fileName)
                && Objects.equals(userId, that.userId)
                && Objects.equals(lastUpdate, that.lastUpdate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(testStepInstanceInputId, testStepInstanceId, key, label, description, required, testStepDataId, fileName, userId, lastUpdate);
    }
}
