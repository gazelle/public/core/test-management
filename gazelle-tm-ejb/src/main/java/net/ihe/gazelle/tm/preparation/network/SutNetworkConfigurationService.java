package net.ihe.gazelle.tm.preparation.network;

import java.util.List;

public interface SutNetworkConfigurationService {

    /**
     * Gets sut network configuration summaries.
     * @see SutNetworkConfigurationSummary
     * @return the sut network configuration summaries
     */
    List<SutNetworkConfigurationSummary> getSutNetworkConfigurationSummaries();

    /**
     * Gets number of network configurations hosts.
     *
     * @return the number of network configurations hosts
     */
    int getNumberOfNetworkConfigurationsHosts();

    /**
     * Gets number of network configurations hosts no ip.
     *
     * @return the number of network configurations hosts no ip
     */
    int getNumberOfNetworkConfigurationsHostsNoIp();

}
