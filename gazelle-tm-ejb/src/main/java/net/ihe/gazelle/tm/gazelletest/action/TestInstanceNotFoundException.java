package net.ihe.gazelle.tm.gazelletest.action;

import java.io.Serializable;

public class TestInstanceNotFoundException extends Exception implements Serializable {

    public TestInstanceNotFoundException() {
    }

    public TestInstanceNotFoundException(String s) {
        super(s);
    }

    public TestInstanceNotFoundException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public TestInstanceNotFoundException(Throwable throwable) {
        super(throwable);
    }
}
