package net.ihe.gazelle.tm.gazelletest.action;

public class TestStepDataNotFoundException extends Exception {

    public TestStepDataNotFoundException() {
    }

    public TestStepDataNotFoundException(String s) {
        super(s);
    }

    public TestStepDataNotFoundException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public TestStepDataNotFoundException(Throwable throwable) {
        super(throwable);
    }

}
