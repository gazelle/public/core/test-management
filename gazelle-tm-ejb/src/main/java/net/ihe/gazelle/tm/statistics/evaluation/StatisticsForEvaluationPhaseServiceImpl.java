package net.ihe.gazelle.tm.statistics.evaluation;

import net.ihe.gazelle.tf.model.Domain;
import net.ihe.gazelle.tm.gazelletest.model.instance.StatusResults;
import net.ihe.gazelle.tm.gazelletest.model.instance.SystemAIPOResultForATestingSession;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.session.TestingSessionService;
import net.ihe.gazelle.tm.statistics.common.StatisticsCommonService;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.util.List;

@Scope(ScopeType.STATELESS)
@AutoCreate
@Name(value = "statisticsForEvaluationPhaseService")
public class StatisticsForEvaluationPhaseServiceImpl implements StatisticsForEvaluationPhaseService {

    @In(value = "statisticsForEvaluationPhaseDao")
    private StatisticsForEvaluationPhaseDao statisticsForEvaluationPhaseDao;

    @In(value = "statisticsCommonService")
    private StatisticsCommonService statisticsCommonService;

    @In(value = "testingSessionService")
    TestingSessionService testingSessionService;


    @Override
    public List<SystemInSession> getSystemsForCurrentUser() {
        return statisticsForEvaluationPhaseDao.getSystemsForCurrentUser(testingSessionService.getUserTestingSession());
    }

    @Override
    public List<TestInstance> getAllTestInstancesForCurrentSession() {
        return statisticsForEvaluationPhaseDao.getAllTestInstancesForCurrentSession();
    }

    @Override
    public StatisticsCommonService.TestInstanceStats getStatisticsForTestInstances(List<TestInstance> testInstances) {
        return statisticsCommonService.getStatisticsForTestInstances(testInstances);
    }

    @Override
    public List<SystemAIPOResultForATestingSession> getCapabilitiesForASystem(SystemInSession system) {
        return statisticsForEvaluationPhaseDao.getCapabilitiesForASystem(system);
    }

    @Override
    public CapabilityStats getCapabilitiesStatisticsForASystemInSession(SystemInSession systemInSession) {
        if (systemInSession != null) {
            List<SystemAIPOResultForATestingSession> capabilities = getCapabilitiesForASystem(systemInSession);
            return getCapabilityStats(capabilities);
        }
        return new CapabilityStats(0, 0, 0, 0, 0, 0);
    }

    private CapabilityStats getCapabilityStats(List<SystemAIPOResultForATestingSession> capabilities) {
        int nbPassed = 0;
        int nbDidNotComplete = 0;
        int nbAtRisk = 0;
        int nbNotEvaluated = 0;
        int nbEvaluationPending = 0;
        int nbNoPeer = 0;
        if (!capabilities.isEmpty()) {

            for (SystemAIPOResultForATestingSession capability : capabilities) {
                nbPassed = getNbPassed(nbPassed, capability);
                nbDidNotComplete = getNbDidNotComplete(nbDidNotComplete, capability);
                nbAtRisk = getNbAtRisk(nbAtRisk, capability);
                nbNotEvaluated = getNbNotEvaluated(nbNotEvaluated, capability);
                nbEvaluationPending = getNbEvaluationPending(nbEvaluationPending, capability);
                nbNoPeer = getNbNoPeer(nbNoPeer, capability);
            }
        }

        return new CapabilityStats(
                nbPassed, nbDidNotComplete, nbAtRisk, nbNotEvaluated, nbEvaluationPending, nbNoPeer);
    }

    private int getNbNoPeer(int nbNoPeer, SystemAIPOResultForATestingSession capability) {
        if (capability.getStatus() == StatusResults.getStatusByKeyword("nopeer")) {
            nbNoPeer++;
        }
        return nbNoPeer;
    }

    @Override
    public CapabilityStats getCapabilitiesStatisticsForADomain(Domain domain) {
        if (domain != null) {
            List<SystemAIPOResultForATestingSession> capabilities = statisticsForEvaluationPhaseDao.getAllCapabilitiesForADomain(
                    domain, testingSessionService.getUserTestingSession());
            return getCapabilityStats(capabilities);
        }
        return new CapabilityStats(0, 0, 0, 0, 0, 0);
    }

    @Override
    public int getNbTotalTestInstancesForCurrentSession() {
        return statisticsForEvaluationPhaseDao.getNbTotalTestInstancesForCurrentSession();
    }

    @Override
    public int getNbTotalCapabilitiesForADomain(Domain domain) {
        return statisticsForEvaluationPhaseDao.getNbTotalCapabilitiesForADomain(domain,
                testingSessionService.getUserTestingSession());
    }

    private int getNbEvaluationPending(int nbEvaluationPending, SystemAIPOResultForATestingSession capability) {
        if (capability.getStatus() == null) {
            nbEvaluationPending++;
        }
        return nbEvaluationPending;
    }

    private int getNbNotEvaluated(int nbNotEvaluated, SystemAIPOResultForATestingSession capability) {
        if (capability.getStatus() == StatusResults.getSTATUS_NO_GRADING() || capability.getStatus() == StatusResults.getStatusByKeyword("withdrawn")) {
            nbNotEvaluated++;
        }
        return nbNotEvaluated;
    }

    private int getNbAtRisk(int nbAtRisk, SystemAIPOResultForATestingSession capability) {
        if (capability.getStatus() == StatusResults.getSTATUS_AT_RISK()) {
            nbAtRisk++;
        }
        return nbAtRisk;
    }

    private int getNbDidNotComplete(int nbDidNotComplete, SystemAIPOResultForATestingSession capability) {
        if (capability.getStatus() == StatusResults.getSTATUS_RESULT_FAILED()) {
            nbDidNotComplete++;
        }
        return nbDidNotComplete;
    }

    private int getNbPassed(int nbPassed, SystemAIPOResultForATestingSession capability) {
        if (capability.getStatus() == StatusResults.getSTATUS_RESULT_PASSED()) {
            nbPassed++;
        }
        return nbPassed;
    }
}
