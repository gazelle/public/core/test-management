package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.instance.service;

import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.instance.AutomatedTestStepInstanceData;
import net.ihe.gazelle.tm.gazelletest.domain.AutomatedTestStepInputDomain;
import net.ihe.gazelle.tm.gazelletest.model.instance.AutomatedTestStepInstanceInput;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestStepsInstance;

import java.util.Date;
import java.util.List;

public interface AutomatedTestStepInstanceInputService {

    /**
     * Create an automated test step instance input
     * @param inputDefinition The definition of the input instance to be saved
     * @param testStepsInstanceId The id of the test step instance linked to this input
     * @return The instance input created
     */
    AutomatedTestStepInstanceInput createAutomatedTestStepInstanceInput(AutomatedTestStepInputDomain inputDefinition, Integer testStepsInstanceId);

    /**
     * Save everything concerning a test step instance input data
     * @param automatedTestStepInstanceInputId The id of the input instance linked to the data
     * @param testStepsInstanceId The id of the test step instance
     * @param filename The input file name
     * @param fileContent The input file content
     */
    void saveInputData(Integer automatedTestStepInstanceInputId, Integer testStepsInstanceId, String filename, String fileContent);

    /**
     * Read an input data file
     * @param inputData The input data to be read
     * @return The content of the input data
     */
    String readInputData(AutomatedTestStepInstanceData inputData);

    /**
     * Delete everything concerning a test step instance input data
     * @param inputData The input data to be deleted
     */
    void deleteInputData(AutomatedTestStepInstanceData inputData);

    /**
     * Get all input instance from a test step instance
     * @param stepInstance The test step instance
     * @return The result list of all input instances
     */
    List<AutomatedTestStepInstanceData> getInputDataFromTestStepInstance(TestStepsInstance stepInstance);

    /**
     * Get the last instance input update for a test step instance
     * @param testStepInstanceId the id of the test step instance
     * @return the date of the last upload in this test step instance
     */
    Date getLatestUploadDate(Integer testStepInstanceId);

    /**
     * Get all the instance input for a whole test instance
     * @param testInstance the test instance
     * @return the list of all instance input
     */
    List<AutomatedTestStepInstanceInput> getInstanceInputForTestInstance(TestInstance testInstance);

    List<AutomatedTestStepInstanceInput> getInstanceInputForTestStepInstance(TestStepsInstance testStepsInstance);
}
