package net.ihe.gazelle.tm.testexecution;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManagerImpl;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.menu.Pages;
import net.ihe.gazelle.tm.gazelletest.action.CookiesPreset;
import net.ihe.gazelle.tm.gazelletest.model.instance.SystemAIPOResultForATestingSession;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.faces.Redirect;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Name(value = "quickFilterBean")
@AutoCreate
@Scope(ScopeType.EVENT)
public class QuickFilterBean implements Serializable {
    private static final Logger LOG = LoggerFactory.getLogger(QuickFilterBean.class);
    private final ApplicationPreferenceManager applicationPreferenceManager = new ApplicationPreferenceManagerImpl();
    private final String APP_BASENAME = applicationPreferenceManager.getStringValue("application_url_basename");
    private static final String COOKIE_BASE_NAME = "testExecutionCookie";
    private static final String DEFAULT_COOKIE_NAME = "defaultTestExec";
    private static final long serialVersionUID = -2623014230459417813L;
    private QuickFilter defaultQuickFilter;
    private boolean hasDefaultQuickFilter;
    private List<QuickFilter> quickFilters;

    @Create
    public void init() {
        this.defaultQuickFilter = initDefaultQuickFilter();
        this.quickFilters = getAllQuickFilter();
        Map<String, String> requestParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (requestParams.isEmpty() && defaultQuickFilter != null) {
            Redirect redirect = Redirect.instance();
            redirect.setViewId(defaultQuickFilter.getUrl());
            for (Map.Entry<String, String> entry : defaultQuickFilter.getParameters().entrySet()) {
                redirect.setParameter(entry.getKey(), entry.getValue());
            }
            redirect.execute();
        }
    }

    public QuickFilter getDefaultQuickFilter() {
        return defaultQuickFilter;
    }

    public String getCurrentQuickFilterStyle(QuickFilter currentQuickFilter) {
        if (currentQuickFilter != null) {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            ExternalContext externalContext = facesContext.getExternalContext();
            String currentUrl = Pages.TEST_EXECUTION.getMenuLink() + "?" + ((HttpServletRequest) externalContext.getRequest()).getQueryString();
            if (currentQuickFilter.getUrl().equals(currentUrl)) {
                return "font-weight:bold;";
            }
        }
        return "";
    }

    public boolean hasDefaultQuickFilter() {
        return hasDefaultQuickFilter;
    }

    public String createCookie(Filter<SystemAIPOResultForATestingSession> filter) {
        CookiesPreset.instance().createCookie(COOKIE_BASE_NAME, filter, null);
        return Pages.TEST_EXECUTION.getMenuLink() + "?" + filter.getUrlParameters();
    }

    public List<QuickFilter> getQuickFilters() {
        return quickFilters;
    }

    public String setDefaultQuickFilter(QuickFilter quickFilter) {
        if (this.defaultQuickFilter != null) {
            CookiesPreset.instance().deleteCookieToLoad(this.defaultQuickFilter.getCookie().getName());
        }
        if (quickFilter.isDefaultFilter()) {
            quickFilter.setDefaultFilter(false);
        } else {
            quickFilter.setDefaultFilter(true);
            String cookieName = DEFAULT_COOKIE_NAME + "_1_" + quickFilter.getDisplayName();
            CookiesPreset.instance().createSimpleCookie(cookieName, quickFilter.getCookie().getValue());
            for (QuickFilter filter : this.quickFilters) {
                filter.setDefaultFilter(filter.getDisplayName().equals(quickFilter.getDisplayName()));
            }
            CookiesPreset.instance().setMainPageCookie(true);
        }
        return quickFilter.getUrl();
    }

    private List<QuickFilter> getAllQuickFilter() {
        List<QuickFilter> allQuickFilters = new ArrayList<>();
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();

        Cookie[] cookies = request.getCookies();
        if (cookies != null && cookies.length > 0) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().startsWith(COOKIE_BASE_NAME)) {
                    if (defaultQuickFilter != null) {
                        allQuickFilters.add(new QuickFilter(cookie, COOKIE_BASE_NAME, defaultQuickFilter.getDisplayName()));
                    } else {
                        allQuickFilters.add(new QuickFilter(cookie, COOKIE_BASE_NAME, ""));
                    }
                }
            }
        }
        return allQuickFilters;
    }

    private QuickFilter initDefaultQuickFilter() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();

        Cookie[] cookies = request.getCookies();
        if (cookies != null && cookies.length > 0) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().startsWith(DEFAULT_COOKIE_NAME)) {
                    this.hasDefaultQuickFilter = true;
                    return new QuickFilter(cookie, DEFAULT_COOKIE_NAME, DEFAULT_COOKIE_NAME);
                }
            }
        }
        this.hasDefaultQuickFilter = false;
        return null;
    }

    public String deleteSelectedQuickFilter(QuickFilter quickFilter) {
        deleteSelectedCookie(quickFilter, "/testing/test");
        if (defaultQuickFilter != null && defaultQuickFilter.getDisplayName().equals(quickFilter.getDisplayName())) {
            CookiesPreset.instance().deleteCookieToLoad(this.defaultQuickFilter.getCookie().getName());
        }
        return Pages.TEST_EXECUTION.getMenuLink();
    }

    public void deleteSelectedCookie(QuickFilter quickFilter, String path) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();
        HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();

        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(quickFilter.getCookie().getName())) {
                    Cookie cookieToDelete = new Cookie(cookie.getName(), "");
                    cookieToDelete.setPath("/" + APP_BASENAME + path);
                    cookieToDelete.setMaxAge(0);
                    response.addCookie(cookieToDelete);
                    FacesMessages.instance().add(StatusMessage.Severity.INFO, "Cookie " + quickFilter.getDisplayName() + " is deleted !");
                }
            }
        }
    }

    public String getShortcutName() {
        return CookiesPreset.instance().getShortcutName();
    }

    public void setShortcutName(String shortcutName) {
        CookiesPreset.instance().setShortcutName(shortcutName);
    }
}
