package net.ihe.gazelle.tm.statistics.registration.gui;


import net.ihe.gazelle.menu.Pages;
import net.ihe.gazelle.tm.statistics.registration.StatisticsForRegistrationPhaseService;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Scope(ScopeType.EVENT)
@AutoCreate
@Name("statisticsForRegistrationPhaseBeanGui")
public class StatisticsForRegistrationPhaseBeanGui {

    public static final String COLOR_RED_STRING = "\"red\"";
    public static final String COLOR_BLUE_GAZELLE_STRING = "\"#4ca8de\"";
    public static final String COLOR_GREY_STRING = "\"#C1C1C1\"";
    public static final String COLOR_GREEN_STRING = "\"#5cb85c\"";
    @In(value = "statisticsForRegistrationPhaseService")
    StatisticsForRegistrationPhaseService statisticsForRegistrationPhaseService;

    public List<Integer> getSystemInSessionStatsForCurrentSession() {
        StatisticsForRegistrationPhaseService.SystemInSessionStats systemInSessionStats = statisticsForRegistrationPhaseService.getSystemInSessionStatisticsForCurrentTestingSession();
        return new ArrayList<>(Arrays.asList(
                systemInSessionStats.getNbAccepted(),
                systemInSessionStats.getNbDropped(),
                systemInSessionStats.getNbInProgress(),
                systemInSessionStats.getNbCompleted()));
    }


    public int getNumberOfSessionParticipants() {
        return statisticsForRegistrationPhaseService.getNumberOfSessionParticipants();
    }

    public int getNumberOfRegisteredInstitution() {
        return statisticsForRegistrationPhaseService.getNumberOfRegisteredInstitution();
    }

    public int getNumberOfRegisteredSystemInSession() {
        return statisticsForRegistrationPhaseService.getNumberOfRegisteredSystemInSession();
    }


    public List<String> getColorsForSystemInSessionChart() {
        return new ArrayList<>(Arrays.asList(
                COLOR_GREEN_STRING,
                COLOR_RED_STRING,
                COLOR_GREY_STRING,
                COLOR_BLUE_GAZELLE_STRING
        ));
    }

    public String getManageAttendeesLink() {
        return Pages.REGISTRATION_PARTICIPANTS.getMenuLink();
    }

    public String getManageSUTsLink() {
        return Pages.REGISTRATION_SYSTEMS.getMenuLink();
    }
}
