package net.ihe.gazelle.tm.gazelletest.testingSessionScope.comparators;

import net.ihe.gazelle.tm.gazelletest.testingSessionScope.ProfileScope;

import java.util.Comparator;

public class TestableFirstComparator implements Comparator<ProfileScope> {
    @Override
    public int compare(ProfileScope profileScope1, ProfileScope profileScope2) {
        return profileScope1.getProfileInTestingSession().getTestability().getRank() - profileScope2.getProfileInTestingSession().getTestability().getRank();
    }
}
