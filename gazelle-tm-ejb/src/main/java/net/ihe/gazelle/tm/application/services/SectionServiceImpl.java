package net.ihe.gazelle.tm.application.services;

import net.ihe.gazelle.tm.application.model.Section;

import java.util.Collections;
import java.util.List;

public class SectionServiceImpl implements SectionService{

    SectionDAO sectionDAO;

    public SectionServiceImpl(SectionDAO sectionDAO) {
        this.sectionDAO = sectionDAO;
    }

    @Override
    public Section getSection(Integer sectionId) {
        return sectionDAO.getSection(sectionId);
    }

    @Override
    public Section getSectionByName(String sectionName) {
        return sectionDAO.getSectionByName(sectionName);
    }

    @Override
    public List<Section> getSections() {
        return sortSectionsByOrder(sectionDAO.getSections());
    }

    @Override
    public List<Section> getRenderedSections() {
        return sortSectionsByOrder(sectionDAO.getRenderedSections());
    }

    @Override
    public void removeSection(Section section) {
        sectionDAO.removeSection(section);
    }

    @Override
    public void updateSection(Section section) {
        sectionDAO.updateSection(section);
    }

    private List<Section> sortSectionsByOrder(List<Section> sections){
        Collections.sort(sections);
        return sections;
    }
}
