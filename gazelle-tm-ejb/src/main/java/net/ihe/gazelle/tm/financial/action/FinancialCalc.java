package net.ihe.gazelle.tm.financial.action;

import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.users.model.Institution;
import net.ihe.gazelle.users.model.Iso3166CountryCode;
import net.ihe.gazelle.users.model.Person;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

/**
 * @author abderrazek boufahja
 */
public abstract class FinancialCalc {

    protected final Logger LOG = LoggerFactory.getLogger(FinancialCalc.class);
    private final String urlRESTForVATValidation = "http://www.apilayer.net/api/validate?";

    public BigDecimal calculateTotalFee(TestingSession ts, int numberSystem, int numberParticipant) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("BigDecimal calculateTotalFee");
        }
        BigDecimal res = BigDecimal.ZERO;
        if (numberSystem > 0) {
            res = ts.getFeeAdditionalSystem().multiply(new BigDecimal(numberSystem - 1));
            res = res.add(ts.getFeeFirstSystem());
        }
        if (numberParticipant > (ts.getNbParticipantsIncludedInSystemFees() * numberSystem)) {
            BigDecimal participantFee = ts.getFeeParticipant();
            BigDecimal feePart = participantFee.multiply(new BigDecimal(numberParticipant - (ts.getNbParticipantsIncludedInSystemFees() *
                    numberSystem)));
            res = res.add(feePart);
        }
        return res;
    }

    public boolean matchVATCountryToInstitutionInSession(Institution inst, Iso3166CountryCode isoInvoice, List<Person> billingContacts) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("boolean matchVATCountryToInstitutionInSession");
        }
        if (isoInvoice == null) {
            return false;
        }
        if (inst == null) {
            return false;
        }
        Iso3166CountryCode iso = null;
        if ((billingContacts != null) && (billingContacts.size() > 0)) {
            Person p = billingContacts.get(0);
            if (p.getAddress() != null) {
                iso = p.getAddress().getIso3166CountryCode();
            } else {
                return false;
            }
        }
        return isoInvoice.equals(iso);
    }

    public boolean validateVATNumber(String vatNumber, Iso3166CountryCode vatCountry, String accessKey) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("boolean validateVATNumber");
        }
        if ((vatNumber == null) || (vatNumber.trim().length() == 0)) {
            return false;
        } else {
            if (vatCountry == null) {
                return false;
            }
            String url = urlRESTForVATValidation + "access_key=" + accessKey + "&vat_number=" + vatCountry.getIso() + vatNumber;
            LOG.info(url);
            InputStream is;
            BufferedReader buff = null;
            try {
                is = connect(url);
                buff = new BufferedReader(new InputStreamReader(is));
                String response = buff.readLine();
                JSONObject obj = (JSONObject) JSONValue.parse(response);
                return (Boolean) obj.get("valid");
            } catch (NullPointerException e) {
                LOG.error("" + e.getMessage());
                return false;
            } catch (IOException e) {
                LOG.error("" + e.getMessage());
                return false;
            } finally {
                if (buff != null) {
                    try {
                        buff.close();
                    } catch (IOException e) {
                        LOG.error("" + e.getMessage());
                    }
                }
            }
        }
    }

    private InputStream connect(String url) throws IOException {
        try {
            URLConnection conn = new URL(url).openConnection();
            InputStream in = conn.getInputStream();
            return in;
        } catch (IOException e) {
            throw e;
        }
    }
}
