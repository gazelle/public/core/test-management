package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.execution.ws;

import net.ihe.gazelle.ssov7.authn.domain.GazelleLoginException;
import net.ihe.gazelle.ssov7.authn.interlay.adapter.GazelleIdentityImpl;
import net.ihe.gazelle.tm.gazelletest.action.InvalidTokenException;
import net.ihe.gazelle.tm.gazelletest.action.TestInstanceTokenService;
import net.ihe.gazelle.tm.gazelletest.action.TokenNotFoundException;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.execution.service.AutomatedTestStepRunService;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.execution.service.NotRunningException;
import net.ihe.gazelle.tm.gazelletest.domain.AutomatedTestStepExecution;
import net.ihe.gazelle.tm.gazelletest.domain.AutomatedTestStepExecutionStatus;
import net.ihe.gazelle.tm.gazelletest.domain.AutomatedTestStepReportFile;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Transactional;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Stateless
@Local
@Name("automatedStepResultController")
public class AutomatedStepResultControllerImpl implements AutomatedStepResultController {

    @In(value = "testInstanceTokenService")
    private TestInstanceTokenService tokenService;
    @In(value = "automatedTestStepRunService")
    private AutomatedTestStepRunService runService;

    @Override
    @Transactional
    public Response receiveReport(Integer testInstanceId, Integer testStepInstanceId, HttpServletRequest request) {
        if (getIdentity().isLoggedIn()) {
            try {
                AutomatedTestStepExecution execution = buildReceivedExecution(testStepInstanceId, request);
                runService.receiveExecution(getIdentity(), execution);
                return Response.status(Response.Status.OK).build();
            } catch (TokenNotFoundException | InvalidTokenException e) {
                return Response.status(Response.Status.UNAUTHORIZED).entity(e.getMessage()).build();
            } catch (NotRunningException e) {
                return Response.status(Response.Status.NOT_MODIFIED).entity(e.getMessage()).build();
            } catch (GazelleLoginException e) {
                return Response.status(Response.Status.UNAUTHORIZED).build();
            } catch (MalformedReportException | FileUploadException e) {
                runService.cancelExecution(testStepInstanceId);
                return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
            } finally {
                getIdentity().logout();
            }
        } else {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }

    private AutomatedTestStepExecution buildReceivedExecution(Integer testStepInstanceId, HttpServletRequest request) throws FileUploadException {
        FileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload upload = new ServletFileUpload(factory);
        AutomatedTestStepExecution execution = new AutomatedTestStepExecution();
        List<FileItem> items = upload.parseRequest(request);
        for (FileItem item : items) {
            if (item.getContentType().contains(MediaType.APPLICATION_JSON)) {
                String jsonReport = item.getString();
                readReport(testStepInstanceId, jsonReport, execution);
            }
            if (item.getContentType().contains("application/pdf")) {
                String reportName = item.getName();
                byte[] pdfContent = item.get();
                execution.addPdfFile(new AutomatedTestStepReportFile(reportName, pdfContent));
            }
        }
        return execution;
    }

    private void readReport(Integer testStepInstanceId, String jsonReport, AutomatedTestStepExecution execution) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            objectMapper.setDateFormat(dateFormat);
            JsonNode rootNode = objectMapper.readTree(jsonReport);
            String status = rootNode.path("status").asText();
            String dateString = rootNode.path("date").asText();
            Date date = dateFormat.parse(dateString);
            execution.setTestStepsInstanceId(testStepInstanceId);
            execution.setStatus(AutomatedTestStepExecutionStatus.findByLabel(status));
            execution.setDate(date);
            execution.setJsonReport(jsonReport);
        } catch (ParseException | IOException e) {
            throw new MalformedReportException("Unable to parse report");
        }
    }

    private GazelleIdentityImpl getIdentity() {
        return GazelleIdentityImpl.instance();
    }
}
