package net.ihe.gazelle.tm.systems;

import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.SystemInSessionRegistrationStatus;
import net.ihe.gazelle.tm.systems.model.System;
public class SystemSummary {

    private String SUTKeyword;

    private String SUTName;

    private String SUTType;

    private String version;

    private String owner;

    private String ownerMail;

    private SystemInSessionRegistrationStatus registrationStatus;

    private boolean missingDependencies;

    private boolean accepted;

    private boolean isTool;
    private SystemInSession systemInSession;

    private System system;
    public String getSUTKeyword() {
        return SUTKeyword;
    }

    public void setSUTKeyword(String SUTKeyword) {
        this.SUTKeyword = SUTKeyword;
    }

    public String getSUTName() {
        return SUTName;
    }

    public void setSUTName(String SUTName) {
        this.SUTName = SUTName;
    }

    public String getVersion() {
        return version;
    }
    public SystemInSession getSystemInSession() {
        return systemInSession;
    }
    public void setSystemInSession(SystemInSession systemInSession) {
        this.systemInSession = systemInSession;
    }
    public void setVersion(String version) {
        this.version = version;
    }

    public String getOwner() {
        return owner;
    }

    public System getSystem() { return system; }

    public boolean isTool() { return isTool; }

    public void setTool(boolean tool) { isTool = tool; }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getOwnerMail() {
        return ownerMail;
    }

    public void setOwnerMail(String ownerMail) {
        this.ownerMail = ownerMail;
    }

    public SystemInSessionRegistrationStatus getRegistrationStatus() {
        return registrationStatus;
    }

    public void setRegistrationStatus(SystemInSessionRegistrationStatus registrationStatus) {
        this.registrationStatus = registrationStatus;
    }

    public boolean hasMissingDependencies() {
        return missingDependencies;
    }

    public void setMissingDependencies(boolean missingDependencies) {
        this.missingDependencies = missingDependencies;
    }

    public boolean isAccepted() {
        return accepted;
    }


    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

    public String getSUTType() {
        return SUTType;
    }

    public void setSUTType(String SUTType) {
        this.SUTType = SUTType;
    }

    public void setSystem(System system) {
        this.system = system;
    }
}
