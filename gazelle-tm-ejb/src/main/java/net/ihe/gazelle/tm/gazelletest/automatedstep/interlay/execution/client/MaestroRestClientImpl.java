package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.execution.client;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManagerImpl;
import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script.MaestroRequest;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.script.dto.MaestroRequestDTO;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestStepsInstance;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response.Status;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@Name("maestroRestClient")
@Scope(ScopeType.STATELESS)
@AutoCreate
public class MaestroRestClientImpl implements MaestroRestClient {

    private static final Logger LOG = LoggerFactory.getLogger(MaestroRestClientImpl.class);
    private static final String GZL_MAESTRO_URL = "maestro_url";
    private static final String GZL_CALLBACK_URL = "%srest/testruns/%d/steps/%d";

    private final ApplicationPreferenceManager preferenceManager = new ApplicationPreferenceManagerImpl();

    @Override
    public void runAutomatedTestStep(MaestroRequest request, TestStepsInstance stepInstance) throws StepRunClientException {
        if (request != null) {
            try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
                request.setClientCallback(buildCallback(stepInstance.getTestInstance().getId(), stepInstance.getId()));
                MaestroRequestDTO maestroRequestDTO = new MaestroRequestDTO(request);
                List<MaestroRequestDTO> maestroRequestList = new ArrayList<>();
                maestroRequestList.add(maestroRequestDTO);
                String url = getMaestroUrl() + "/run";
                HttpEntityEnclosingRequestBase runStepRequest = new HttpPost(url);

                ObjectMapper objectMapper = new ObjectMapper();
                objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
                String scriptJson = objectMapper.writeValueAsString(maestroRequestList);

                runStepRequest.addHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.getMimeType());
                runStepRequest.setEntity(new StringEntity(scriptJson, StandardCharsets.UTF_8));
                try (CloseableHttpResponse response = httpClient.execute(runStepRequest)) {
                    int statusCode = response.getStatusLine().getStatusCode();
                    if (statusCode != Status.OK.getStatusCode()) {
                        String errorString = "Step run aborted, communication problem with external tool";
                        if (statusCode == Status.BAD_REQUEST.getStatusCode()) {
                            errorString = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);
                        }
                        LOG.error(errorString + " to url : " + url);
                        throw new StepRunClientException(errorString);
                    }
                }
            } catch (IOException e) {
                throw new StepRunClientException("Run step failed", e);
            }
        } else {
            throw new IllegalArgumentException("Run failed cause maestro script is null.");
        }
    }

    private String getMaestroUrl() {
        return preferenceManager.getStringValue(GZL_MAESTRO_URL);
    }

    private String getApplicationUrl() {
        return preferenceManager.getApplicationUrl();
    }

    private String buildCallback(Integer testInstanceId, Integer testStepInstanceId) {
        String applicationUrl = getApplicationUrl();
        if (applicationUrl.endsWith("/")) {
            return String.format(GZL_CALLBACK_URL, applicationUrl, testInstanceId, testStepInstanceId);
        } else {
            return String.format(GZL_CALLBACK_URL, applicationUrl + "/", testInstanceId, testStepInstanceId);
        }
    }
}
