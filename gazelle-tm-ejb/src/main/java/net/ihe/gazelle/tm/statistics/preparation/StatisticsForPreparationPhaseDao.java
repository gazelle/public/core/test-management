package net.ihe.gazelle.tm.statistics.preparation;

import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.systems.model.TestingSession;

import java.util.List;

/**
 * The DAO interface preparation phase statistics.
 */
public interface StatisticsForPreparationPhaseDao {

    /**
     * Gets number of activated monitor in session.
     *
     * @param selectedTestingSession the selected testing session
     * @return the number of activated monitor in session
     */
    int getNumberOfActivatedMonitorInSession(TestingSession selectedTestingSession);

    /**
     * Gets number of accepted system in session.
     *
     * @return the number of accepted system in session
     */
    int getNumberOfAcceptedSystemInSession();

    /**
     * Gets number of not accepted system in session.
     *
     * @return the number of not accepted system in session
     */
    int getNumberOfNotAcceptedSystemInSession();

    /**
     * Gets number of accepted supportive request.
     *
     * @param selectedTestingSession the selected testing session
     * @return the number of accepted supportive request
     */
    int getNumberOfAcceptedSupportiveRequest(TestingSession selectedTestingSession);

    /**
     * Gets number of not yet accepted supportive request.
     *
     * @param selectedTestingSession the selected testing session
     * @return the number of not yet accepted supportive request
     */
    int getNumberOfNotYetAcceptedSupportiveRequest(TestingSession selectedTestingSession);

    /**
     * Gets number of sut network configuration.
     *
     * @param selectedTestingSession the selected testing session
     * @return the number of sut network configuration
     */
    int getNumberOfSUTNetworkConfiguration(TestingSession selectedTestingSession);

    /**
     * Gets number of sut network configuration accepted.
     *
     * @param selectedTestingSession the selected testing session
     * @return the number of sut network configuration accepted
     */
    int getNumberOfSUTNetworkConfigurationAccepted(TestingSession selectedTestingSession);

    /**
     * Gets number of sut network configuration not accepted.
     *
     * @param selectedTestingSession the selected testing session
     * @return the number of sut network configuration not accepted
     */
    int getNumberOfSUTNetworkConfigurationNotAccepted(TestingSession selectedTestingSession);

    /**
     * Gets number of generated hosts.
     *
     * @param selectedTestingSession the selected testing session
     * @return the number of generated hosts
     */
    int getNumberOfGeneratedHosts(TestingSession selectedTestingSession);

    /**
     * Gets number of generated hosts with ip.
     *
     * @param selectedTestingSession the selected testing session
     * @return the number of generated hosts with ip
     */
    int getNumberOfGeneratedHostsWithIP(TestingSession selectedTestingSession);

    /**
     * Gets number of generated hosts without ip.
     *
     * @param selectedTestingSession the selected testing session
     * @return the number of generated hosts without ip
     */
    int getNumberOfGeneratedHostsWithoutIP(TestingSession selectedTestingSession);

    /**
     * Gets number of preparatory tests.
     *
     * @param selectedTestingSession the selected testing session
     * @return the number of preparatory tests
     */
    int getNumberOfPreparatoryTests(TestingSession selectedTestingSession);

    /**
     * Gets all preparatory test instances.
     *
     * @param selectedTestingSession the selected testing session
     * @return the all preparatory test instances
     */
    List<TestInstance> getAllPreparatoryTestInstances(TestingSession selectedTestingSession);

}
