package net.ihe.gazelle.tm.gazelletest.action;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXParseException;

import java.util.ArrayList;
import java.util.List;

public class XSDErrorHandler implements ErrorHandler {

   private final List<SAXParseException> warnings = new ArrayList<>();
   private final List<SAXParseException> errors = new ArrayList<>();
   private final List<SAXParseException> fatalErrors = new ArrayList<>();

   @Override
   public void warning(SAXParseException exception) {
      warnings.add(exception);
   }

   @Override
   public void error(SAXParseException exception) {
      errors.add(exception);
   }

   @Override
   public void fatalError(SAXParseException exception) {
      fatalErrors.add(exception);
   }

   List<SAXParseException> getWarnings() {
      return warnings;
   }

   public List<SAXParseException> getErrors() {
      return errors;
   }

   List<SAXParseException> getFatalErrors() {
      return fatalErrors;
   }

   public boolean isErrors() {
      return !(getErrors().isEmpty() && getFatalErrors().isEmpty());
   }

   public boolean containsError(String expectedError) {
      return containsException(getErrors(), expectedError);
   }

   boolean containsWarning(String expectedWarning) {
      return containsException(getWarnings(), expectedWarning);
   }

   boolean containsFatalError(String expectedFatalError) {
      return containsException(getFatalErrors(), expectedFatalError);
   }

   private boolean containsException(List<SAXParseException> exceptions, String expectedMessage) {
      for(SAXParseException exception : exceptions) {
         if(exception.getMessage().contains(expectedMessage)) {
            return true;
         }
      }
      return false;
   }
}
