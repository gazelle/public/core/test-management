package net.ihe.gazelle.tm.comtool.interlay;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.tm.comtool.application.ComToolManager;
import net.ihe.gazelle.tm.comtool.application.ComToolManagerDAO;
import net.ihe.gazelle.tm.comtool.application.ComToolManagerImpl;
import net.ihe.gazelle.tm.comtool.application.ComToolRestClient;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Factory;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.persistence.EntityManager;

@Name("comToolDaoFactory")
@AutoCreate
@Scope(ScopeType.EVENT)
public class ComToolDAOFactory {

   @In
   EntityManager entityManager;

   @Factory(value = "comToolManagerDAO", autoCreate = true, scope = ScopeType.EVENT)
   public ComToolManagerDAO getComToolManagerDAO() {
      return new ComToolManagerDAOImpl(entityManager);
   }

}
