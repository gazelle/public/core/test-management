package net.ihe.gazelle.tm.application.gui.summary;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.io.Serializable;

@Name("SUTNetworkSummaryBeanGui")
@Scope(ScopeType.PAGE)
public class SUTNetworkSummaryBeanGui implements Serializable {

    private static final long serialVersionUID = 5504782634899135954L;


    /////////////////// Links ///////////////////

    public String editSUTNetworkInterfacesButton(){
        return "/configuration/list/configurationMainWindow.seam";
    }

    public String networkDetailsButton(){
        return "/configuration/parameter/configureIPsAndOIDSForSession.seam";
    }

    public String SUTNetworkInterfacesButton(){
        return "/configuration/configurations.seam";
    }
}
