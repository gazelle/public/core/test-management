package net.ihe.gazelle.tm.statistics.testing;

import net.ihe.gazelle.tm.gazelletest.model.instance.MonitorInSession;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.TestingSession;

import java.util.List;

/**
 * The DAO interface for testing phase statistics.
 */
public interface StatisticsForTestingPhaseDao {

    /**
     * Gets systems for current user.
     *
     * @param selectedTestingSession the selected testing session
     * @return the systems for current user
     */
    List<SystemInSession> getSystemsForCurrentUser(TestingSession selectedTestingSession);

    /**
     * Gets test instances for a system.
     *
     * @param system the system
     * @return the test instances for a system
     */
    List<TestInstance> getTestInstancesForASystem(SystemInSession system);

    /**
     * Gets all test instances for current session.
     *
     * @return the all test instances for current session
     */
    List<TestInstance> getAllTestInstancesForCurrentSession();

    /**
     * Gets test instances with verification pending.
     *
     * @return the test instances with verification pending
     */
    List<TestInstance> getTestInstancesWithVerificationPending();

    /**
     * Gets leftover test instances.
     *
     * @return the leftover test instances
     */
    List<TestInstance> getLeftOverTestInstances();

    /**
     * Gets test instances for monitor in session.
     *
     * @param monitor the monitor
     * @return the test instances for monitor in session
     */
    List<TestInstance> getTestInstancesForMonitorInSession(MonitorInSession monitor);

    /**
     * Gets number of total test instance for a system in session.
     *
     * @param system the system
     * @return the number of total test instance for a system in session
     */
    int getNumberOfTotalTestInstanceForASystemInSession(SystemInSession system);

    /**
     * Gets number of total test instances for monitor in session.
     *
     * @param monitor the monitor
     * @return the number of total test instances for monitor in session
     */
    int getNumberOfTotalTestInstancesForMonitorInSession(MonitorInSession monitor);

    /**
     * Gets number of total test instances.
     *
     * @return the number of total test instances
     */
    int getNumberOfTotalTestInstances();

    /**
     * Gets number of verification pending test instances.
     *
     * @return the number of verification pending test instances
     */
    int getNumberOfVerificationPendingTestInstances();

}
