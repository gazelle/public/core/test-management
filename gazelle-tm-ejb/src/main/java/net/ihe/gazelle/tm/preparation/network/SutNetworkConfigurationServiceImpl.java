package net.ihe.gazelle.tm.preparation.network;

import net.ihe.gazelle.tm.session.TestingSessionService;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.util.ArrayList;
import java.util.List;

@Name(value = "sutNetworkConfigurationService")
@AutoCreate
@Scope(ScopeType.EVENT)
public class SutNetworkConfigurationServiceImpl implements SutNetworkConfigurationService {

    @In(value = "sutNetworkConfigurationDao")
    private SutNetworkConfigurationDao sutNetworkConfigurationDao;

    @In(value = "testingSessionService")
    private TestingSessionService testingSessionService;

    @Override
    public List<SutNetworkConfigurationSummary> getSutNetworkConfigurationSummaries() {
        List<SutNetworkConfigurationSummary> networkConfigurationSummaries = new ArrayList<>();
        for (SystemInSession system : sutNetworkConfigurationDao.getListSystemInSessionOfCurrenUser(
                testingSessionService.getUserTestingSession())) {
            networkConfigurationSummaries.add(constructSutNetworkConfigurationSummary(system));
        }
        return networkConfigurationSummaries;
    }

    @Override
    public int getNumberOfNetworkConfigurationsHosts() {
        return sutNetworkConfigurationDao.getNumberOfNetworkConfigurationsHosts(testingSessionService.getUserTestingSession());
    }

    @Override
    public int getNumberOfNetworkConfigurationsHostsNoIp() {
        return sutNetworkConfigurationDao.getNumberOfNetworkConfigurationsHostsNoIp(testingSessionService.getUserTestingSession());
    }


    private SutNetworkConfigurationSummary constructSutNetworkConfigurationSummary(SystemInSession systemInSession) {
        SutNetworkConfigurationSummary sutNetworkConfigurationSummary = new SutNetworkConfigurationSummary();
        sutNetworkConfigurationSummary.setSutName(systemInSession.getSystem().getKeyword());
        sutNetworkConfigurationSummary.setTestingSessionId(systemInSession.getTestingSession().getId());

        Integer systemInSessionId = systemInSession.getId();
        sutNetworkConfigurationSummary.setSystemId(systemInSessionId);
        sutNetworkConfigurationSummary.setNumberApproved(sutNetworkConfigurationDao.getNumberOfNetworkConfigurationsApprovedForASystemId(systemInSessionId));
        sutNetworkConfigurationSummary.setNumberNotApproved(sutNetworkConfigurationDao.getNumberOfNetworkConfigurationsNotApprovedForASystemId(systemInSessionId));

        return sutNetworkConfigurationSummary;
    }
}
