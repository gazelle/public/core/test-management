package net.ihe.gazelle.tm.organization.ws;

import net.ihe.gazelle.ssov7.authn.interlay.adapter.GazelleIdentityImpl;
import net.ihe.gazelle.ssov7.gum.client.interlay.ws.OrganizationController;
import net.ihe.gazelle.ssov7.gum.client.interlay.ws.OrganizationResource;
import net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.domain.ReportGenerator;
import net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.model.InstitutionListModel;
import net.ihe.gazelle.tm.organization.OrganizationService;
import net.ihe.gazelle.tm.organization.exception.OrganizationCreationException;
import net.ihe.gazelle.tm.organization.exception.OrganizationServiceDAOException;
import net.ihe.gazelle.token.wsclient.InvalidTokenException;
import net.ihe.gazelle.users.model.DelegatedOrganization;
import net.ihe.gazelle.users.model.Institution;
import org.jboss.resteasy.spi.UnauthorizedException;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

@Stateless
@Local
@Name("organizationController")
public class OrganizationControllerImpl implements OrganizationController {

    private static final Logger LOG = LoggerFactory.getLogger(OrganizationControllerImpl.class);

    @In(value = "securedOrganizationService", create = true)
    OrganizationService organizationService;

    public OrganizationControllerImpl() {
    }

    public OrganizationControllerImpl(OrganizationService organizationService) {
        this.organizationService = organizationService;
    }

    @Override
    public Response getOrganizationById(String id) {
        LOG.trace("getOrganizationById");
        try {
            Institution institution = organizationService.getOrganizationById(id);
            OrganizationResource organizationResource = organizationService.getOrganizationresource(institution);
            return Response.status(Response.Status.OK).entity(organizationResource).build();
        } catch (NoSuchElementException e) {
            LOG.error(e.getMessage());
            return Response.status(Response.Status.NOT_FOUND).entity(getErrorMessage(e.getMessage())).build();
        } catch (Exception e) {
            LOG.error(e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Override
    public Response searchForOrganizations(HttpServletRequest servletRequest, String organizationName, String externalId, String idpId, Boolean delegated) {
        LOG.trace("searchForOrganizations");
        try {
            List<Institution> institutions = organizationService.searchForOrganizations(organizationName, externalId, idpId, delegated);
            List<OrganizationResource> organizationResources = new LinkedList<>();
            for (Institution institution : institutions) {
                organizationResources.add(organizationService.getOrganizationresource(institution));
            }
            return Response.ok().status(Response.Status.OK).entity(organizationResources).build();
        } catch (UnauthorizedException e) {
            LOG.error(e.getMessage());
            return Response.status(Response.Status.UNAUTHORIZED).build();
        } catch (Exception e) {
            LOG.error(e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Override
    public Response getXMLOrganizations(HttpServletRequest servletRequest) {
        LOG.trace("Executing getOrganizations(tokenString)");
        GazelleIdentityImpl identity = getIdentity();

        if (getIdentity().isLoggedIn()) {
            try {
                ReportGenerator reportGenerator = new ReportGenerator(identity);
                InstitutionListModel report = reportGenerator.exportOrganisations(identity);

                return Response.status(Response.Status.OK).entity(report).build();
            } catch (InvalidTokenException e) {
                return Response.status(Response.Status.UNAUTHORIZED).build();
            } catch (Exception e) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build(); // Unknown error
            } finally {
                getIdentity().logout();
            }
        } else {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }

    @Override
    public Response createOrganization(OrganizationResource organizationResource) {
        LOG.trace("createOrganization");
        try {
            organizationService.createOrganization(organizationResource);
            return Response.status(Response.Status.CREATED).build();
        } catch (IllegalArgumentException e) {
            LOG.warn(e.getMessage());
            String errorMessage = getErrorMessage("The provided JSON is not valid.");
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage).build();
        } catch (OrganizationCreationException e) {
            LOG.warn(e.getMessage());
            return Response.status(Response.Status.CONFLICT).entity(e.getMessage()).build();
        } catch (Exception e) {
            LOG.error(e.getMessage());
            return Response.serverError().build();
        }
    }

    @Override
    public Response patchOrganization(String id, OrganizationResource organizationResource) {
        if(organizationResource == null) {
            return Response.status(Response.Status.BAD_REQUEST).entity("Missing organization resource").build();
        }

        try {
            Institution institution = organizationService.updateOrganization(id, asInstitution(organizationResource));
            return Response.ok().entity(new OrganizationResource(institution.getKeyword(), institution.getName(), institution.getUrl())).build();
        } catch (OrganizationServiceDAOException e) {
            LOG.error(e.toString());
            return Response.status(Response.Status.BAD_REQUEST).entity(getErrorMessage(e.getMessage())).build();
        }catch (NoSuchElementException e) {
            LOG.error(e.toString());
            return Response.status(Response.Status.NOT_FOUND).entity(getErrorMessage(e.getMessage())).build();
        }catch (UnauthorizedException e){
            LOG.error(e.toString());
            return Response.status(Response.Status.UNAUTHORIZED).entity(getErrorMessage(e.getMessage())).build();
        }catch (Exception e){
            LOG.error(e.toString());
            return Response.serverError().entity(getErrorMessage(e.getMessage())).build();
        }
    }

    private String getErrorMessage(String message) {
        return "{ \"error\": \"" + message + "\" }";
    }

    private GazelleIdentityImpl getIdentity() {
        return GazelleIdentityImpl.instance();
    }

    private Institution asInstitution(OrganizationResource organizationResource) {
        Institution institution;
        if (organizationResource.isDelegated()) {
            institution = new DelegatedOrganization();
            ((DelegatedOrganization) institution).setExternalId(organizationResource.getExternalId());
            ((DelegatedOrganization) institution).setIdpId(organizationResource.getIdpId());
        } else {
            institution = new Institution();
        }
        institution.setKeyword(organizationResource.getId());
        institution.setName(organizationResource.getName());
        institution.setUrl(organizationResource.getUrl());
        return institution;
    }

}