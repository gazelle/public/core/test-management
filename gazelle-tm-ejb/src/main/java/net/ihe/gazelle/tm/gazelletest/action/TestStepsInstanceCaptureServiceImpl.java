package net.ihe.gazelle.tm.gazelletest.action;

import net.ihe.gazelle.tm.gazelletest.model.instance.TestStepsInstance;
import net.ihe.gazelle.tm.tee.status.dao.TestStepsInstanceDAO;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.util.Date;

@Scope(ScopeType.EVENT)
@AutoCreate
@Name("testStepsInstanceCaptureService")
public class TestStepsInstanceCaptureServiceImpl implements TestStepsInstanceCaptureService {

    private final TestStepsInstanceDAO testStepsInstanceDAO;

    public TestStepsInstanceCaptureServiceImpl() {
        this(new TestStepsInstanceDAO());
    }

    public TestStepsInstanceCaptureServiceImpl(TestStepsInstanceDAO testStepsInstanceDAO) {
        this.testStepsInstanceDAO = testStepsInstanceDAO;
    }


    @Override
    public void startCapture(TestStepsInstance testStepsInstance) {
        validateTestStepsInstance(testStepsInstance);

        testStepsInstance.setStartDate(new Date());
        testStepsInstanceDAO.mergeTestStepsInstance(testStepsInstance);
    }

    @Override
    public void endCapture(TestStepsInstance testStepsInstance) {
        validateTestStepsInstance(testStepsInstance);
        if (testStepsInstance.getStartDate()== null)
            throw new IllegalStateException("Cannot end capture because startDate is null");

        testStepsInstance.setEndDate(new Date());
        testStepsInstanceDAO.mergeTestStepsInstance(testStepsInstance);
    }

    @Override
    public void restartCapture(TestStepsInstance testStepsInstance) {
        validateTestStepsInstance(testStepsInstance);

        testStepsInstance.setStartDate(new Date());
        testStepsInstance.setEndDate(null);
        testStepsInstanceDAO.mergeTestStepsInstance(testStepsInstance);
    }

    @Override
    public void extendCapture(TestStepsInstance testStepsInstance) {
        validateTestStepsInstance(testStepsInstance);
        if (testStepsInstance.getStartDate()== null)
            throw new IllegalStateException("Cannot extend capture because startDate is null");

        testStepsInstance.setEndDate(null);
        testStepsInstanceDAO.mergeTestStepsInstance(testStepsInstance);
    }

    private void validateTestStepsInstance(TestStepsInstance testStepsInstance) {
        if (testStepsInstance == null)
            throw new IllegalArgumentException("testStepsInstance is null");
    }
}
