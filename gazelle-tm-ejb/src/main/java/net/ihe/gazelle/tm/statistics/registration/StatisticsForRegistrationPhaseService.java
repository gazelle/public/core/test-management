package net.ihe.gazelle.tm.statistics.registration;

import net.ihe.gazelle.tm.statistics.common.StatisticsCommonService;

public interface StatisticsForRegistrationPhaseService {
    int getNumberOfSessionParticipants();

    int getNumberOfRegisteredInstitution();

    int getNumberOfRegisteredSystemInSession();

    int getNumberOfIntegrationProfilesForCurrentSession();

    int getNumberOfDomainsForCurrentSession();

    SystemInSessionStats getSystemInSessionStatisticsForCurrentTestingSession();

    StatisticsCommonService.ProfileInTestingSessionStats getProfileInTestingSessionStatsForCurrentSession();

    class SystemInSessionStats {
        private final int nbAccepted;
        private final int nbDropped;
        private final int nbInProgress;
        private final int nbCompleted;

        public SystemInSessionStats(int nbAccepted, int nbDropped, int nbInProgress, int nbCompleted) {
            this.nbAccepted = nbAccepted;
            this.nbDropped = nbDropped;
            this.nbInProgress = nbInProgress;
            this.nbCompleted = nbCompleted;
        }

        public int getNbAccepted() {
            return nbAccepted;
        }

        public int getNbDropped() {
            return nbDropped;
        }

        public int getNbInProgress() {
            return nbInProgress;
        }

        public int getNbCompleted() {
            return nbCompleted;
        }

        public boolean isEmpty() {
            return nbAccepted == 0 && nbDropped == 0 && nbInProgress == 0 && nbCompleted == 0;
        }
    }

}
