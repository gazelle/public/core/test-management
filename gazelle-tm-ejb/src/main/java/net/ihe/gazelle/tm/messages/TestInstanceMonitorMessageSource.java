package net.ihe.gazelle.tm.messages;

import net.ihe.gazelle.messaging.MessagePropertyChanged;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.tm.gazelletest.model.instance.MonitorInSession;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import org.kohsuke.MetaInfServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;

@MetaInfServices(MessageSourceFromPropertyChange.class)
public class TestInstanceMonitorMessageSource extends MessageSourceUsersDB<TestInstance> implements
        MessageSourceFromPropertyChange<TestInstance, MonitorInSession> {
    private static final Logger LOG = LoggerFactory.getLogger(TestInstanceMonitorMessageSource.class);
    private final Collection<String> pathsToUser = new ArrayList<>();

    public TestInstanceMonitorMessageSource() {
        super();
        pathsToUser.add("listTestInstanceEvent.userId");
    }

    public TestInstanceMonitorMessageSource(UserService userService) {
        super(userService);
        pathsToUser.add("listTestInstanceEvent.userId");
    }

    @Override
    public Collection<String> getPathsToUsername() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getPathsToUsername");
        }
        return pathsToUser;
    }

    @Override
    public Class<TestInstance> getSourceClass() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getSourceClass");
        }
        return TestInstance.class;
    }

    @Override
    public String getLink(TestInstance instance, String[] messageParameters) {
        return "testInstance.seam?id=" + instance.getId();
    }

    @Override
    public Integer getId(TestInstance sourceObject, String[] messageParameters) {
        return sourceObject.getId();
    }

    @Override
    public String getImage(TestInstance sourceObject, String[] messageParameters) {
        return "gzl-icon-release";
    }

    @Override
    public TestingSession getTestingSession(TestInstance sourceObject, String[] messageParameters) {
        return sourceObject.getTestingSession();
    }

    @Override
    public String getType(TestInstance instance, String[] messageParameters) {
        return "gazelle.message.testinstance.monitor";
    }

    @Override
    public Class<TestInstance> getPropertyObjectClass() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getPropertyObjectClass");
        }
        return TestInstance.class;
    }

    @Override
    public String getPropertyName() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getPropertyName");
        }
        return "monitor";
    }

    @Override
    public void receivePropertyMessage(MessagePropertyChanged<TestInstance, MonitorInSession> messagePropertyChanged) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("receivePropertyMessage");
        }
        String lastMonitor = "None";
        if ((messagePropertyChanged.getOldValue() != null) && (messagePropertyChanged.getOldValue().getUserId() != null)) {
            lastMonitor = messagePropertyChanged.getOldValue().getUserId();
        }
        String newMonitor = "None";
        if ((messagePropertyChanged.getNewValue() != null) && (messagePropertyChanged.getNewValue().getUserId() != null)) {
            newMonitor = messagePropertyChanged.getNewValue().getUserId();
        }
        if (!lastMonitor.equals(newMonitor)) {
            NotificationService.dispatchNotification(this, messagePropertyChanged.getObject(), newMonitor, lastMonitor, messagePropertyChanged.getObject()
                    .getId().toString());
        }
    }

}
