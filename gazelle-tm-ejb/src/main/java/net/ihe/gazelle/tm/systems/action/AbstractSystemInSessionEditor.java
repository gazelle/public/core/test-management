package net.ihe.gazelle.tm.systems.action;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.common.util.File;
import net.ihe.gazelle.dao.SystemInSessionDAO;
import net.ihe.gazelle.dao.SystemInSessionDAOImpl;
import net.ihe.gazelle.hql.HQLReloader;
import net.ihe.gazelle.hql.HQLRestriction;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.menu.Authorizations;
import net.ihe.gazelle.menu.Pages;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.gum.client.application.Role;
import net.ihe.gazelle.tf.model.ActorIntegrationProfileOption;
import net.ihe.gazelle.tm.systems.model.System;
import net.ihe.gazelle.tm.systems.model.*;
import net.ihe.gazelle.users.model.Institution;
import org.jboss.seam.annotations.In;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by gthomazon on 12/07/17.
 */
public abstract class AbstractSystemInSessionEditor extends SystemInSessionNavigator {

    protected static final String NO_ACTIVE_SESSION = "No Active Session !";

    protected Institution institutionForCreation;
    protected String defaultTab;
    protected SystemInSession systemInSession = new SystemInSession();
    // List of all HL7 Conformance Statements (Documents) for a selected system
    protected ArrayList<File> hl7DocumentsFilesForSelectedSystem = new ArrayList<File>();
    // List of all DICOM Conformance Statements (Documents) for a selected system
    protected ArrayList<File> dicomDocumentsFilesForSelectedSystem = new ArrayList<File>();

    @In
    protected GazelleIdentity identity;
    protected SystemInSessionDAO systemInSessionDAO = new SystemInSessionDAOImpl();


    public static List<SystemType> getPossibleSystemTypes() {
        SystemTypeQuery query = new SystemTypeQuery();
        HQLRestriction visibleTrue = query.visible().eqRestriction(true);
        HQLRestriction visibleNull = query.visible().isNullRestriction();
        query.addRestriction(HQLRestrictions.or(visibleTrue, visibleNull));
        query.systemTypeKeyword().order(true);
        return query.getList();
    }

    public void reloadSystemInSession() {
        systemInSession = HQLReloader.reloadDetached(systemInSession);
    }

    public SystemInSession getSystemInSession() {
        return systemInSession;
    }

    public void setSystemInSession(SystemInSession systemInSession) {
        this.systemInSession = systemInSession;
    }

    public ArrayList<File> getHl7DocumentsFilesForSelectedSystem() {
        return hl7DocumentsFilesForSelectedSystem;
    }

    public ArrayList<File> getDicomDocumentsFilesForSelectedSystem() {
        return dicomDocumentsFilesForSelectedSystem;
    }

    public abstract String getSystemInSessionKeywordBase();

    public String getDefaultTab() {
        if (defaultTab == null || defaultTab.isEmpty()) {
            setDefaultTab("editSystemSummaryTab");
        }
        return defaultTab;
    }

    public void setDefaultTab(String defaultTab) {
        this.defaultTab = defaultTab;
    }

    public String goToSystemList() {
        if (identity.hasRole(Role.ADMIN) ||
                Authorizations.TESTING_SESSION_ADMIN_OF_CURRENT_TESTING_SESSION.isGranted()) {
            return Pages.REGISTRATION_SYSTEMS.getLink();
        } else {
            return Pages.SINGLE_REGISTRATION.getLink();
        }
    }

    public boolean userIsCreator(SystemInSession currentSystemInSession) {
        String ownerUserName = currentSystemInSession != null ? currentSystemInSession.getSystem().getOwnerUserId(): "";
        String userName = identity.getUsername();
        return ownerUserName.equals(userName);
    }

    /**
     * Reinitialize the list of Dicom Documents listed in the Dicom Conformance statement modal panel (method used when
     * user clicks on 'Cancel' or for initialization)
     */
    public void initializeDicomDocuments() {
        this.dicomDocumentsFilesForSelectedSystem = systemInSessionDAO
                .retrieveDicomConformanceStatementForSelectedSystemToViewOrEdit(systemInSession);
    }

    /**
     * Reinitialize the list of HL7 Documents listed in the HL7 Conformance statement modal panel (method used when user
     * clicks on 'Cancel' or for initialization)
     */
    public void initializeHL7Documents() {
        this.hl7DocumentsFilesForSelectedSystem = systemInSessionDAO
                .retrieveHL7ConformanceStatementForSelectedSystemToViewOrEdit(systemInSession);
    }

    /**
     * Show a Hl7 conformance statements document (clicking on a link) It calls a static method which performing job for
     * all kind of docs (HL7, DICOM...) This method is in net.ihe.gazelle.common.util.DocumentFileUpload
     *
     * @param file name : File name of the conformance statements (without path)
     */
    public void showHl7ConformanceStatementFile(String file) {
        showConformanceStatementFile(file, systemInSessionDAO.getHl7ConformanceStatementsPath());
    }

    /**
     * Show a DICOM conformance statements document (clicking on a link) It calls a static method which performing job
     * for all kind of docs (HL7, DICOM...) This method is in net.ihe.gazelle.common.util.DocumentFileUpload
     *
     * @param file name : File name of the conformance statements (without path)
     */
    public void showDicomConformanceStatementFile(String file) {
        showConformanceStatementFile(file, systemInSessionDAO.getDicomConformanceStatementsPath());
    }

    private void showConformanceStatementFile(String file, String conformanceStatementsPath) {
        String fullPath = conformanceStatementsPath + java.io.File.separatorChar + systemInSession.getSystem()
                .getId() + java.io.File.separatorChar
                + file;
        net.ihe.gazelle.common.util.DocumentFileUpload.showFile(fullPath);
    }

    public boolean canViewSystemNotes() {
        if (identity.hasRole(Role.ADMIN) || identity.hasRole(Role.PROJECT_MANAGER) ||
                Authorizations.MONITOR_OR_TESTING_SESSION_ADMIN_OF_CURRENT_TESTING_SESSION.isGranted()) {
            return true;
        }
        return isUserRelatedToSystemOrga();
    }


    public boolean canEditSystemInSession() {
        if (identity.hasRole(Role.ADMIN)) {
            return true;
        }
        return isUserRelatedToSystemOrga() &&
                (isRegistrationOpened() || isLateRegistrationGranted()) &&
                isUserCreatorOrVendorAdmin();
    }

    private boolean isUserCreatorOrVendorAdmin() {
        return userIsCreator(systemInSession) || identity.hasRole(Role.VENDOR_ADMIN);
    }

    private boolean isUserRelatedToSystemOrga() {
        return systemInSessionDAO.isSystemInSessionRelatedToUser(systemInSession, identity.getUsername());
    }

    private boolean isRegistrationOpened() {
        return systemInSession.getTestingSession().isRegistrationOpened();
    }

    private boolean isLateRegistrationGranted() {
        return identity.hasRole(Role.VENDOR_LATE_REGISTRATION) && !systemInSession.getTestingSession().isClosed();
    }

    /**
     * Copy apio from <b>systemFrom</b> to <b>newSystem</b>
     *
     * @param systemFrom system containing AIPO
     * @param newSystem  system to copy AIPO
     */
    protected void linkAIPO(System systemFrom, System newSystem) {
        List<SystemActorProfiles> listSAP = SystemActorProfiles.getListOfActorIntegrationProfileOptionsWithTestingDepth(
                systemFrom, getTestingSessionService().getUserTestingSession());

        EntityManager entityManager = EntityManagerService.provideEntityManager();

        for (int elementAIPO = 0; elementAIPO < listSAP.size(); elementAIPO++) {
            SystemActorProfiles newSAP = new SystemActorProfiles();
            newSAP.setSystem(newSystem);
            newSAP.setActorIntegrationProfileOption(entityManager.find(ActorIntegrationProfileOption.class, listSAP
                    .get(elementAIPO).getActorIntegrationProfileOption().getId()));
            newSAP.setTestingDepth(AuditedObject.getObjectByKeyword(TestingDepth.class, "T"));
            entityManager.merge(newSAP);
        }
        entityManager.flush();
    }
}
