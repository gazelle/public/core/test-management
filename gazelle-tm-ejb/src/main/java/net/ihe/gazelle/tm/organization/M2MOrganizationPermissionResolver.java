package net.ihe.gazelle.tm.organization;

import net.ihe.gazelle.ssov7.m2m.server.domain.M2MAuthorization;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.intercept.PostActivate;
import org.jboss.seam.security.permission.PermissionResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Name("m2mOrganizationPermissionResolver")
@Scope(ScopeType.APPLICATION)
@AutoCreate
@Install
public class M2MOrganizationPermissionResolver implements PermissionResolver, Serializable {

    private static final long serialVersionUID = -5660433469908306751L;
    public static final String ANY_ORGA = "any-orga";
    public static final String MY_ORGA = "my-orga";
    public static final String CREATE = "create";
    public static final String READ_PRIVATE = "read-private";
    public static final String READ_PUBLIC = "read-public";
    public static final String UPDATE = "update";

    private static final Logger LOG = LoggerFactory.getLogger(M2MOrganizationPermissionResolver.class);

    private transient Map<MultiKey, M2MAuthorization> authorizations;

    public M2MOrganizationPermissionResolver() {
        LOG.trace("initAuthorizations");
        authorizations = computeAuthorizations();
    }

    @Create
    public void initAuthorizations() {
        LOG.trace("initAuthorizations");
        authorizations = computeAuthorizations();
    }

    @PostActivate
    public void postActivate() {
        initAuthorizations();
    }

    @Override
    public boolean hasPermission(Object target, String action) {
        MultiKey key = new MultiKey(target, action);
        if (authorizations.get(key) != null) {
            return authorizations.get(key).isGranted();
        }
        return false;
    }

    @Override
    public void filterSetByAction(Set<Object> targets, String action) {
        Set<Object> toRemove = new HashSet<>();
        for (Object target : targets) {
            if (hasPermission(target, action)) {
                toRemove.add(target);
            }
        }
        for (Object target : toRemove) {
            targets.remove(target);
        }
    }

    private Map<MultiKey, M2MAuthorization> computeAuthorizations() {
        Map<MultiKey, M2MAuthorization> result = new HashMap<>();
        result.put(new MultiKey(ANY_ORGA, CREATE), M2MOrganizationAuthorization.ANY_ORGA_CREATE);
        result.put(new MultiKey(ANY_ORGA, READ_PRIVATE), M2MOrganizationAuthorization.ANY_ORGA_READ_PRIVATE);
        result.put(new MultiKey(ANY_ORGA, READ_PUBLIC), M2MOrganizationAuthorization.ANY_ORGA_READ_PUBLIC);
        result.put(new MultiKey(ANY_ORGA, UPDATE), M2MOrganizationAuthorization.ANY_ORGA_UPDATE);
        result.put(new MultiKey(MY_ORGA, CREATE), M2MOrganizationAuthorization.MY_ORGA_UPDATE);
        result.put(new MultiKey(MY_ORGA, UPDATE), M2MOrganizationAuthorization.MY_ORGA_UPDATE);
        result.put(new MultiKey(MY_ORGA, "read"), M2MOrganizationAuthorization.MY_ORGA_READ);

        return result;

    }
}
