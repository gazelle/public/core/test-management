package net.ihe.gazelle.tm.organization;

import net.ihe.gazelle.tm.organization.exception.OrganizationServiceDAOException;
import net.ihe.gazelle.users.model.Address;
import net.ihe.gazelle.users.model.DelegatedOrganization;
import net.ihe.gazelle.users.model.Institution;
import net.ihe.gazelle.users.model.PersonFunction;

import java.util.List;
import java.util.NoSuchElementException;

public interface OrganizationServiceDAO {

    List<PersonFunction> getRequiredRolesToCompleteRegistration();

    boolean hasOrganisationBillingContact(Institution institution);

    Institution getCurrentOrganization();

    List<Institution> getAllInstitution();

    /**
     * Return the Institution by the keyword
     * @param keyword : a String of the institution keyword
     * @return an Institution
     * @throws java.util.NoSuchElementException if there is no Institution corresponding to the keyword
     */
    Institution getInstitutionWithKeyword(String keyword);

    /**
     * Return the Institution by the id
     *
     * @param id an Integer
     * @return an Institution
     * @throws java.util.NoSuchElementException if there is no Institution corresponding to the id
     */
    Institution getInstitutionById(Integer id);

    /**
     * Get the organization with the given name
     * @param name the name of the organization
     * @return the corresponding organization
     */
    Institution findOrganisationByName(String name);

    /**
     * Finds the institution by id.
     *
     * @param organizationId the id of the Delegated Organization.
     * @return the delegated organization.
     * @throws IllegalArgumentException if organizationId is null.
     * @throws NoSuchElementException   if delegated organization is not found.
     */
    DelegatedOrganization getDelegatedOrganizationById(Integer organizationId);

    /**
     * Return the delegatedOrganization that has this keyword.
     *
     * @param keyword the keyword of Delegated Organization to check.
     * @throws IllegalArgumentException if keyword of Delegated Organization is null.
     */
    DelegatedOrganization getDelegatedOrganizationByKeyword(String keyword);

    void createInstitution(Institution institution, Address address);

    /**
     * Create delegated Organization.
     *
     * @param delegatedOrganization the Delegated Organization to create.
     * @throws IllegalArgumentException if Delegated Organization is null.
     */
    void createDelegatedOrganization(DelegatedOrganization delegatedOrganization);

    /**
     * Check if organization exist.
     * @param id the id of the Organization to check.
     * @return true if the organization exist, false otherwise.
     */
    boolean isOrganizationExisting(String id);

    /**
     * Update the name of a delegated organization
     *
     * @param institution the organization to update
     * @param name        the new name
     * @return the updated organization
     * @throws IllegalArgumentException        if institution ou name is null
     * @throws OrganizationServiceDAOException if the name could not be updated
     */
    Institution updateOrganizationName(Institution institution, String name);

    /**
     * Update the name of a delegated organization
     *
     * @param institution the institution to migrate.
     * @throws OrganizationServiceDAOException if the delegated organization could not be created.
     */
    Institution migrateLocalOrganizationToDelegated(Institution institution);

    /**
     * Is there at least one organization boolean.
     *
     * @return true if one or more Organization exist, false otherwise
     */
    boolean isThereAtLeastOneOrganization();
}
