package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.script.dto;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonSetter;
import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script.Property;
import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script.Step;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JsonPropertyOrder({"id", "@type", "properties"})
public class StepDTO implements Serializable {

    private static final long serialVersionUID = -2065502630789132428L;

    @JsonIgnore
    private Step step;

    public StepDTO() {
        this.step = new Step();
    }

    public StepDTO(Step step) {
        this.step = step;
    }

    @JsonGetter("id")
    public String getId() {
        return step.getId();
    }

    @JsonSetter("id")
    public void setId(String id) {
        step.setId(id);
    }

    @JsonGetter("@type")
    public String getType() {
        return step.getType();
    }

    @JsonSetter("@type")
    public void setType(String type) {
        step.setType(type);
    }

    @JsonGetter("properties")
    public List<PropertyDTO> getProperties() {
        List<PropertyDTO> properties = new ArrayList<>();
        for (Property property : step.getProperties()) {
            properties.add(new PropertyDTO(property));
        }
        return properties;
    }

    @JsonSetter("properties")
    public void setProperties(List<PropertyDTO> propertyDTOList) {
        List<Property> properties = new ArrayList<>();
        for (PropertyDTO propertyDTO : propertyDTOList) {
            properties.add(propertyDTO.getProperty());
        }
        step.setProperties(properties);
    }

    @JsonIgnore
    public Step getStep() {
        return step;
    }

    @JsonIgnore
    public void setStep(Step step) {
        this.step = step;
    }
}
