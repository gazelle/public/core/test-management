package net.ihe.gazelle.tm.gazelletest.action;

import java.io.Serializable;

/**
 * Expected exceptions occurring during a test using an external tool when the used token is not found.
 *
 * @author nbt
 */

public class TokenNotFoundException extends RuntimeException implements Serializable {

    private static final long serialVersionUID = -5658451896017217129L;

    public TokenNotFoundException() {
    }

    public TokenNotFoundException(String s) {
        super(s);
    }

    public TokenNotFoundException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public TokenNotFoundException(Throwable throwable) {
        super(throwable);
    }
}
