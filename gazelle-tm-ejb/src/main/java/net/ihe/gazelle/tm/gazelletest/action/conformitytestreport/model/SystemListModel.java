package net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.model;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "systems")
@XmlAccessorType(XmlAccessType.NONE)
public class SystemListModel {

    @XmlElements({@XmlElement(name = "system", type = SystemModel.class)})
    private List<SystemModel> systems = new ArrayList<>();

    public List<SystemModel> getSystems() {
        return new ArrayList<>(systems);
    }

    public void setSystems(List<SystemModel> systems) {
        this.systems = new ArrayList<>(systems);
    }
}
