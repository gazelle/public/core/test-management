package net.ihe.gazelle.tm.statistics.preparation.dao;

import net.ihe.gazelle.tm.configurations.model.ConfigurationQuery;
import net.ihe.gazelle.tm.configurations.model.HostQuery;
import net.ihe.gazelle.tm.gazelletest.model.instance.MonitorInSessionQuery;
import net.ihe.gazelle.tm.gazelletest.model.instance.Status;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstanceParticipantsQuery;
import net.ihe.gazelle.tm.statistics.common.StatisticsCommonService;
import net.ihe.gazelle.tm.statistics.preparation.StatisticsForPreparationPhaseDao;
import net.ihe.gazelle.tm.systems.model.SystemActorProfilesQuery;
import net.ihe.gazelle.tm.systems.model.SystemInSessionQuery;
import net.ihe.gazelle.tm.systems.model.SystemInSessionRegistrationStatus;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.util.List;

@Scope(ScopeType.EVENT)
@AutoCreate
@Name("statisticsForPreparationPhaseDao")
public class StatisticsForPreparationPhaseDaoImpl implements StatisticsForPreparationPhaseDao {

    @In(value = "statisticsCommonService")
    StatisticsCommonService statisticsCommonService;

    @Override
    public int getNumberOfActivatedMonitorInSession(TestingSession selectedTestingSession) {
        MonitorInSessionQuery monitorInSessionQuery = new MonitorInSessionQuery();
        monitorInSessionQuery.isActivated().eq(true);
        monitorInSessionQuery.testingSession().id().eq(selectedTestingSession.getId());
        return monitorInSessionQuery.getCountDistinctOnPath();
    }

    @Override
    public int getNumberOfAcceptedSystemInSession() {
        SystemInSessionQuery systemInSessionQuery = statisticsCommonService.getSystemInSessionQuery();
        systemInSessionQuery.registrationStatus().neq(SystemInSessionRegistrationStatus.DROPPED);
        systemInSessionQuery.acceptedToSession().eq(true);
        return systemInSessionQuery.getCountDistinctOnPath();
    }

    @Override
    public int getNumberOfNotAcceptedSystemInSession() {
        SystemInSessionQuery systemInSessionQuery = statisticsCommonService.getSystemInSessionQuery();
        systemInSessionQuery.registrationStatus().neq(SystemInSessionRegistrationStatus.DROPPED);
        systemInSessionQuery.acceptedToSession().eq(false);
        return systemInSessionQuery.getCountDistinctOnPath();
    }

    @Override
    public int getNumberOfAcceptedSupportiveRequest(TestingSession selectedTestingSession) {
        SystemActorProfilesQuery systemActorProfilesQuery = getSystemActorProfilesQuery(selectedTestingSession);
        systemActorProfilesQuery.testingDepthReviewed().eq(true);
        return systemActorProfilesQuery.getCountDistinctOnPath();
    }

    @Override
    public int getNumberOfNotYetAcceptedSupportiveRequest(TestingSession selectedTestingSession) {
        SystemActorProfilesQuery systemActorProfilesQuery = getSystemActorProfilesQuery(selectedTestingSession);
        systemActorProfilesQuery.testingDepthReviewed().eq(false);
        return systemActorProfilesQuery.getCountDistinctOnPath();
    }

    @Override
    public int getNumberOfSUTNetworkConfiguration(TestingSession selectedTestingSession) {
        ConfigurationQuery configurationQuery = getConfigurationQuery(selectedTestingSession);
        return configurationQuery.getCountDistinctOnPath();
    }

    @Override
    public int getNumberOfSUTNetworkConfigurationAccepted(TestingSession selectedTestingSession) {
        ConfigurationQuery configurationQuery = getConfigurationQuery(selectedTestingSession);
        configurationQuery.isApproved().eq(true);
        return configurationQuery.getCountDistinctOnPath();
    }

    @Override
    public int getNumberOfSUTNetworkConfigurationNotAccepted(TestingSession selectedTestingSession) {
        ConfigurationQuery configurationQuery = getConfigurationQuery(selectedTestingSession);
        configurationQuery.isApproved().eq(false);
        return configurationQuery.getCountDistinctOnPath();
    }

    @Override
    public int getNumberOfGeneratedHosts(TestingSession selectedTestingSession) {
        HostQuery hostQuery = getHostQuery(selectedTestingSession);
        return hostQuery.getCountDistinctOnPath();
    }

    @Override
    public int getNumberOfGeneratedHostsWithIP(TestingSession selectedTestingSession) {
        HostQuery hostQuery = getHostQuery(selectedTestingSession);
        hostQuery.ip().isNotNull();
        return hostQuery.getCount();
    }

    @Override
    public int getNumberOfGeneratedHostsWithoutIP(TestingSession selectedTestingSession) {
        HostQuery hostQuery = getHostQuery(selectedTestingSession);
        hostQuery.ip().isNull();
        return hostQuery.getCount();
    }

    @Override
    public int getNumberOfPreparatoryTests(TestingSession selectedTestingSession) {
        TestInstanceParticipantsQuery testInstanceParticipantsQuery = getPreparatoryTestInstanceParticipantsQuery(selectedTestingSession);
        return testInstanceParticipantsQuery.testInstance().getCountDistinctOnPath();

    }

    @Override
    public List<TestInstance> getAllPreparatoryTestInstances(TestingSession selectedTestingSession) {
        TestInstanceParticipantsQuery testInstanceParticipantsQuery = getPreparatoryTestInstanceParticipantsQuery(selectedTestingSession);
        return testInstanceParticipantsQuery.testInstance().getListDistinct();
    }

    private TestInstanceParticipantsQuery getPreparatoryTestInstanceParticipantsQuery(TestingSession selectedTestingSession) {
        TestInstanceParticipantsQuery testInstanceParticipantsQuery = new TestInstanceParticipantsQuery();
        testInstanceParticipantsQuery.testInstance().testingSession().eq(selectedTestingSession);
        testInstanceParticipantsQuery.systemInSessionUser().systemInSession().system().isTool().eq(false);
        testInstanceParticipantsQuery.testInstance().lastStatus().neq(Status.ABORTED);
        testInstanceParticipantsQuery.testInstance().test().testType().keyword().eq("preparatory");
        return testInstanceParticipantsQuery;
    }
    private HostQuery getHostQuery(TestingSession selectedTestingSession) {
        HostQuery hostQuery = new HostQuery();
        hostQuery.testingSession().id().eq(selectedTestingSession.getId());
        return hostQuery;
    }

    private ConfigurationQuery getConfigurationQuery(TestingSession selectedTestingSession) {
        ConfigurationQuery configurationQuery = new ConfigurationQuery();
        configurationQuery.systemInSession().testingSession().id().eq(selectedTestingSession.getId());
        configurationQuery.systemInSession().registrationStatus().neq(SystemInSessionRegistrationStatus.DROPPED);
        return configurationQuery;
    }
    private SystemActorProfilesQuery getSystemActorProfilesQuery(TestingSession selectedTestingSession) {
        SystemActorProfilesQuery systemActorProfilesQuery = new SystemActorProfilesQuery();
        systemActorProfilesQuery.system().systemsInSession().testingSession().id().eq(selectedTestingSession.getId());
        systemActorProfilesQuery.system().systemsInSession().registrationStatus().neq(SystemInSessionRegistrationStatus.DROPPED);
        systemActorProfilesQuery.wantedTestingDepth().keyword().eq("S");
        systemActorProfilesQuery.testingDepth().keyword().eq("S");
        return systemActorProfilesQuery;
    }
}
