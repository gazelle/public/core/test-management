package net.ihe.gazelle.tm.testexecution.sorting;

public enum SutSortOrder {

    SYSTEM("systemActorProfile.system.keyword",true),
    PROFILE("systemActorProfile.actorIntegrationProfileOption.actorIntegrationProfile.integrationProfile.keyword",true),
    ACTOR("systemActorProfile.actorIntegrationProfileOption.actorIntegrationProfile.actor.keyword",true),
    OPTION("systemActorProfile.actorIntegrationProfileOption.integrationProfileOption.keyword",true),
    STATUS_ASCENDING ("status.rank",true),
    STATUS_DESCENDING ("status.rank",false);

    private final String queryPath;
    private final boolean ascending;

    SutSortOrder(java.lang.String queryPath, boolean ascending) {
        this.queryPath = queryPath;
        this.ascending = ascending;
    }

    public java.lang.String getQueryPath() {
        return queryPath;
    }

    public boolean isAscending() {
        return ascending;
    }
}
