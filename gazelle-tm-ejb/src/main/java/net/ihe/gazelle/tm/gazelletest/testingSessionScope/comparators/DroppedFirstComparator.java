package net.ihe.gazelle.tm.gazelletest.testingSessionScope.comparators;

import net.ihe.gazelle.tm.gazelletest.testingSessionScope.ProfileScope;
import net.ihe.gazelle.tm.systems.model.Testability;

import java.util.Comparator;

public class DroppedFirstComparator implements Comparator<ProfileScope> {

    @Override
    public int compare(ProfileScope profileScope1, ProfileScope profileScope2) {
        if (profileScope1.getProfileInTestingSession().getTestability() == Testability.DECISION_PENDING && profileScope2.getProfileInTestingSession().getTestability() == profileScope1.getProfileInTestingSession().getTestability()) {
            return 0;
        } else if (profileScope2.getProfileInTestingSession().getTestability() == Testability.DECISION_PENDING) {
            return -1;
        } else if (profileScope1.getProfileInTestingSession().getTestability() == Testability.DECISION_PENDING) {
            return 1;
        } else {
            return profileScope2.getProfileInTestingSession().getTestability().getRank() - profileScope1.getProfileInTestingSession().getTestability().getRank();
        }
    }
}
