package net.ihe.gazelle.tm.application.services;

import net.ihe.gazelle.tm.application.model.Section;

import java.util.List;

public interface SectionDAO {

    Section getSection(Integer sectionId);

    Section getSectionByName(String name);

    List<Section> getSections();

    List<Section> getRenderedSections();

    void removeSection(Section section);

    void updateSection(Section section);

}
