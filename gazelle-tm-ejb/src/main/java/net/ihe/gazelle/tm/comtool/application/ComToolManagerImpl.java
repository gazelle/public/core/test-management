package net.ihe.gazelle.tm.comtool.application;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.ssov7.gum.client.application.User;
import net.ihe.gazelle.tf.model.IntegrationProfile;
import net.ihe.gazelle.tm.comtool.interlay.ComToolRestClientImpl;
import net.ihe.gazelle.tm.gazelletest.model.instance.MonitorInSession;
import net.ihe.gazelle.tm.gazelletest.model.instance.Status;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.systems.model.System;
import net.ihe.gazelle.tm.systems.model.TestingSession;

import net.ihe.gazelle.users.model.Institution;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Name("comToolManager")
@AutoCreate
@Scope(ScopeType.EVENT)
public class ComToolManagerImpl implements ComToolManager {
   private static final Logger LOG = LoggerFactory.getLogger(ComToolManagerImpl.class);

   @In
   private ApplicationPreferenceManager applicationPreferenceManager;

   @In
   private ComToolRestClient comToolRestClient;

   @In
   private ComToolManagerDAO comToolManagerDAO;

   @In(value = "gumUserService")
   private UserService userService;

   @Override
   @Transactional
   public boolean isGctEnabled() {
      LOG.debug("isGctEnabled");
      Boolean enabled = applicationPreferenceManager.getBooleanValue("gct_enabled");
      return Boolean.TRUE.equals(enabled) && isGctUrlDefined();
   }

   @Override
   @Transactional
   public void updateTestingSessionsUsersAndChannels() {
      if (isGctEnabled()) {
         LOG.debug("updateTestingSessions");
         List<ComToolRestClient.TestingSessionUpdate> updates = new ArrayList<>();
         for (TestingSession testingSession : comToolManagerDAO.getOpenAndActiveTestingSessions()) {
            updates.add(buildTestingSessionUpdate(testingSession));
         }
         comToolRestClient.updateTestingSessionsUsersAndChannels(updates);
      }
   }

   @Override
   public void archiveTestRunChannel(TestInstance testInstance, String testStatus,
                                     String lastTestStatus) {
      if (isGctEnabled()) {
         LOG.debug("archiveTestRunChannel");
         comToolRestClient.archiveTestRunChannel(testInstance, testStatus, lastTestStatus);
      }
   }

   @Override
   public void unarchiveTestRunChannel(TestInstance testInstance, String testStatus, String lastTestStatus) {
      if (isGctEnabled()) {
         LOG.debug("unarchiveTestRunChannel");
         comToolRestClient.unarchiveTestRunChannel(testInstance, testStatus, lastTestStatus);
      }
   }

   @Override
   public String getInvitationLink(TestInstance testInstance) {
      return isGctEnabled() ? comToolRestClient.getInvitationLink(testInstance) : null;
   }

   @Override
   public void testRunStatusChanged(TestInstance testInstance, Status oldStatus, Status newStatus) {
      if (isGctEnabled()) {
         LOG.debug("testRunStatusChanged");
         String newStatusString = String.valueOf(newStatus);
         String comment = "Status changed to " + newStatusString;
         comToolRestClient.sendTestRunMessage(testInstance, comment);
         if (isArchived(newStatus) != isArchived(oldStatus)) {
            String oldStatusString = String.valueOf(oldStatus);
            if (isArchived(newStatus)) {
               comToolRestClient.archiveTestRunChannel(testInstance, newStatusString, oldStatusString);
               comToolRestClient.sendTestRunMessage(testInstance, "Channel is set as read only due to new status");
            } else {
               comToolRestClient.unarchiveTestRunChannel(testInstance, newStatusString, oldStatusString);
            }
         }
      }
   }

   @Override
   public void testRunMonitorChanged(TestInstance testInstance, MonitorInSession oldMonitor,
                                     MonitorInSession newMonitor) {
      if (isGctEnabled()) {
         LOG.debug("testRunMonitorChanged");
         if (isMonitorDefined(oldMonitor)) {
            comToolRestClient.removeUserFromTestRunChannel(testInstance, userService.getUserById(oldMonitor.getUserId()));
         }
         String comment;
         if (isMonitorDefined(newMonitor)) {
            comToolRestClient.addMonitorToTestRunChannel(testInstance, userService.getUserById(newMonitor.getUserId()));
            comment = "Monitor changed to " + newMonitor.getUserId();
         } else {
            comment = "No more monitor for this test run";
         }
         comToolRestClient.sendTestRunMessage(testInstance, comment);
      }
   }

   @Override
   public void sendTestRunComment(TestInstance testInstance, String username, String inputComment) {
      if (isGctEnabled()) {
         LOG.debug("sendTestRunMessage");
         String comment;
         if (username != null) {
            comment = username + " added a comment : " + inputComment;
         } else {
            comment = "New comment : " + inputComment;
         }
         comToolRestClient.sendTestRunMessage(testInstance, comment);
      }
   }

   @Override
   @Transactional
   public void createTestRunChannel(TestInstance testInstance, User creator) {
      if (isGctEnabled()) {
         LOG.debug("createTestRunChannel");
         List<System> systems = comToolManagerDAO.getTestRunSystems(testInstance);
         comToolRestClient.createTestRunChannel(testInstance, systems, userService.getUserById(creator.getId()));
      }
   }

   private boolean isGctUrlDefined() {
      String gctUrl = comToolRestClient.getGctUrl();
      if (gctUrl == null || gctUrl.isEmpty()) {
         LOG.error("Communication-tool Gateway URL is null/empty.");
         return false;
      }
      return true;
   }

   private ComToolRestClient.TestingSessionUpdate buildTestingSessionUpdate(TestingSession testingSession) {
      LOG.debug("Communication-tool provision update for testing-session {}", testingSession);
      List<User> participants = getAcceptedParticipants(testingSession);
      return new ComToolRestClient.TestingSessionUpdate(
            testingSession,
            participants,
            comToolManagerDAO.getActiveMonitors(testingSession),
            comToolManagerDAO.getActiveAdmins(testingSession),
            asInstitutionsUsers(participants),
            new ArrayList<IntegrationProfile>() // [ceoche] Disabled, too many profile channel were created.
      );
   }

   private List<User> getAcceptedParticipants(TestingSession testingSession) {
      LOG.debug("getUsers for {}", testingSession);
      if (Boolean.TRUE.equals(testingSession.getContinuousSession())) {
         // all participants are added to continuous testing sessions, regardless system acceptance.
         // FIXME [ceoche] verify this condition, this is strange to me
         return comToolManagerDAO.getActiveParticipants(testingSession);
      } else {
         return comToolManagerDAO.getActiveAndAcceptedParticipants(testingSession);
      }
   }

   private List<ComToolRestClient.InstitutionUsers> asInstitutionsUsers(List<User> participants) {
      LOG.debug("getInstitutionsUsers");
      Map<String, ComToolRestClient.InstitutionUsers> map = new HashMap<>();
      for (User user : participants) {
         if (map.containsKey(Institution.findInstitutionWithKeyword(user.getOrganizationId()).getName())) {
            map.get(Institution.findInstitutionWithKeyword(user.getOrganizationId()).getKeyword()).getUsers().add(user);
         } else {
            map.put(
                    Institution.findInstitutionWithKeyword(user.getOrganizationId()).getKeyword(),
                  new ComToolRestClientImpl.InstitutionUsers( Institution.findInstitutionWithKeyword(user.getOrganizationId()), Arrays.asList(user))
            );
         }
      }
      return new ArrayList<>(map.values());
   }

   private List<IntegrationProfile> getProfiles(TestingSession testingSession) {
      LOG.debug("getProfiles for {}", testingSession);
      // retrieve all profiles for testing session
      return testingSession.getIntegrationProfiles();
   }

   private boolean isMonitorDefined(MonitorInSession oldMonitor) {
      return oldMonitor != null && oldMonitor.getUserId() != null;
   }

   private boolean isArchived(Status status) {
      return status.equals(Status.VERIFIED) ||
            status.equals(Status.BROKEN) ||
            status.equals(Status.SELF_VERIFIED) ||
            status.equals(Status.ABORTED) ||
            status.equals(Status.FAILED);
   }

}