package net.ihe.gazelle.tm.users.action;

import net.ihe.gazelle.preferences.PreferenceService;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Calendar;
import java.util.Date;


/**
 * This class is used to keep the method related to user logout countdown from the now deleted class UserManagerExtra
 * @since 10.0.0 (GUM step 4)
 */
@Name("userActionDoneBean")
@AutoCreate
@Scope(ScopeType.SESSION)
public class UserActionDoneBean {
    private Date lastUserAction;

    public void userDoneAction() {
        lastUserAction = new Date();
    }

    public long getLogoutDateTime() {
        if (lastUserAction != null) {
            Calendar c = Calendar.getInstance();
            c.setTime(lastUserAction);
            Integer sessionTimeout = PreferenceService.getInteger("use_messages_session_timeout");
            if (sessionTimeout == null) {
                sessionTimeout = 40;
            }
            c.add(Calendar.MINUTE, sessionTimeout);
            Date logoutDateTime = c.getTime();
            if (logoutDateTime.before(new Date())) {
                FacesContext context = FacesContext.getCurrentInstance();
                ExternalContext ec = context.getExternalContext();
                HttpServletRequest request = (HttpServletRequest) ec.getRequest();
                HttpSession session = request.getSession(false);
                if (session != null) {
                    org.jboss.seam.web.Session.instance().invalidate();
                }
                return 0;
            }
            return logoutDateTime.getTime();
        } else {
            return 0;
        }
    }

}
