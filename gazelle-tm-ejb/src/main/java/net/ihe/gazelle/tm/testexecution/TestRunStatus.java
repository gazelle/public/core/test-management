package net.ihe.gazelle.tm.testexecution;

public enum TestRunStatus {
    RUNNING("started", "gazelle.tm.testing.status.running", "fa gzl-icon-play-blue", 1),
    PAUSED("paused", "gazelle.tm.testing.status.paused", "fa gzl-icon-pause", 2),
    PARTIALLY_VERIFIED("partially verified", "gazelle.tm.testing.status.partiallyVerified", "fa gzl-icon-partially-verified", 3),
    CRITICAL("critical", "gazelle.tm.testing.status.critical", "fa gzl-icon-critical-orange", 4),
    TO_BE_VERIFIED("completed", "gazelle.tm.testing.status.completed", "fa gzl-icon-waiting", 5),
    CLAIMED("", "Claimed", "fa gzl-icon-eye", 6),
    VERIFIED("verified", "gazelle.tm.testing.status.verified", "fa gzl-icon-checked", 7),
    FAILED("failed", "gazelle.tm.testing.status.failed", "fa gzl-icon-ban-red", 8),
    COMPLETED_ERRORS("completed errors", "gazelle.tm.testing.status.completedwitherrors", "fa gzl-icon-ban-red", 9),
    SELF_VERIFIED("self verified", "gazelle.tm.testing.status.selfVerified", "fa gzl-icon-checked", 10),
    ABORTED("aborted", "gazelle.tm.testing.status.aborted", "gzl-icon-trash-o", 15),
    UNSPECIFIED("", "Unspecified", "", 100);

    private String keyword;
    private String labelToDisplay;
    private String cssStyle;
    private int rank;

    TestRunStatus(String keyword, String labelToDisplay, String cssStyle, int rank) {
        this.keyword = keyword;
        this.labelToDisplay = labelToDisplay;
        this.cssStyle = cssStyle;
        this.rank = rank;
    }


    public static TestRunStatus getStatusByKeyword(String keyword) {
        if (keyword != null) {
            for (TestRunStatus testRunStatus : values()){
                if (testRunStatus.getKeyword().equals(keyword)) {
                    return testRunStatus;
                }
            }
        }
        return UNSPECIFIED;
    }

    public String getKeyword() {
        return keyword;
    }

    public String getLabelToDisplay() {
        return labelToDisplay;
    }

    public String getCssStyle() {
        return cssStyle;
    }

    public String getStatusStyleClass() {
        return cssStyle;
    }

    public int getRank() {
        return rank;
    }
}
