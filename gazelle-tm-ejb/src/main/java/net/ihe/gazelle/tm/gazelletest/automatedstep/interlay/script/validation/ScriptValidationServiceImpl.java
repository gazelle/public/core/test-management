package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.script.validation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script.Script;
import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script.ScriptParsingException;
import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script.ScriptValidationException;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.script.dto.ScriptDTO;
import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.IOException;
import java.io.InputStream;

@Name("scriptValidationService")
@Scope(ScopeType.STATELESS)
@AutoCreate
public class ScriptValidationServiceImpl implements ScriptValidationService {

    private static final String JSON_SCHEMA_FILE = "/automation/automationScriptSchema.json";

    @Override
    public String validate(String jsonString) {
        try (InputStream inputStream = getClass().getResourceAsStream(JSON_SCHEMA_FILE)) {
            if (inputStream == null) {
                throw new ScriptValidationException("Unable to load automation script schema");
            }
            JSONObject rawSchema = new JSONObject(new JSONTokener(inputStream));
            SchemaLoader loader = SchemaLoader.builder()
                    .schemaJson(rawSchema)
                    .draftV7Support()
                    .build();
            Schema schema = loader.load().build();
            schema.validate(new JSONObject(jsonString));
            return null;
        } catch (IOException e) {
            throw new ScriptValidationException("Unable to load automation script schema", e);
        } catch (ValidationException e) {
            StringBuilder result = new StringBuilder(e.getMessage());
            if (!e.getCausingExceptions().isEmpty()) {
                result.append(" : \n");
                for (ValidationException ve : e.getCausingExceptions()) {
                    result.append(ve.getMessage()).append(" \n");
                }
            }
            return result.toString();
        }
    }

    @Override
    public Script parse(String jsonString) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return (mapper.readValue(jsonString, ScriptDTO.class)).getScript();
        } catch (JsonProcessingException e) {
            throw new ScriptParsingException("Unable to parse uploaded script", e);
        }
    }
}
