package net.ihe.gazelle.tm.testexecution.sorting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum SUTCapabilityReportSortMethod {
    SYSTEM("gazelle.configuration.system.system",
            Arrays.asList(SutSortOrder.SYSTEM, SutSortOrder.PROFILE, SutSortOrder.ACTOR, SutSortOrder.OPTION)),
    PROFILE("gazelle.testmanagement.object.Profile",
            Arrays.asList(SutSortOrder.PROFILE, SutSortOrder.ACTOR, SutSortOrder.OPTION, SutSortOrder.SYSTEM)),
    ACTOR("gazelle.tf.Actor",
            Arrays.asList(SutSortOrder.ACTOR, SutSortOrder.PROFILE, SutSortOrder.OPTION, SutSortOrder.SYSTEM)),
    OPTION("gazelle.tf.IntegrationProfileOptionAcronym",
            Arrays.asList(SutSortOrder.OPTION, SutSortOrder.PROFILE, SutSortOrder.ACTOR, SutSortOrder.SYSTEM)),
    STATUS_PASSED_FIRST("net.ihe.gazelle.tm.PassedFirst",
            Arrays.asList(SutSortOrder.STATUS_ASCENDING, SutSortOrder.SYSTEM, SutSortOrder.PROFILE, SutSortOrder.ACTOR, SutSortOrder.OPTION)),
    STATUS_TO_BE_COMPLETED_FIRST("net.ihe.gazelle.tm.ToBeCompletedFirst",
            Arrays.asList(SutSortOrder.STATUS_DESCENDING, SutSortOrder.SYSTEM, SutSortOrder.PROFILE, SutSortOrder.ACTOR, SutSortOrder.OPTION));

    private final String labelToDisplay;
    /**
     * List of all sorts to aggregate.
     * The order of values is important to sort on multiple attributes.
     */
    private final List<SutSortOrder> sortOrders;

    SUTCapabilityReportSortMethod(String labelToDisplay, List<SutSortOrder> sortOrders) {
        this.labelToDisplay = labelToDisplay;
        this.sortOrders = sortOrders != null
                ? new ArrayList<>(sortOrders)
                : new ArrayList<SutSortOrder>();
    }

    public String getLabelToDisplay() {
        return labelToDisplay;
    }

    public List<SutSortOrder> getSortOrders() {
        return new ArrayList<>(sortOrders);
    }

}
