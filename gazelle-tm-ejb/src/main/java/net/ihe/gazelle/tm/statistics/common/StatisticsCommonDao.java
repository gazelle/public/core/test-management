package net.ihe.gazelle.tm.statistics.common;

import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstanceParticipantsQuery;
import net.ihe.gazelle.tm.systems.model.ProfileInTestingSession;
import net.ihe.gazelle.tm.systems.model.SystemInSessionQuery;
import net.ihe.gazelle.tm.systems.model.TestingSession;

import java.util.List;


public interface StatisticsCommonDao {


    TestInstanceParticipantsQuery getTestInstanceParticipantsQuery(TestingSession selectedTestingSession);

    SystemInSessionQuery getSystemInSessionQuery(TestingSession selectedTestingSession);

    List<ProfileInTestingSession> getAllProfileInTestingSessionForCurrentSession(TestingSession selectedTestingSession);

    int getNumberOfSessionParticipants(TestingSession selectedTestingSession);

    int getNumberOfRegisteredInstitutionWithAttendee(TestingSession selectedTestingSession);

    int getNumberOfRegisteredInstitutionWithSystem(TestingSession selectedTestingSession);

    int getNumberOfRegisteredSystemInSession(TestingSession selectedTestingSession);

    int getNumberOfDomainForCurrentSession(TestingSession selectedTestingSession);

    int getNumberOfIntegrationProfilesForCurrentSession(TestingSession testingSession);
}
