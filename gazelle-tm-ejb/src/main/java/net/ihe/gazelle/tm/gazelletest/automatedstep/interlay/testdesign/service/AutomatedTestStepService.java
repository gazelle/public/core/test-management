package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.testdesign.service;

import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script.Script;
import net.ihe.gazelle.tm.gazelletest.domain.TestStepDomain;

public interface AutomatedTestStepService {

    /**
     * Create an automated test step for test definition, and write the corresponding automation script
     * @param testStep the domain object representing the test step to be persisted
     * @param script the script to be written in file system
     * @return the domain object updated with the auto incremented id after creation
     */
    TestStepDomain createTestStep(TestStepDomain testStep, Script script);

    /**
     * Edit an existing automated test step, it will delete and rewrite the linked automation script
     * @param testStep the domain object representing the test step to be edited
     * @param newScriptName the new name of the script edited
     * @param script the script to be written in file system
     * @return the domain object updated after edition
     */
    TestStepDomain editTestStep(TestStepDomain testStep, String newScriptName, Script script);


    TestStepDomain copyTestStep(TestStepDomain automatedStepCopy, Integer stepToCopyId);

    /**
     * Delete an existing automated test step and its linked automation script
     * @param testStep the domain object representing the test step to be deleted
     */
    void deleteTestStep(TestStepDomain testStep);

    /**
     * Read a script from the test step id and script name
     * @param stepId the id of the test step
     * @param scriptName the name of the script
     * @return The domain representation of the script
     */
    Script readScript(Integer stepId, String scriptName);
}
