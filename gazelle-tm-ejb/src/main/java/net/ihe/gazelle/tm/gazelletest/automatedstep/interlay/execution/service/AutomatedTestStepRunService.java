package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.execution.service;

import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.tm.gazelletest.action.InvalidTokenException;
import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.instance.AutomatedTestStepInstanceData;
import net.ihe.gazelle.tm.gazelletest.domain.AutomatedTestStepExecution;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestStepsInstance;

import java.util.Map;

public interface AutomatedTestStepRunService {

    /**
     * Execute an automated test step run
     * @param identity The identity of the user who ran the test step
     * @param stepInstance The test step instance running
     * @throws StepRunServiceException if a maestro client exception is thrown
     */
    void run(GazelleIdentity identity, TestStepsInstance stepInstance) throws StepRunServiceException ;

    /**
     * Cancel an automated test step run
     * @param testStepInstanceId The id of the test step instance running
     */
    void cancelExecution(Integer testStepInstanceId);

    /**
     * Reception of an execution result
     * @param identity The Identity logged in
     * @param execution The execution result and report received
     * @throws InvalidTokenException if the received token is invalid or malformed
     * @throws NotRunningException if the execution was not in running status anymore
     * @throws StepRunServiceException if the identity is not logged in
     */
    void receiveExecution(GazelleIdentity identity, AutomatedTestStepExecution execution);

    /**
     * Get an execution from the is of the test step instance linked to it
     * @param testStepInstanceId The id of the test step instance linked to the execution
     * @return The execution filtered with those criteria
     */
    AutomatedTestStepExecution getExecution(Integer testStepInstanceId);

    /**
     * Test if an execution is currently running
     * @param execution The execution to test
     * @return True if it is actually running
     */
    boolean isStepRunning(AutomatedTestStepExecution execution);

    /**
     * Test if the execution of an automated test step is up-to-date compared to the latest uploaded inputs
     * @param stepInstance The test step instance linked to the execution
     * @return The map of all inputData paired its up-to-date status
     */
    Map<AutomatedTestStepInstanceData, Boolean> getUpToDateStatusForAllInstanceInput(TestStepsInstance stepInstance);

    /**
     * Test if the execution of an automated test step is up-to-date compared to the latest uploaded inputs
     * @param testStepInstanceId The id of the test step instance linked to the execution
     * @return True if the execution is not up-to-date
     */
    boolean isRunOutDated(Integer testStepInstanceId);
}
