package net.ihe.gazelle.tm.session;

import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.tm.users.model.ConnectathonParticipant;
import net.ihe.gazelle.users.model.Institution;

import java.util.List;

public interface AttendeeServiceDAO {

    /**
     *
     * @param session Session instance
     * @param institution Institution instance
     * @return List of participants in a session of an institution
     * @throws IllegalArgumentException if session or institution are null
     */
    List<ConnectathonParticipant> getRegisteredAttendees(TestingSession session, Institution institution);

    /**
     *
     * @param sessionId Session id
     * @param organizationId Institution id
     * @return List of participants in a session of an institution
     * @throws java.util.NoSuchElementException if session or institution are not found by id
     */
    List<ConnectathonParticipant> getRegisteredAttendees(Integer sessionId, Integer organizationId);

    /**
     *
     * @param session Session instance
     * @param institution Institution instance
     * @return List of participants in a session of an institution
     * @throws IllegalArgumentException if session or institution are null
     */
    long getNumberOfRegisteredAttendees(TestingSession session, Institution institution);

    /**
     *
     * @param institution Institution instance
     * @return number of active attendees in an institution
     * @throws IllegalArgumentException if institution are null
     */
    long getNumberOfActiveMembers(Institution institution);


    /**
     *
     * @param institution Institution instance
     * @return number of active attendees in an institution
     * @throws IllegalArgumentException if institution are null
     */
    long getNumberOfPendingMembers(Institution institution);




}
