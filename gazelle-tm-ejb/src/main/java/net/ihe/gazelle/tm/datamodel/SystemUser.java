package net.ihe.gazelle.tm.datamodel;

import net.ihe.gazelle.ssov7.gum.client.application.User;
import net.ihe.gazelle.tm.systems.model.SystemInSession;

public class SystemUser {
    private User user;
    private SystemInSession systemInSession;

    public SystemUser(User user, SystemInSession systemInSession) {
        this.user = user;
        this.systemInSession = systemInSession;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public SystemInSession getSystemInSession() {
        return systemInSession;
    }

    public void setSystemInSession(SystemInSession systemInSession) {
        this.systemInSession = systemInSession;
    }
}
