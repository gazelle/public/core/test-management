package net.ihe.gazelle.tm.session;

import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.tm.users.model.ConnectathonParticipant;
import net.ihe.gazelle.users.model.Institution;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.util.List;

@Name("attendeeService")
@Scope(ScopeType.STATELESS)
public class AttendeeServiceImpl implements AttendeeService {

    @In(value = "attendeesServiceDAO", create = true)
    private AttendeeServiceDAO attendeeServiceDAO;

    @Override
    public List<ConnectathonParticipant> getRegisteredAttendees(TestingSession session, Institution institution) {
        if(session == null || institution == null){
            throw new IllegalArgumentException("Testing session and Institution must be defined");
        }
        return attendeeServiceDAO.getRegisteredAttendees(session, institution);
    }

    @Override
    public List<ConnectathonParticipant> getRegisteredAttendees(Integer sessionId, Integer organizationId) {
        if(sessionId == null || organizationId == null){
            throw new IllegalArgumentException("Testing session Id and Institution Id must be defined");
        }
       return attendeeServiceDAO.getRegisteredAttendees(sessionId,organizationId);
    }

    @Override
    public long getNumberOfRegisteredAttendees(TestingSession testingSession, Institution institution) {
        if(testingSession == null || institution == null){
            throw new IllegalArgumentException("Testing session and Institution must be defined");
        }
        return attendeeServiceDAO.getNumberOfRegisteredAttendees(testingSession, institution);
    }

    @Override
    public long getNumberOfActiveMembers(Institution institution) {
        if(institution == null){
            throw new IllegalArgumentException("Institution must be defined");
        }
        return attendeeServiceDAO.getNumberOfActiveMembers(institution);
    }

    @Override
    public long getNumberOfPendingMembers(Institution institution) {
        if(institution == null){
            throw new IllegalArgumentException("Institution must be defined");
        }
        return attendeeServiceDAO.getNumberOfPendingMembers(institution);
    }
}
