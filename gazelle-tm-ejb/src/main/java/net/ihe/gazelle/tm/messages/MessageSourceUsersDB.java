package net.ihe.gazelle.tm.messages;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.ssov7.gum.client.interlay.factory.UserServiceFactory;
import net.ihe.gazelle.ssov7.gum.client.application.User;

import java.util.*;

public abstract class MessageSourceUsersDB<T> implements MessageSource<T> {

    protected UserService userService;

    protected MessageSourceUsersDB() {
        this(UserServiceFactory.getUserService());
    }

    protected MessageSourceUsersDB(UserService userService) {
        this.userService = userService;
    }

    @Override
    public List<User> getUsers(String userId, T sourceObject, String[] messageParameters) {
        EntityManagerService.provideEntityManager();

        Collection<String> pathsToUser = getPathsToUsername();
        Set<String> userIdList = new HashSet<>();
        for (String pathToUser : pathsToUser) {
            HQLQueryBuilder<T> queryBuilder = new HQLQueryBuilder<>(getSourceClass());
            queryBuilder.addEq("id", getId(sourceObject, messageParameters));

            List<?> values = queryBuilder.getListDistinct(pathToUser);
            for (Object value : values) {
                if (value instanceof String) {
                    userIdList.add((String) value);
                }
            }
        }

        if (userId != null) {
            userIdList.remove(userId);
        }

        List<User> users = new ArrayList<>();
        for (String id : userIdList) {
            users.add(userService.getUserById(id));
        }
        return users;
    }

    /**
     * @param sourceObject
     * @return the id of sourceObject
     */
    public abstract Integer getId(T sourceObject, String[] messageParameters);

    /**
     * Paths from T to the usernames
     */
    public abstract Collection<String> getPathsToUsername();

    /**
     * @return T class
     */
    public abstract Class<T> getSourceClass();

}
