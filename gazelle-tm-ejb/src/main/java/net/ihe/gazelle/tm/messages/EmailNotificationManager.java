package net.ihe.gazelle.tm.messages;

import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.ssov7.gum.client.application.User;
import net.ihe.gazelle.ssov7.gum.client.application.UserPreference;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserPreferencesService;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.tm.messages.model.Message;
import net.ihe.gazelle.tm.messages.model.MessageUser;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.users.action.EmailManagerLocal;
import net.ihe.gazelle.users.action.EmailType;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.In;

import java.io.IOException;
import java.io.Serializable;

/**
 * Send notification by email
 */
public class EmailNotificationManager implements Serializable, NotificationListener {

    private static final long serialVersionUID = 1621387439421020188L;
    private final MessageFormatter messageFormatter;

    public EmailNotificationManager(MessageFormatter messageFormatter) {
        this.messageFormatter = messageFormatter;
    }

    @In(value = "userPreferencesService")
    transient UserPreferencesService userPreferencesService;

    /**
     * Send notification by email to all concerned user
     *
     * @param message message to send
     */
    @Override
    public void sendNotification(Message message) {
        TestingSession testingSession = message.getTestingSession();
        if (testingSession != null && testingSession.getSendNotificationByEmail()) {
            EmailManagerLocal emailManager = (EmailManagerLocal) Component.getInstance("emailManager");
            UserService userService = (UserService) Component.getInstance("gumUserService");

            User author = userService.getUserById(message.getAuthor());
            for (MessageUser recipientId : message.getUsers()) {
                User recipient = userService.getUserById(recipientId.getUserId());
                UserPreference userPreference = getUserPreferencesService().getUserPreferencesByUserId(recipientId.getUserId());
                if (userPreference != null && userPreference.isNotifiedByEmail()) {
                    emailManager.setConcerned(author);
                    emailManager.setRecipient(recipient);
                    emailManager.setMessage(messageFormatter.getPreferredLangFormattedMessage(message));
                    emailManager.setTestingSession(testingSession);
                    emailManager.setLink(getFormattedLink(message));
                    try {
                        emailManager.sendEmail(EmailType.TO_USER_NOTIFICATION);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private String getFormattedLink(Message message) {
        String link = message.getLink();
        if (link != null && link.toLowerCase().startsWith("http")) {
            return link;
        } else {
            return PreferenceService.getString("application_url") + link;
        }
    }

    private UserPreferencesService getUserPreferencesService() {
        if (userPreferencesService != null)
            return userPreferencesService;
        else
            return (UserPreferencesService) Component.getInstance("userPreferencesService");
    }


    /**
     * Reload userPreferencesService in case of deserialization.
     *
     * @param inputStream InputStream of the UserValueFormatter.
     * @throws IOException
     * @throws ClassNotFoundException
     * @see Serializable
     */
    private void readObject(java.io.ObjectInputStream inputStream) throws IOException, ClassNotFoundException {
        inputStream.defaultReadObject();
        this.userPreferencesService = (UserPreferencesService) Component.getInstance("userPreferencesService");
    }
}
