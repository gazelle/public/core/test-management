package net.ihe.gazelle.tm.organization.exception;

public class OrganizationServiceException extends RuntimeException{
    public OrganizationServiceException() {
        super();
    }

    public OrganizationServiceException(String message) {
        super(message);
    }

    public OrganizationServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public OrganizationServiceException(Throwable cause) {
        super(cause);
    }
}
