package net.ihe.gazelle.tm.application.gui;

import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.gum.client.application.Role;
import net.ihe.gazelle.tm.application.action.ApplicationManager;
import net.ihe.gazelle.tm.application.action.SectionsTypePage;
import net.ihe.gazelle.tm.application.model.Section;
import net.ihe.gazelle.tm.application.services.SectionService;
import net.ihe.gazelle.tm.gazelletest.action.UserManagementURIService;
import net.ihe.gazelle.tm.organization.OrganizationService;
import net.ihe.gazelle.tm.session.TestingSessionService;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.Identity;

import javax.faces.context.FacesContext;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Name("homeBeanGui")
@AutoCreate
@Scope(ScopeType.EVENT)
public class HomeBeanGui implements Serializable {


    private static final long serialVersionUID = 5931436677551756972L;

    public SectionService getSectionService() {
        return sectionService;
    }

    @In(create = true)
    private SectionService sectionService;

    @In(value = "organizationService")
    private transient OrganizationService organizationService;

    public boolean isTestManagement() {
        return applicationManager.isTestManagement();
    }

    public boolean isMasterModel() {
        return applicationManager.isMasterModel();
    }

    public boolean isProductRegistry() {
        return applicationManager.isProductRegistry();
    }

    @In(create = true)
    private ApplicationManager applicationManager;

    @In
    GazelleIdentity identity;

    private Section selectedSection;

    private String copyrightDate;


    private enum ApplicationType {
        TM,
        PR,
        GMM
    }

    @In(value = "testingSessionService")
    private transient TestingSessionService testingSessionService;

    @In(value = "userManagementURIService")
    private transient UserManagementURIService userManagementURIService;


    /**
     * Get displayed sections, and filter tool index section
     *
     * @return {@link List<Section>} Filtered sessions
     */

    public List<Section> getDisplayedSectionsWithoutTools() {
        List<Section> sectionsWithoutToolIndex = new ArrayList<>();
        for (Section section : sectionService.getRenderedSections()) {
            if (!section.getSectionType().getName().equals(SectionsTypePage.TOOL_INDEX.getValue())) {
                sectionsWithoutToolIndex.add(section);
            }
        }
        if (identity.isLoggedIn() && applicationManager.isTestManagement()) {
            if (testingSessionService.isTestingSessionRunning(testingSessionService.getUserTestingSession())) {
                return filterSectionForRoles(sectionsWithoutToolIndex, SectionsTypePage.ANNOUNCEMENT,
                        Role.MONITOR, Role.ADMIN, Role.TESTING_SESSION_ADMIN, Role.VENDOR, Role.VENDOR_ADMIN);

            }
            if (testingSessionService.isAfterSessionEnd(testingSessionService.getUserTestingSession())) {
                return filterSectionForRoles(sectionsWithoutToolIndex, SectionsTypePage.ANNOUNCEMENT,
                        Role.ADMIN, Role.TESTING_SESSION_ADMIN, Role.VENDOR, Role.VENDOR_ADMIN);
            }

        }


        return filterSectionForRoles(sectionsWithoutToolIndex, SectionsTypePage.ANNOUNCEMENT,
                Role.VENDOR, Role.VENDOR_ADMIN, Role.ADMIN, Role.TESTING_SESSION_ADMIN);
    }

    public boolean isTestManager() {
        return applicationManager.isTestManagement();
    }

    public boolean showAnnouncement() {
        return (!applicationManager.isTestManagement() && identity.isLoggedIn());
    }

    public Section getToolIndexSection() {
        if (applicationManager.isTestManagement()) {
            return sectionService.getSectionByName("ToolIndex");
        }
        return null;
    }

    public Section getDocumentationSection() {
        if (applicationManager.isTestManagement()) {
            return sectionService.getSectionByName("Documentation");
        }
        return null;
    }

    public Section getAnnouncementSection() {
        if (applicationManager.isTestManagement()) {
            return sectionService.getSectionByName("Announcement");
        }
        return null;
    }

    public List<Section> getSections() {
        return sectionService.getSections();
    }

    public String displaySection(Section section) {
        return "/administration/displaySection.seam?id=" + section.getId();
    }

    public Section getSelectedSection() {
        return selectedSection;
    }

    public void setSelectedSection(Section selectedSection) {
        this.selectedSection = selectedSection;
    }

    // Not used yet
    public void deleteSelectedSection(Section section) {
        if (selectedSection != null) {
            this.sectionService.removeSection(section);
            FacesMessages.instance().add(StatusMessage.Severity.INFO,
                    "Section '" + section.getName() + "' deleted.");
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                    "There was no Section selected for deletion!");
        }
    }


    // Not used yet
    public String createNewSection() {
        return "/administration/displaySection.seam";
    }

    public String getLayoutToRender(Section section) {
        SectionsTypePage sectionsTypePage = SectionsTypePage.of(section.getSectionType().getName());
        return sectionsTypePage.getPage();
    }

    public String getCopyRightDate() {
        if (copyrightDate == null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            copyrightDate = String.valueOf(calendar.get(Calendar.YEAR));
        }
        return copyrightDate;
    }

    /**
     * Test if the application need to create an Admin user.
     *
     * @return true if the application need to create an Admin user, false otherwise
     * @see Pages
     */
    public boolean isItNeededToCreateFirstAdminUser() {
        return !organizationService.isThereAtLeastOneOrganization();
    }


    private boolean checkApplicationType(ApplicationType applicationType) {
        if (applicationType.equals(ApplicationType.TM)) {
            return applicationManager.isTestManagement();
        }
        if (applicationType.equals(ApplicationType.PR)) {
            return applicationManager.isProductRegistry();
        }
        return applicationManager.isMasterModel();
    }


    private boolean checkVendorAndVendorAdminRole() {
        return Identity.instance().hasRole(Role.VENDOR) ||
                Identity.instance().hasRole(Role.VENDOR_ADMIN);
    }

    private boolean checkOneOfRoles(String... roleNames) {
        for (String roleName : roleNames) {
            if (Identity.instance().hasRole(roleName)) {
                return true;
            }
        }
        return false;
    }

    // keep all sections only if we are in a case of one of "roles"
    private List<Section> filterSectionForRoles(List<Section> sections, SectionsTypePage sectionsTypePage,
                                                String... roleNames) {
        List<Section> filtered = new ArrayList<>();
        for (Section section : sections) {
            if (!section.getSectionType().getName()
                    .equals(sectionsTypePage.getValue()) || !checkOneOfRoles(roleNames)) {
                filtered.add(section);
            }
        }
        return filtered;
    }

    public String getAnnouncementContent() {
        Section announcement = getAnnouncementSection();
        if (announcement == null) {
            return "";
        }
        return announcement.getContent();
    }

    public void setAnnouncementContent(String content) {
        getAnnouncementSection().setContent(content);
    }

    public void redirectToGum() throws IOException {
        FacesContext.getCurrentInstance().getExternalContext().redirect(userManagementURIService.getUserManagementURI());
    }
}
