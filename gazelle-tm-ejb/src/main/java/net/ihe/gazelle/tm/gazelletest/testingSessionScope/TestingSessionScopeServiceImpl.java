package net.ihe.gazelle.tm.gazelletest.testingSessionScope;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.tf.model.Actor;
import net.ihe.gazelle.tf.model.ActorIntegrationProfile;
import net.ihe.gazelle.tf.model.IntegrationProfile;
import net.ihe.gazelle.tf.model.IntegrationProfileOption;
import net.ihe.gazelle.tm.gazelletest.model.instance.StatusResults;
import net.ihe.gazelle.tm.gazelletest.model.instance.SystemAIPOResultForATestingSession;
import net.ihe.gazelle.tm.session.TestingSessionService;
import net.ihe.gazelle.tm.systems.model.ProfileInTestingSession;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.Testability;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.tm.testexecution.SUTCapabilityReportService;
import net.ihe.gazelle.tm.testexecution.SUTCapabilityStatus;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.faces.context.FacesContext;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Name(value = "testingSessionScopeService")
@AutoCreate
@Scope(ScopeType.STATELESS)
public class TestingSessionScopeServiceImpl implements TestingSessionScopeService {

    @In(value = "testingSessionScopeDao")
    TestingSessionScopeDao testingSessionScopeDao;
    @In(value = "SUTCapabilityReportService")
    SUTCapabilityReportService sutCapabilityReportService;
    @In(value = "testingSessionService")
    TestingSessionService testingSessionService;

    @Override
    public Filter<ProfileInTestingSession> getFilter(Map<String, String> requestParameterMap) {
        return testingSessionScopeDao.getFilter(requestParameterMap);
    }

    @Override
    public List<ProfileScope> getProfileScopes(Filter<ProfileInTestingSession> filter) {
        FilterDataModel<ProfileInTestingSession> datamodel = new FilterDataModel<ProfileInTestingSession>(filter) {
            @Override
            protected Object getId(ProfileInTestingSession profileInTestingSession) {
                return profileInTestingSession.getId();
            }
        };

        List<ProfileScope> profileScopes = new ArrayList<>();
        for (ProfileInTestingSession profileInTestingSession : datamodel.getAllItems(FacesContext.getCurrentInstance())) {
            ProfileScope profileScope = new ProfileScope();
            profileScope.setProfileInTestingSession(profileInTestingSession);

            profileScopes.add(profileScope);
        }
        return profileScopes;
    }

    @Override
    public String getFormattedLastChangedDateForProfileScope(ProfileScope profileScope) {
        ProfileInTestingSession profileInTestingSession = profileScope.getProfileInTestingSession();
        if(profileInTestingSession.getLastChanged()!=null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");
            return dateFormat.format(profileInTestingSession.getLastChanged());
        }
        return "no date available";
    }

    @Override
    public int getMaxProfileCoverageForProfileScope(ProfileScope profileScope) {
        return getProfileCoverageForProfileScope(profileScope).size();
    }

    @Override
    public int getCurrentProfileCoverageForProfileScope(ProfileScope profileScope) {
        int totalCoverage = 0;
        for (ProfileCoverage profileCoverage : getProfileCoverageForProfileScope(profileScope)){
            if (!profileCoverage.getImplementedActorInSystems().isEmpty()){
                totalCoverage++;
            }
        }
        return totalCoverage;
    }

    @Override
    public void updateProfileInTestingSession(ProfileInTestingSession profileInTestingSession) {
        testingSessionScopeDao.updateProfileInTestingSession(profileInTestingSession);
    }

    @Override
    public List<ProfileCoverage> getProfileCoverageForProfileScope(ProfileScope profileScope) {

        if (profileScope.getActorCoverages() == null) {
            List<ProfileCoverage> profileCoverages = new ArrayList<>();
            for (ActorIntegrationProfile actorIntegrationProfile : profileScope.getProfileInTestingSession().getIntegrationProfile().getActorIntegrationProfiles()) {
                if (actorIntegrationProfile != null) {
                    profileCoverages.add(constructActorReport(profileScope.getProfileInTestingSession().getIntegrationProfile(), actorIntegrationProfile.getActor()));
                }
            }
            profileScope.setActorCoverages(profileCoverages);
        }
        return profileScope.getActorCoverages();
    }


    private ProfileCoverage constructActorReport(IntegrationProfile integrationProfile, Actor actor) {
        ProfileCoverage profileCoverage = new ProfileCoverage();
        profileCoverage.setActorName(actor.getName());

        List<ImplementedActorInSystem> implementedActorInSystems = new ArrayList<>();
        List<SystemInSession> systemInSessions = testingSessionScopeDao.getSystemsInSessionByActorAndProfile(
                integrationProfile, actor, testingSessionService.getUserTestingSession());

        for (SystemInSession systemInSession : systemInSessions) {
            if (systemInSession != null) {
                implementedActorInSystems.add(constructImplementedActorInSystem(systemInSession, actor, integrationProfile));
            }
        }

        profileCoverage.setImplementedActorInSystems(implementedActorInSystems);
        return profileCoverage;
    }

    private ImplementedActorInSystem constructImplementedActorInSystem(SystemInSession systemInSession, Actor actor, IntegrationProfile integrationProfile) {
        ImplementedActorInSystem implementedActorInSystem = new ImplementedActorInSystem();

        implementedActorInSystem.setSystemKeyword(systemInSession.getSystem().getKeyword());
        implementedActorInSystem.setOrganizationName(systemInSession.getSystem().getUniqueInstitution().getName());


        List<String> profileOptionNames = new ArrayList<>();

        for (IntegrationProfileOption integrationProfileOption : testingSessionScopeDao.getProfileOptionsBySAIP(systemInSession, actor, integrationProfile)) {
            profileOptionNames.add(integrationProfileOption.getName());
        }
        implementedActorInSystem.setProfileOptions(profileOptionNames);

        implementedActorInSystem.setAcceptedToSession(systemInSession.getAcceptedToSession());
        implementedActorInSystem.setRegistrationStatus(systemInSession.getRegistrationStatus());
        return implementedActorInSystem;
    }


    @Override
    public List<SystemAIPOResultForATestingSession> getSystemAIPOResultByTestability(Testability testability, TestingSession testingSession){
        return testingSessionScopeDao.getSystemAIPOResultByTestability(testability, testingSession);
    }

    @Override
    public void autoEvaluate(TestingSession session, Testability testability, SUTCapabilityStatus evaluationStatus) {
        List<SystemAIPOResultForATestingSession> aipoResults = getSystemAIPOResultByTestability(testability, session);
        if(!aipoResults.isEmpty()) {
            StatusResults status = StatusResults.getStatusByKeyword(evaluationStatus.getKeyword());
            for (SystemAIPOResultForATestingSession aipoResult:
                 aipoResults) {
                aipoResult.setStatus(status);
                aipoResult.setComment("Auto-evaluated : profile is " + testability.getKeyword());
            }
        }
        sutCapabilityReportService.updateEvaluation(aipoResults);
    }
}
