package net.ihe.gazelle.tm.monitor.gui;


import net.ihe.gazelle.ssov7.gum.client.application.UserPreference;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserPreferencesService;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.international.LocaleSelector;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@AutoCreate
@Name(value = "monitorInfoBeanGui")
@Scope(ScopeType.EVENT)
public class MonitorInfoBeanGui {

    @In(value = "userPreferencesService")
    private UserPreferencesService userPreferencesService;

    @In(value = "gumUserService")
    private UserService userService;

    private UserPreference userPreference;

    public String getTableLabel() {
        return userPreference.getTableLabel();
    }

    public UserPreference getUserPreferenceByUserId(String userId) {
        if (userPreference != null && userPreference.getUserId().equals(userId))
            return userPreference;

        // Check is the user existing without throw exception otherwise there is an error in application.
        if (userService.getUserDisplayNameWithoutException(userId).equals("Unknown user")) {
            UserPreference newUserPreference = new UserPreference();
            newUserPreference.setUserId(userId);
            userPreference = newUserPreference;
        } else {
            userPreference = userPreferencesService.getUserPreferencesByUserId(userId);
            List<String> languagesSpokenDisplayLabel = getLanguagesSpokenDisplayLabel(userPreference);
            userPreference.setLanguagesSpoken(languagesSpokenDisplayLabel);
        }

        return userPreference;
    }

    public UserPreference getUserPreference() {
        return userPreference;
    }


    public void setTableLabel(String tableLabel) {
        userPreference.setTableLabel(tableLabel);
        userPreferencesService.updateUserPreferences(userPreference.getUserId(), userPreference);
    }

    private List<String> getLanguagesSpokenDisplayLabel(UserPreference userPreference) {
        List<String> languagesSpokenDisplayLabel = new ArrayList<>();
        for (String languages : userPreference.getLanguagesSpoken()) {
            Locale locale = new Locale(languages);
            languagesSpokenDisplayLabel.add(locale.getDisplayLanguage(getLocale()));
        }
        return languagesSpokenDisplayLabel;
    }

    public static Locale getLocale() {
        if (Contexts.isSessionContextActive()) {
            LocaleSelector selector = LocaleSelector.instance();
            return selector.getLocale();
        } else {
            return Locale.getDefault();
        }
    }
}
