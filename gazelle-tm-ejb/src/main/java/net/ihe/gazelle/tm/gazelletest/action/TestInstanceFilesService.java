package net.ihe.gazelle.tm.gazelletest.action;

import net.ihe.gazelle.tm.gazelletest.dao.TestInstanceDataDAO;
import net.ihe.gazelle.tm.gazelletest.model.instance.Status;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstanceToken;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestStepsData;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import java.io.IOException;

@Name("testInstanceFilesService")
@Scope(ScopeType.STATELESS)
public class TestInstanceFilesService {

    @In(create = true)
    private TestInstanceDataDAO testInstanceDataDAO;

    private static final Logger LOGGER = LoggerFactory.getLogger(TestInstanceFilesService.class);

    @In(create = true, value = "testInstanceTokenService")
    private TestInstanceTokenService testInstanceTokenService;

    private static final String TEST_REPORT_XSD_LOCATION = "test-report/test-report.xsd";

    /*
     * Methods to Save Logs File
     */

    /**
     * Save a logs file into a TestInstance by Token Value
     *
     * @param tokenValue String uuid token
     * @param bytesInput byte[]
     * @throws InvalidTokenException         (Expired Token, Null or Empty token Value, Invalid Token)
     * @throws EmptyFileException            (File is empty or null)
     * @throws IOException                   ()
     * @throws TestInstanceNotFoundException (No test Instance found)
     * @throws TestInstanceClosedException   ( Test Instance closed, forbidden to modify)
     * @throws SecurityException             ()
     */
    public void saveTestLogsInTestInstance(String tokenValue, byte[] bytesInput) throws InvalidTokenException, EmptyFileException,
            IOException, TestInstanceNotFoundException, TestInstanceClosedException, SecurityException {
        checkInputFile(bytesInput);
        try {
            TestInstanceToken testInstanceToken = checkAndGetTestInstanceTokenByTokenValue(tokenValue);
            Integer testInstanceIdentifier = testInstanceToken.getTestStepsInstanceId();
            checkTestInstance(testInstanceIdentifier);
            saveTestLogs(bytesInput, testInstanceIdentifier, testInstanceToken.getUserId(), "test-logs-" + tokenValue + ".zip");

        } catch (TokenNotFoundException e) {
            throw new InvalidTokenException("Invalid Token");
        }
    }

    /**
     * Save logs file
     *
     * @param instanceId id of the Test
     * @param bytesInput byte[]
     * @param userId     id of the user
     * @param fileName   name of the file to save
     * @throws TestInstanceNotFoundException if the test instance is not found
     * @throws IOException                   ()
     */
    private void saveTestLogs(byte[] bytesInput, Integer instanceId, String userId, String fileName) throws TestInstanceNotFoundException,
            IOException {

        TestStepsData testStepData = testInstanceDataDAO.instantiateTestStepDataFile(userId, fileName);
        testStepData = testInstanceDataDAO.createTestStepData(testStepData);
        testInstanceDataDAO.saveFileInTestStepsData(testStepData.getId(), bytesInput);
        try {
            testInstanceDataDAO.updateTestInstanceAndTestStepDataForLogs(testStepData, instanceId, userId);
        } catch (TestInstanceNotFoundException e) {
            throw new TestInstanceNotFoundException("Test instance does not exist anymore, can not save logs");
        }

    }


    /*
     * Methods to save TestReport File
     */

    /**
     * Save a report file into a TestInstance by Token Value
     *
     * @param tokenValue String uuid token
     * @param bytesInput byte[]
     * @throws InvalidTokenException         (Expired Token, Null or Empty token Value, Invalid Token)
     * @throws EmptyFileException            (File is empty or null)
     * @throws IOException                   ()
     * @throws TestInstanceNotFoundException (No test Instance found)
     * @throws TestInstanceClosedException   ( Test Instance closed, forbidden to modify)
     * @throws SecurityException             ()
     */
    public void saveTestReportInTestInstance(String tokenValue, byte[] bytesInput) throws InvalidTokenException, EmptyFileException,
            IOException, TestInstanceNotFoundException, TestInstanceClosedException, SecurityException, InvalidTestReportException {
        checkInputFile(bytesInput);

        try {
            TestInstanceToken testInstanceToken = checkAndGetTestInstanceTokenByTokenValue(tokenValue);
            Integer testInstanceIdentifier = testInstanceToken.getTestStepsInstanceId();
            checkTestInstance(testInstanceIdentifier);
            validateTestReportAgainstXSD(bytesInput);
            saveTestReport(bytesInput, testInstanceIdentifier, testInstanceToken.getUserId(), "test-report-" + tokenValue + ".xml");

        } catch (TokenNotFoundException e) {
            throw new InvalidTokenException("Token invalid", e);
        }
    }

    /**
     * Save the report
     *
     * @param instanceId id of the Test
     * @param bytesInput byte[]
     * @param userId     id of the user
     * @param fileName   name of the file to save
     * @throws TestInstanceNotFoundException if the test instance is not found
     * @throws IOException                   ()
     */
    private void saveTestReport(byte[] bytesInput, Integer instanceId, String userId, String fileName) throws TestInstanceNotFoundException,
            IOException {

        TestStepsData testStepData = this.testInstanceDataDAO.instantiateTestStepDataFile(userId, fileName);
        testStepData = testInstanceDataDAO.createTestStepData(testStepData);
        testInstanceDataDAO.saveFileInTestStepsData(testStepData.getId(), bytesInput);
        try {
            testInstanceDataDAO.updateTestInstanceAndAddTestStepData(testStepData, instanceId, userId);

        } catch (TestInstanceNotFoundException e) {
            throw new TestInstanceNotFoundException("Test instance does not exist anymore, can not save report");
        }
    }

    public boolean validateTestReportAgainstXSD(byte[] bytesInput) throws InvalidTestReportException {
        try {
            TestReportXSDValidator validator = new TestReportXSDValidator(TEST_REPORT_XSD_LOCATION);
            XSDErrorHandler errorHandler = validator.validateResource(new String(bytesInput));
            if (errorHandler.isErrors()) {
                LOGGER.error("Invalid report, errors in XSD Validation");
                throw new InvalidTestReportException("Errors in the report");
            }
            LOGGER.info("Report is validated");
            return true;
        } catch (SAXException | IOException e) {
            throw new InvalidTestReportException("Cannot Validate File", e);
        }

    }

    /*
     * Common methods
     */

    /**
     * Function to check if the InputFile is empty or null
     *
     * @param bytesInput File to check
     * @throws EmptyFileException if file is empty or null
     */
    private void checkInputFile(byte[] bytesInput) throws EmptyFileException {
        if (bytesInput == null || bytesInput.length <= 0) {
            throw new EmptyFileException("Test report or test logs is empty");
        }
        LOGGER.info("InputFile is not Empty");

    }

    private void checkTestInstance(Integer testInstanceId) throws TestInstanceNotFoundException, TestInstanceClosedException {
        TestInstance testInstance = testInstanceDataDAO.getTestInstance(testInstanceId);
        if (isTestInstanceFinished(testInstance)) {
            throw new TestInstanceClosedException("Test Instance or TestingSession is Closed");
        }
        LOGGER.info("TestInstance is available");

    }


    /**
     * Retrieve The TestInstanceToken By Token Value
     *
     * @param tokenValue String uuid token
     * @return Valid TestInstance matching to the unique TokenValue
     * @throws InvalidTokenException  If the TokenValidity is expired
     * @throws TokenNotFoundException If the token Value doesn't match any TestInstanceToken
     */
    private TestInstanceToken checkAndGetTestInstanceTokenByTokenValue(String tokenValue) throws InvalidTokenException, TokenNotFoundException {
        if (StringUtils.isBlank(tokenValue)) {
            LOGGER.error("Invalid token either empty or null");
            throw new InvalidTokenException("Invalid token either empty or null");
        }
        TestInstanceToken testInstanceToken = testInstanceTokenService.getTokenByValue(tokenValue);
        if (!testInstanceTokenService.isTokenValid(testInstanceToken)) {
            LOGGER.error("Invalid expired token");
            throw new InvalidTokenException("Invalid Token");
        }
        LOGGER.info("Token value is OK");
        return testInstanceToken;
    }


    /**
     * The TestInstance status is finished and the testingSession is closed
     *
     * @param testInstance TestInstance to verify
     * @return boolean
     */
    private boolean isTestInstanceFinished(TestInstance testInstance) {
        Status status = testInstance.getLastStatus();
        TestingSession testingSession = testInstance.getTestingSession();
        Boolean sessionClosed = testingSession.getSessionClosed();
        return (status.equals(Status.getSTATUS_VERIFIED())
                || status.equals(Status.getSTATUS_ABORTED())
                || status.equals(Status.getSTATUS_FAILED()) || sessionClosed);
    }

    /*
     * Methods to prepare Tests
     */

    /**
     * Set test instance token DAO
     *
     * @param testInstanceTokenService object
     */
    public void setTestInstanceTokenService(TestInstanceTokenService testInstanceTokenService) {
        this.testInstanceTokenService = testInstanceTokenService;
    }

    /**
     * Set test instance data DAO
     *
     * @param testInstanceDataDAO object
     */
    public void setTestInstanceDataDAO(TestInstanceDataDAO testInstanceDataDAO) {
        this.testInstanceDataDAO = testInstanceDataDAO;
    }


}
