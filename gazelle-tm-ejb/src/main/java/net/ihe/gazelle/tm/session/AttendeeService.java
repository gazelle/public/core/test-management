package net.ihe.gazelle.tm.session;

import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.tm.users.model.ConnectathonParticipant;
import net.ihe.gazelle.users.model.Institution;
import org.jboss.seam.annotations.In;

import java.util.List;

public interface AttendeeService {

    List<ConnectathonParticipant> getRegisteredAttendees(TestingSession session, Institution institution);

    List<ConnectathonParticipant> getRegisteredAttendees(Integer sessionId, Integer organizationId);

    long getNumberOfRegisteredAttendees(TestingSession testingSession, Institution institution);

    long getNumberOfActiveMembers(Institution institution);

    long getNumberOfPendingMembers(Institution institution);

}
