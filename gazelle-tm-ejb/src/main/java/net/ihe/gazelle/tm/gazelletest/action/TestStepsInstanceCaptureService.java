package net.ihe.gazelle.tm.gazelletest.action;

import net.ihe.gazelle.tm.gazelletest.model.instance.TestStepsInstance;

/**
 * This service is used for managing date of capture for TestStepsInstance
 * @see TestStepsInstance
 */
public interface TestStepsInstanceCaptureService {

    /**
     * Set the capture start date of a TestStepsInstance
     * @param testStepsInstance the TestStepsInstance that starts capturing
     * @throws IllegalArgumentException if testStepsInstance is null
     */
    void startCapture(TestStepsInstance testStepsInstance);

    /**
     * Set the capture end date of a TestStepsInstance
     * @param testStepsInstance the TestStepsInstance that stops capturing
     * @throws IllegalArgumentException if testStepsInstance is null
     * @throws IllegalStateException if the start date of testStepsInstance is null (.i.e the capture never started)
     */
    void endCapture(TestStepsInstance testStepsInstance);

    /**
     * Restart the capture dates for a given TestStepsInstance
     * @param testStepsInstance the TestStepsInstance that restarts capturing
     * @throws IllegalArgumentException if testStepsInstance is null
     */
    void restartCapture(TestStepsInstance testStepsInstance);

    /**
     * Extend the capture by keeping the start date and resetting the end date
     * @param testStepsInstance the TestStepsInstance that extends capturing
     * @throws IllegalArgumentException if testStepsInstance is null
     * @throws IllegalStateException if the start date of testStepsInstance is null (.i.e the capture never started)
     */
    void extendCapture(TestStepsInstance testStepsInstance);
}
