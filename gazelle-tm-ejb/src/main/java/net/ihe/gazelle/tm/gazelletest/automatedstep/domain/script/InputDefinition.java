package net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script;

public class InputDefinition {

    private String key;
    private String label;
    private String format;
    private String type;
    private boolean required;

    public InputDefinition() {
        // empty for serialization
    }

    public InputDefinition(String key, String label, String format, String type, boolean required) {
        this.key = key;
        this.label = label;
        this.format = format;
        this.type = type;
        this.required = required;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean getRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }
}
