package net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script;

public class ScriptParsingException extends RuntimeException {

    public ScriptParsingException() {
    }

    public ScriptParsingException(String message) {
        super(message);
    }

    public ScriptParsingException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public ScriptParsingException(Throwable throwable) {
        super(throwable);
    }
}
