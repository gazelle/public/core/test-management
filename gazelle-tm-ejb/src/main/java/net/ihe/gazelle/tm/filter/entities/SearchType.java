package net.ihe.gazelle.tm.filter.entities;

import net.ihe.gazelle.common.filter.list.ListQueryBuilder;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.HQLRestriction;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.objects.model.ObjectInstance;
import net.ihe.gazelle.tm.datamodel.MonitorUser;
import net.ihe.gazelle.tm.gazelletest.model.definition.Test;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.session.TestingSessionService;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.Component;
import org.jboss.seam.core.ResourceBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public enum SearchType implements SearchEntity {

    SAMPLE("gazelle.tm.search.sample", "spl", ObjectInstance.class),
    SYSTEM("gazelle.tm.search.system", "sys", SystemInSession.class),
    TEST_INSTANCE("gazelle.tm.search.testInstance", "ti", TestInstance.class),
    TEST("gazelle.tm.test.Test", "t", Test.class),
    MONITOR("gazelle.tm.search.monitor", "m", MonitorUser.class);

    private static final Logger LOG = LoggerFactory.getLogger(SearchType.class);
    private String label;
    private Class<?> entityClass;
    private String shortId;

    private final transient TestingSessionService testingSessionService = (TestingSessionService) Component.getInstance("testingSessionService");

    SearchType(String label, String shortId, Class<?> entityClass) {
        this.label = label;
        this.shortId = shortId;
        this.entityClass = entityClass;
    }

    public static void addListRestrictions(HQLQueryBuilder<?> queryBuilder, List<HQLRestriction> restrictions) {
        if (restrictions.size() == 1) {
            queryBuilder.addRestriction(restrictions.get(0));
        } else if (!restrictions.isEmpty()) {
            HQLRestriction[] array = restrictions.toArray(new HQLRestriction[restrictions.size()]);
            queryBuilder.addRestriction(HQLRestrictions.or(array));
        }
    }

    public static void tryAddId(List<HQLRestriction> restrictions, String query) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("tryAddId");
        }
        try {
            Integer id = Integer.parseInt(query);
            restrictions.add(HQLRestrictions.eq("id", id));
        } catch (NumberFormatException e) {
            // no restriction...
        }
    }

    @Override
    public boolean addRestrictionsForQuery(HQLQueryBuilder queryBuilder, String query) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("addRestrictionsForQuery");
        }
        List<HQLRestriction> restrictions;
        TestingSession selectedTestingSession = testingSessionService.getUserTestingSession();
        switch (this) {
            case SAMPLE:
                restrictions = new ArrayList<>();
                SearchType.tryAddId(restrictions, query);
                restrictions.add(HQLRestrictions.like("name", query));
                restrictions.add(HQLRestrictions.like("description", query));
                restrictions.add(HQLRestrictions.like("object.description", query));
                SearchType.addListRestrictions(queryBuilder, restrictions);
                queryBuilder.addEq("system.testingSession", selectedTestingSession);
                return true;
            case SYSTEM:
                restrictions = new ArrayList<>();
                SearchType.tryAddId(restrictions, query);
                restrictions.add(HQLRestrictions.like("system.keyword", query));
                restrictions.add(HQLRestrictions.like("system.name", query));
                SearchType.addListRestrictions(queryBuilder, restrictions);
                queryBuilder.addEq("testingSession", selectedTestingSession);
                return true;
            case TEST:
                restrictions = new ArrayList<>();
                SearchType.tryAddId(restrictions, query);
                restrictions.add(HQLRestrictions.like("name", query));
                restrictions.add(HQLRestrictions.like("keyword", query));
                restrictions.add(HQLRestrictions.like("testDescription.description", query));
                SearchType.addListRestrictions(queryBuilder, restrictions);
                return true;
            case TEST_INSTANCE:
                try {
                    Integer id = Integer.parseInt(query);
                    queryBuilder.addEq("id", id);
                } catch (NumberFormatException e) {
                    return false;
                }
                return true;
            default:
                return false;
        }
    }

    @Override
    public Class getEntityClass() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getEntityClass");
        }
        return entityClass;
    }

    public List<MonitorUser> searchMonitor(String query, int maxCount, List<MonitorUser> monitorUserDataModels) {
        ListQueryBuilder<MonitorUser> monitorQueryBuilder = new ListQueryBuilder<>(monitorUserDataModels);
        try {
            Integer id = Integer.parseInt(query);
            monitorQueryBuilder.addEq("monitorInSession.id", id);
            return monitorQueryBuilder.getList();
        } catch (NumberFormatException e) {
            // no restriction...
        }
        List<MonitorUser> firstResult = parameterizedSearch(new ListQueryBuilder<>(monitorUserDataModels), "user.firstName", query, maxCount);
        List<MonitorUser> secondResult = parameterizedSearch(new ListQueryBuilder<>(monitorUserDataModels), "user.lastName", query, maxCount);
        firstResult.addAll(secondResult);
        return firstResult;
    }

    public List<MonitorUser> parameterizedSearch(ListQueryBuilder<MonitorUser> monitorQueryBuilder, String searchParameter, String query, int maxCount) {
        monitorQueryBuilder.addLike(searchParameter, query);
        monitorQueryBuilder.addEq("monitorInSession.testingSession", testingSessionService.getUserTestingSession());
        monitorQueryBuilder.addEq("monitorInSession.isActivated", true);
        monitorQueryBuilder.setMaxResults(maxCount);
        return monitorQueryBuilder.getList();
    }

    public List<?> search(String query, int maxCount, List<MonitorUser> monitorUserDataModels) {
        if (this == SearchType.MONITOR) {
            return searchMonitor(query, maxCount, monitorUserDataModels);
        }
        HQLQueryBuilder queryBuilder = new HQLQueryBuilder(getEntityClass());
        boolean process = addRestrictionsForQuery(queryBuilder, query);
        if (process) {
            queryBuilder.setMaxResults(maxCount);
            return queryBuilder.getList();
        } else {
            return new ArrayList<>();
        }
    }

    public String getLabel() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getLabel");
        }
        return ResourceBundle.instance().getString(label);
    }

    public String getShortId() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getShortId");
        }
        return shortId;
    }

    private HQLRestriction addPatientIdRestriction(String query) {
        // different parts of the patient ID :
        // identifier^^^domainKeyword&domainRoot&typeCode
        String[] split1 = StringUtils.split(query, '&');
        if (split1.length == 3) {
            String[] split2 = StringUtils.splitByWholeSeparator(split1[0], "^^^");
            if (split2.length == 2) {
                String identifier = split2[0];
                String domainKeyword = split2[1];
                String domainRoot = split1[1];
                String typeCode = split1[2];
                return HQLRestrictions.and(addEq("identifiers.identifier", identifier),
                        addEq("identifiers.authority.keyword", domainKeyword),
                        addEq("identifiers.authority.rootOID", domainRoot),
                        addEq("identifiers.typeCode.keyword", typeCode));
            }
        }

        return null;
    }

    private HQLRestriction addEq(String path, String value) {
        if (value.equals("null")) {
            return HQLRestrictions.isNull(path);
        } else {
            return HQLRestrictions.eq(path, value);
        }
    }

}
