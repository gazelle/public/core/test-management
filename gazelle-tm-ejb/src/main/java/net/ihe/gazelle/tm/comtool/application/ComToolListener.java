package net.ihe.gazelle.tm.comtool.application;

import net.ihe.gazelle.messaging.MessagePropertyChanged;
import net.ihe.gazelle.messaging.MessagingProvider;
import net.ihe.gazelle.tm.gazelletest.model.instance.MonitorInSession;
import net.ihe.gazelle.tm.gazelletest.model.instance.Status;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstanceEvent;
import org.apache.commons.lang.StringEscapeUtils;
import org.jboss.seam.Component;
import org.kohsuke.MetaInfServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@MetaInfServices(MessagingProvider.class)
public class ComToolListener implements MessagingProvider {

    private static final Logger LOG = LoggerFactory.getLogger(ComToolListener.class);

    @Override
    public void receiveMessage(Object message) {
        LOG.debug("receiveMessage");
        if (message instanceof MessagePropertyChanged) {
            MessagePropertyChanged messagePropertyChanged = (MessagePropertyChanged) message;
            if (messagePropertyChanged.getObject() instanceof TestInstance) {
                TestInstance testInstance = (TestInstance) messagePropertyChanged.getObject();
                if (messagePropertyChanged.getPropertyName().equals("comment")) {
                    handleComment(
                            testInstance,
                            (TestInstanceEvent) messagePropertyChanged.getNewValue()
                    );
                } /*else if (messagePropertyChanged.getPropertyName().equals("lastStatus")) {
                    handleStatus(
                            testInstance,
                            (Status) messagePropertyChanged.getOldValue(),
                            (Status) messagePropertyChanged.getNewValue()
                    );
                }*/ else if (messagePropertyChanged.getPropertyName().equals("monitor")) {
                    handleMonitor(
                            testInstance,
                            (MonitorInSession) messagePropertyChanged.getOldValue(),
                            (MonitorInSession) messagePropertyChanged.getNewValue()
                    );
                }
            }
        }
    }

    private void handleComment(TestInstance testInstance, TestInstanceEvent event) {
        ComToolManager comToolManager = getCommToolManager();

        String inputComment = event.getComment();

        String comment = StringEscapeUtils.unescapeHtml(inputComment);
        comment = comment.replace("<br/>", "\r\n");

        String email = event.getUserId();

        comToolManager.sendTestRunComment(testInstance, email, comment);
    }

    private void handleStatus(TestInstance testInstance, Status oldStatus, Status newStatus) {
        ComToolManager comToolManager = getCommToolManager();
        comToolManager.testRunStatusChanged(testInstance, oldStatus, newStatus);
    }

    private void handleMonitor(TestInstance testInstance, MonitorInSession oldMonitor, MonitorInSession newMonitor) {
        ComToolManager comToolManager = getCommToolManager();
        comToolManager.testRunMonitorChanged(testInstance, oldMonitor, newMonitor);
    }

    private ComToolManager getCommToolManager() {
        return (ComToolManager) Component.getInstance("comToolManager");
    }

}
