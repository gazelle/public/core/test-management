package net.ihe.gazelle.tm.preparation.network.dao;

import net.ihe.gazelle.tm.configurations.model.ConfigurationQuery;
import net.ihe.gazelle.tm.configurations.model.HostQuery;
import net.ihe.gazelle.tm.preparation.network.SutNetworkConfigurationDao;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.SystemInSessionQuery;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.users.model.Institution;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.util.List;

@Scope(ScopeType.STATELESS)
@AutoCreate
@Name("sutNetworkConfigurationDao")
public class SutNetworkConfigurationDaoImpl implements SutNetworkConfigurationDao {

    @Override
    public int getNumberOfNetworkConfigurationsApprovedForASystemId(int systemInSessionId) {
        ConfigurationQuery configurationQuery = new ConfigurationQuery();
        configurationQuery.systemInSession().id().eq(systemInSessionId);
        configurationQuery.isApproved().eq(true);
        return configurationQuery.getCount();
    }

    @Override
    public int getNumberOfNetworkConfigurationsNotApprovedForASystemId(int systemInSessionId) {
        ConfigurationQuery configurationQuery = new ConfigurationQuery();
        configurationQuery.systemInSession().id().eq(systemInSessionId);
        configurationQuery.isApproved().eq(false);
        return configurationQuery.getCount();
    }

    @Override
    public int getNumberOfNetworkConfigurationsHosts(TestingSession selectedTestingSession) {
        HostQuery hostQuery = new HostQuery();
        hostQuery.testingSession().eq(selectedTestingSession);
        hostQuery.institution().eq(Institution.getLoggedInInstitution());

        return hostQuery.getCount();
    }

    @Override
    public int getNumberOfNetworkConfigurationsHostsNoIp(TestingSession selectedTestingSession) {
        HostQuery hostQuery = new HostQuery();
        hostQuery.testingSession().eq(selectedTestingSession);
        hostQuery.institution().eq(Institution.getLoggedInInstitution());
        hostQuery.ip().isNull();
        return hostQuery.getCount();
    }

    @Override
    public List<SystemInSession> getListSystemInSessionOfCurrenUser(TestingSession selectedTestingSession) {
        SystemInSessionQuery systemInSessionQuery = new SystemInSessionQuery();
        systemInSessionQuery.acceptedToSession().eq(true);
        systemInSessionQuery.testingSession().eq(selectedTestingSession);
        systemInSessionQuery.system().institutionSystems().institution().eq(Institution.getLoggedInInstitution());

        return systemInSessionQuery.getList();
    }
}
