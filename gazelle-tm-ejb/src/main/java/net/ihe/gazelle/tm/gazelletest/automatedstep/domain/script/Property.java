package net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script;

public class Property {

    private String key;
    private String value;

    public Property() {
        // empty for serialization
    }

    public Property(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
