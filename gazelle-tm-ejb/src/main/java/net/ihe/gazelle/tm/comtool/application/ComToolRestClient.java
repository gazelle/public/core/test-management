package net.ihe.gazelle.tm.comtool.application;

import net.ihe.gazelle.ssov7.gum.client.application.User;
import net.ihe.gazelle.tf.model.IntegrationProfile;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.users.model.Institution;
import net.ihe.gazelle.tm.systems.model.System;

import javax.ejb.Local;
import java.util.ArrayList;
import java.util.List;

@Local
public interface ComToolRestClient {

	void addMonitorToTestRunChannel(TestInstance testInstance, User user);

	void archiveTestRunChannel(TestInstance testInstance, String testStatus, String lastTestStatus);

	void createTestRunChannel(TestInstance testInstance, List<System> systems, User creator);

	String getGctUrl();

	String getInvitationLink(TestInstance testInstance);

	void removeUserFromTestRunChannel(TestInstance testInstance, User user);

	void sendTestRunMessage(TestInstance testInstance, String testMsg);

	void unarchiveTestRunChannel(TestInstance testInstance, String testStatus, String lastTestStatus);

	void updateTestingSessionUsersAndChannels(ComToolRestClient.TestingSessionUpdate testingSessionUpdate);

	void updateTestingSessionsUsersAndChannels(List<ComToolRestClient.TestingSessionUpdate> testingSessionUpdateList);

	class InstitutionUsers {
		 private final Institution institution;
		 private final List<User> users = new ArrayList<>();

		 public InstitutionUsers(Institution institution, List<User> users) {
			  this.institution = institution;
			  this.users.addAll(users);
		 }

		 public Institution getInstitution() {
			  return institution;
		 }

		 public List<User> getUsers() {
			  return users;
		 }
	}

	class TestingSessionUpdate {
		 private final TestingSession testingSession;
		 private final List<User> users;
		 private final List<User> monitors;
		 private final List<User> admins;
		 private final List<InstitutionUsers> institutions;
		 private final List<IntegrationProfile> integrationProfiles;

		 public TestingSessionUpdate(TestingSession testingSession,
											  List<User> users,
											  List<User> monitors,
											  List<User> admins,
											  List<InstitutionUsers> institutions,
											  List<IntegrationProfile> integrationProfiles) {
			  this.testingSession = testingSession;
			  this.users = users;
			  this.monitors = monitors;
			  this.admins = admins;
			  this.institutions = institutions;
			  this.integrationProfiles = integrationProfiles;
		 }

		 public TestingSession getTestingSession() {
			  return testingSession;
		 }

		 public List<User> getMonitors() {
			  return monitors;
		 }

		 public List<User> getUsers() {
			  return users;
		 }

		 public List<User> getAdmins() {
			  return admins;
		 }

		 public List<InstitutionUsers> getInstitutions() {
			  return institutions;
		 }

		 public List<IntegrationProfile> getIntegrationProfiles() {
			  return integrationProfiles;
		 }
	}
}
