package net.ihe.gazelle.tm.configurations.action.converter;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.tf.model.ActorIntegrationProfileOption;
import net.ihe.gazelle.tf.model.ActorIntegrationProfileOptionQuery;
import net.ihe.gazelle.tm.configurations.model.*;

import javax.persistence.EntityManager;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Convert OIDRootDefinition to XML and back.
 */
public class OIDRootDefinitionToXmlConverter {

    /**
     * Convert a list of OID Root Definition to an XML representation.
     *
     * @param oidRootDefinitions list of OID Root Definitions to convert.
     * @return the literal representation of the list.
     * @throws JAXBException if the marshalling cannot be done.
     */
    public String oidRootDefinitionToXml(List<OIDRootDefinition> oidRootDefinitions) throws JAXBException {
        OIDRootDefinitionIE oidRootDefinitionIE = new OIDRootDefinitionIE(oidRootDefinitions);
        JAXBContext jaxbContext = JAXBContext.newInstance(OIDRootDefinition.class, OIDRootDefinitionIE.class, OIDRootDefinitionLabel.class,
                OIDRequirement.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        StringWriter sw = new StringWriter();
        jaxbMarshaller.marshal(oidRootDefinitionIE, sw);
        return sw.toString();
    }

    /**
     * Unmarshall an XML string to a {@link OIDRootDefinitionIE} structure.
     *
     * @param xmlContent value of the string to unmarshal.
     * @return the {@link OIDRootDefinitionIE} structure.
     * @throws JAXBException if the unmarshalling cannot be performed.
     */
    public OIDRootDefinitionIE xmlToOIDRootDefinition(String xmlContent) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(OIDRootDefinition.class, OIDRootDefinitionIE.class, OIDRootDefinitionLabel.class,
                OIDRequirement.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

        Reader reader = new InputStreamReader(new ByteArrayInputStream
                (xmlContent.getBytes(StandardCharsets.UTF_8)), StandardCharsets.UTF_8);
        return (OIDRootDefinitionIE) unmarshaller.unmarshal(reader);
    }

    /**
     * Extract {@link OIDRootDefinitionIE} from an XML String and analyze for missing TF element missing or duplicate entries.
     *
     * @param xmlOIDRootDefinitions literal representation of the OID Root Definition list.
     * @return the {@link OIDRootDefinitionIE} structure.
     * @throws JAXBException if the unmarshalling cannot be done.
     */
    public OIDRootDefinitionIE extractAndCheckOIDRootDefinitions(String xmlOIDRootDefinitions) throws JAXBException {
        EntityManager entityManager = EntityManagerService.provideEntityManager();

        OIDRootDefinitionIE oidRootDefinitions = xmlToOIDRootDefinition(xmlOIDRootDefinitions);
        oidRootDefinitions = searchForDuplicate(oidRootDefinitions, entityManager);
        return checkOIDRootDefinition(oidRootDefinitions, entityManager);
    }

    /**
     * Check if the OID Root Definition from the list are valid.
     *
     * @param oidRootDefinitionIE the list of OID Root Definition.
     * @param entityManager       EntityManager used to search in DB.
     * @return the updated {@link OIDRootDefinitionIE}
     */
    private OIDRootDefinitionIE checkOIDRootDefinition(OIDRootDefinitionIE oidRootDefinitionIE, EntityManager entityManager) {

        List<OIDRootDefinition> oidRootDefinitions = oidRootDefinitionIE.getOidRootDefinitions();
        Iterator iterator = oidRootDefinitions.iterator();
        while (iterator.hasNext()) {
            OIDRootDefinition oidRootDefinition = ((OIDRootDefinition) iterator.next());
            checkAIPOFromRequirement(oidRootDefinitionIE, oidRootDefinition, entityManager);
            if (!checkObjectTypeStatus(oidRootDefinitionIE, oidRootDefinition, entityManager)) {
                iterator.remove();
                oidRootDefinitionIE.addIgnoredOIDRootDefinition(oidRootDefinition);
            }
        }
        oidRootDefinitionIE.setOidRootDefinitions(oidRootDefinitions);
        return oidRootDefinitionIE;
    }

    /**
     * Check Object Type Status.
     *
     * @param oidRootDefinitionIE {@link OIDRootDefinitionIE}
     * @param oidRootDefinition   OID Root Definition to check.
     * @param entityManager       EntityManager used to search in DB.
     * @return false if the Object Type Status is invalid.
     */
    private boolean checkObjectTypeStatus(OIDRootDefinitionIE oidRootDefinitionIE, OIDRootDefinition oidRootDefinition, EntityManager entityManager) {
        boolean configOk = true;
        //Check objectTypeStatus
        if (oidRootDefinition.getLabel() == null) {
            configOk = false;
        } else {
            OIDRootDefinitionLabelQuery oidRootDefinitionLabelQuery = new OIDRootDefinitionLabelQuery(entityManager);
            oidRootDefinitionLabelQuery.label().eq(oidRootDefinition.getLabel().getLabel());
            OIDRootDefinitionLabel dbOIDRootDefinitionLabel = oidRootDefinitionLabelQuery.getUniqueResult();
            if (dbOIDRootDefinitionLabel == null) {
                configOk = false;
                oidRootDefinitionIE.addUnknownLabel(oidRootDefinition.getLabel().getLabel());
            } else {
                oidRootDefinition.setLabel(dbOIDRootDefinitionLabel);
            }
        }
        return configOk;
    }

    /**
     * Check AIPO from Requirement.
     *
     * @param oidRootDefinitionIE {@link OIDRootDefinitionIE}
     * @param oidRootDefinition   OID Root Definition to check.
     * @param entityManager       EntityManager used to search in DB.
     * @return false if the AIPO is unknown.
     */
    private void checkAIPOFromRequirement(OIDRootDefinitionIE oidRootDefinitionIE, OIDRootDefinition oidRootDefinition,
                                          EntityManager entityManager) {
        if (oidRootDefinition.getListOIDRequirementsNotPersisted() != null && !oidRootDefinition.getListOIDRequirementsNotPersisted().isEmpty()) {
            for (OIDRequirement oidRequirement : oidRootDefinition.getListOIDRequirementsNotPersisted()) {
                if (oidRequirement.getActorIntegrationProfileOptionList() != null && !oidRequirement.getActorIntegrationProfileOptionList().isEmpty()) {
                    List<ActorIntegrationProfileOption> persistedAipo = new ArrayList<>();
                    if (oidRequirement.getActorIntegrationProfileOptionList() != null && !oidRequirement.getActorIntegrationProfileOptionList().isEmpty()) {
                        for (ActorIntegrationProfileOption aipo : oidRequirement.getActorIntegrationProfileOptionList()) {
                            if (aipo != null) {
                                ActorIntegrationProfileOption dbActorIntegrationProfileOption = existingActorIntegrationProfileOption(aipo,
                                        entityManager);
                                if (dbActorIntegrationProfileOption != null) {
                                    persistedAipo.add(dbActorIntegrationProfileOption);
                                } else {
                                    oidRootDefinitionIE.addUnknownAipo(aipo.toStringShort());
                                }
                            }
                        }
                        oidRequirement.setActorIntegrationProfileOptionList(persistedAipo);
                    }
                }
            }
        }
    }

    /**
     * Checks an AIPO is existing in DB.
     *
     * @param aipo          AIPO to look for in DB.
     * @param entityManager EntityManager used to search in DB.
     * @return the value of the {@link ActorIntegrationProfileOption}
     */
    public ActorIntegrationProfileOption existingActorIntegrationProfileOption(ActorIntegrationProfileOption aipo, EntityManager entityManager) {
        ActorIntegrationProfileOptionQuery aipoQuery = new ActorIntegrationProfileOptionQuery(entityManager);
        if (aipo.getIntegrationProfileOption() != null) {
            aipoQuery.integrationProfileOption().keyword().eq(aipo.getIntegrationProfileOption().getKeyword());
        } else {
            aipoQuery.integrationProfileOption().eq(null);
        }
        if (aipo.getActorIntegrationProfile().getActor() != null) {
            aipoQuery.actorIntegrationProfile().actor().keyword().eq(aipo.getActorIntegrationProfile().getActor().getKeyword());
        } else {
            aipoQuery.actorIntegrationProfile().actor().eq(null);
        }
        if (aipo.getActorIntegrationProfile().getIntegrationProfile() != null) {
            aipoQuery.actorIntegrationProfile().integrationProfile().keyword().eq(aipo.getActorIntegrationProfile().getIntegrationProfile().getKeyword());
        } else {
            aipoQuery.actorIntegrationProfile().integrationProfile().eq(null);
        }
        ActorIntegrationProfileOption aipoFromDB = aipoQuery.getUniqueResult();
        return aipoFromDB;
    }

    /**
     * Search for duplicate entries.
     *
     * @param oidRootDefinitionIE list of OID Root Definition.
     * @param entityManager       EntityManager used to search in DB.
     * @return the updated {@link OIDRootDefinitionIE}
     */
    public OIDRootDefinitionIE searchForDuplicate(OIDRootDefinitionIE oidRootDefinitionIE, EntityManager entityManager) {
        List<OIDRootDefinition> objectTypes = oidRootDefinitionIE.getOidRootDefinitions();
        Iterator iterator = objectTypes.iterator();
        while (iterator.hasNext()) {
            OIDRootDefinition oidRootDefinition = (OIDRootDefinition) iterator.next();

            OIDRootDefinitionQuery oidRootDefinitionQuery = new OIDRootDefinitionQuery(entityManager);
            oidRootDefinitionQuery.rootOID().eq(oidRootDefinition.getRootOID());
            oidRootDefinitionQuery.label().label().eq(oidRootDefinition.getLabel().getLabel());
            OIDRootDefinition dbOIDRootDefinition = oidRootDefinitionQuery.getUniqueResult();
            if (dbOIDRootDefinition != null) {


                iterator.remove();
                oidRootDefinitionIE.addDuplicatedOIDRootDefinition(oidRootDefinition);
            }
        }
        oidRootDefinitionIE.setOidRootDefinitions(objectTypes);
        return oidRootDefinitionIE;
    }

    /**
     * Add requirement if needed.
     *
     * @param oidRootDefinitionIE {@link OIDRootDefinitionIE}
     * @param oidRootDefinition   OID Root Definition to check.
     * @param dbOIDRootDefinition OID Root Definition from DB.
     * @param entityManager       EntityManager used to search the DB.
     */
    private void addRequirementIfNeeded(OIDRootDefinitionIE oidRootDefinitionIE, OIDRootDefinition oidRootDefinition,
                                        OIDRootDefinition dbOIDRootDefinition, EntityManager entityManager) {
        /// Add requirements to persist if needed ///
        if (oidRootDefinition.getListOIDRequirementsNotPersisted() != null && !oidRootDefinition.getListOIDRequirementsNotPersisted().isEmpty()) {
            for (OIDRequirement requirement : oidRootDefinition.getListOIDRequirementsNotPersisted()) {
                if (dbOIDRootDefinition.getListOIDRequirementsNotPersisted() != null && !dbOIDRootDefinition.getListOIDRequirementsNotPersisted().isEmpty()) {
                    boolean exists = false;
                    for (OIDRequirement dbRequirement : dbOIDRootDefinition.getListOIDRequirementsNotPersisted()) {
                        if (requirement.getLabel() != null ? requirement.getLabel().equals(dbRequirement.getLabel()) :
                                dbRequirement.getLabel() == null &&
                                        requirement.getPrefix() != null ? requirement.getPrefix().equals(dbRequirement.getPrefix()) :
                                        dbRequirement.getPrefix() == null) {
                            exists = true;
                            List<ActorIntegrationProfileOption> updatedAipos = new ArrayList<>();
                            if (checkAIPOFromRequirement(oidRootDefinitionIE, requirement, dbRequirement, updatedAipos, entityManager)) {
                                requirement.setActorIntegrationProfileOptionList(updatedAipos);
                                requirement.setOidRootDefinition(dbOIDRootDefinition);
                                oidRootDefinitionIE.addNewRequirement(requirement);
                            }
                        }
                    }
                    if (!exists) {
                        requirement.setOidRootDefinition(dbOIDRootDefinition);
                        oidRootDefinitionIE.addNewRequirement(requirement);
                    }
                } else {
                    requirement.setOidRootDefinition(dbOIDRootDefinition);
                    oidRootDefinitionIE.addNewRequirement(requirement);
                }
            }
        }
        //////////////////////////////////////////
    }

    /**
     * Check AIPO from requirements.
     *
     * @param oidRootDefinitionIE {@link OIDRootDefinitionIE}
     * @param requirement         requirement to check.
     * @param dbRequirement       requirement form DB.
     * @param updatedAipos        list of updates AIPOs
     * @param entityManager       EntityManager used to search DB.
     * @return true if AIPO need update.
     */
    private boolean checkAIPOFromRequirement(OIDRootDefinitionIE oidRootDefinitionIE, OIDRequirement requirement, OIDRequirement dbRequirement,
                                             List<ActorIntegrationProfileOption> updatedAipos, EntityManager entityManager) {
        boolean toModify = false;
        for (ActorIntegrationProfileOption aipo : requirement.getActorIntegrationProfileOptionList()) {
            boolean existAipo = false;
            for (ActorIntegrationProfileOption dbAipo : dbRequirement.getActorIntegrationProfileOptionList()) {
                if (aipo.toStringShort().equals(dbAipo.toStringShort())) {
                    existAipo = true;
                    updatedAipos.add(dbAipo);
                }
            }
            if (!existAipo) {
                ActorIntegrationProfileOption dbActorIntegrationProfileOption =
                        existingActorIntegrationProfileOption(aipo, entityManager);
                if (dbActorIntegrationProfileOption != null) {
                    updatedAipos.add(dbActorIntegrationProfileOption);
                    toModify = true;
                } else {
                    oidRootDefinitionIE.addUnknownAipo(aipo.toStringShort());
                }
            }
        }
        return toModify;
    }
}
