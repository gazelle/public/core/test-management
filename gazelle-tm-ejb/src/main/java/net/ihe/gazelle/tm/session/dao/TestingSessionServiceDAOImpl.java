package net.ihe.gazelle.tm.session.dao;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.tm.session.TestingSessionServiceDAO;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.tm.systems.model.TestingSessionQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.persistence.EntityManager;


@Name("testingSessionServiceDAO")
@Scope(ScopeType.STATELESS)
public class TestingSessionServiceDAOImpl implements TestingSessionServiceDAO {

    private EntityManager entityManager;

    @Override
    public TestingSession getDefaultTestingSession() {
        TestingSessionQuery testingSessionQuery = new TestingSessionQuery();
        testingSessionQuery.isDefaultTestingSession().eq(true);
        return testingSessionQuery.getUniqueResult();
    }

    @Override
    public boolean isTestingSessionExisting(Integer testingSessionId) {
        entityManager = EntityManagerService.provideEntityManager();
        return entityManager.find(TestingSession.class, testingSessionId) != null;
    }

    @Override
    public TestingSession getTestingSessionById(Integer testingSessionId) {
        entityManager = EntityManagerService.provideEntityManager();
        return entityManager.find(TestingSession.class, testingSessionId);
    }
}
