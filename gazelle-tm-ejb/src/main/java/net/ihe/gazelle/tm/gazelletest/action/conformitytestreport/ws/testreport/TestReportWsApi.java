package net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.ws.testreport;

import javax.ejb.Local;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Local
@Path("")
@Produces(MediaType.APPLICATION_XML)
public interface TestReportWsApi {

    @GET
    @Path("/testsessions")
    Response getTestSessions(@Context HttpServletRequest servletRequest,
                             @QueryParam("open") String open);

    @GET
    @Path("/testsessions/{testSessionId}/suts")
    Response getSuts(@Context HttpServletRequest servletRequest,
                     @PathParam("testSessionId") String testSessionId,
                     @QueryParam("organization") String organizationId);

    @GET
    @Path("/testsessions/{testSessionId}/suts/{sut}/report")
    Response getTestReports(@Context HttpServletRequest servletRequest,
                            @PathParam("testSessionId") String testSessionId,
                            @PathParam("sut") String sutId);
}