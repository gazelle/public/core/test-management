package net.ihe.gazelle.tm.gazelletest.testingSessionScope;

import java.util.List;
import java.util.Objects;

public class ProfileCoverage {

    private String actorName;
    private List<ImplementedActorInSystem> implementedActorInSystems;

    public String getActorName() {
        return actorName;
    }

    public void setActorName(String actorName) {
        this.actorName = actorName;
    }

    public List<ImplementedActorInSystem> getImplementedActorInSystems() {
        return implementedActorInSystems;
    }

    public void setImplementedActorInSystems(List<ImplementedActorInSystem> implementedActorInSystems) {
        this.implementedActorInSystems = implementedActorInSystems;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProfileCoverage that = (ProfileCoverage) o;
        return Objects.equals(actorName, that.actorName) && Objects.equals(implementedActorInSystems, that.implementedActorInSystems);
    }

    @Override
    public int hashCode() {
        return Objects.hash(actorName, implementedActorInSystems);
    }
}
