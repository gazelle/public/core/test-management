package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.execution.service;

import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.tm.gazelletest.action.TestInstanceTokenService;
import net.ihe.gazelle.tm.gazelletest.action.TestStepDataNotFoundException;
import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.instance.AutomatedTestStepInstanceData;
import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script.InputContent;
import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script.MaestroRequest;
import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script.Script;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.execution.client.MaestroRestClient;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.execution.client.StepRunClientException;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.execution.dao.AutomatedTestStepExecutionDAO;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.instance.service.AutomatedTestStepInstanceInputService;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.testdesign.service.AutomatedTestStepService;
import net.ihe.gazelle.tm.gazelletest.dao.TestInstanceDataDAO;
import net.ihe.gazelle.tm.gazelletest.domain.AutomatedTestStepExecution;
import net.ihe.gazelle.tm.gazelletest.domain.AutomatedTestStepExecutionStatus;
import net.ihe.gazelle.tm.gazelletest.domain.TestStepDomain;
import net.ihe.gazelle.tm.gazelletest.model.instance.AutomatedTestStepInstanceInput;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestStepsData;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestStepsInstance;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestStepsInstanceStatus;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

@Name("automatedTestStepRunService")
@Scope(ScopeType.EVENT)
@AutoCreate
public class AutomatedTestStepRunServiceImpl implements AutomatedTestStepRunService {

    private static final Map<Integer, Semaphore> INSTANCE_SEMAPHORE = new ConcurrentHashMap<>();
    private static final int EXECUTION_VALIDITY_TIME_IN_MINUTES = 2;
    private static final String GAZELLE_TOKEN = "GazelleToken ";

    @In(value = "maestroRestClient")
    private MaestroRestClient client;
    @In(value = "testInstanceTokenService")
    private TestInstanceTokenService tokenService;
    @In(value = "testInstanceDataDAO")
    private TestInstanceDataDAO testInstanceDataDAO;
    @In(value = "automatedTestStepService")
    private AutomatedTestStepService automatedTestStepService;
    @In(value = "automatedTestStepExecutionDAO")
    private AutomatedTestStepExecutionDAO automatedExecutionDAO;
    @In(value = "automatedTestStepInstanceInputService")
    private AutomatedTestStepInstanceInputService inputDataService;

    public AutomatedTestStepRunServiceImpl() {
        // empty for injection
    }

    public AutomatedTestStepRunServiceImpl(MaestroRestClient client, TestInstanceTokenService tokenService, TestInstanceDataDAO testInstanceDataDAO, AutomatedTestStepService automatedTestStepService, AutomatedTestStepExecutionDAO automatedExecutionDAO, AutomatedTestStepInstanceInputService inputDataService) {
        this.client = client;
        this.tokenService = tokenService;
        this.testInstanceDataDAO = testInstanceDataDAO;
        this.automatedTestStepService = automatedTestStepService;
        this.automatedExecutionDAO = automatedExecutionDAO;
        this.inputDataService = inputDataService;
    }

    @Override
    public void run(GazelleIdentity identity, TestStepsInstance stepInstance) throws StepRunServiceException {
        Semaphore semaphore = null;
        try {
            semaphore = aquireSemaphore(stepInstance.getId());
            synchronizedRun(identity, stepInstance);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } finally {
            if (semaphore != null) {
                semaphore.release();
            }
        }
    }

    private void synchronizedRun(GazelleIdentity identity, TestStepsInstance stepInstance) throws StepRunServiceException {
        AutomatedTestStepExecution execution = getExecution(stepInstance.getId());
        if (execution != null && AutomatedTestStepExecutionStatus.RUNNING.equals(execution.getStatus())) {
            throw new StepRunServiceException("Step was already running");
        }
        String tokenValue = tokenService.generateTokenWithMinutesValidity(identity.getUsername(), stepInstance, EXECUTION_VALIDITY_TIME_IN_MINUTES);
        MaestroRequest maestroRequest = buildMaestroScript(tokenValue, stepInstance);
        try {
            client.runAutomatedTestStep(maestroRequest, stepInstance);
            createOrUpdateExecution(stepInstance, new AutomatedTestStepExecution(stepInstance.getId(), identity.getUsername(), AutomatedTestStepExecutionStatus.RUNNING, new Date()));
        } catch (StepRunClientException e) {
            createOrUpdateExecution(stepInstance, new AutomatedTestStepExecution(stepInstance.getId(), identity.getUsername(), AutomatedTestStepExecutionStatus.DONE_UNDEFINED, new Date()));
            tokenService.deleteTokenByTestStepInstance(stepInstance.getId());
            throw new StepRunServiceException(e.getMessage(), e);
        }
    }

    @Override
    public void cancelExecution(Integer testStepInstanceId) {
        AutomatedTestStepExecution executionResource = new AutomatedTestStepExecution();
        executionResource.setTestStepsInstanceId(testStepInstanceId);
        executionResource.setStatus(AutomatedTestStepExecutionStatus.DONE_UNDEFINED);
        automatedExecutionDAO.updateExecution(executionResource);
        tokenService.deleteTokenByTestStepInstance(testStepInstanceId);
    }

    @Override
    public void receiveExecution(GazelleIdentity identity, AutomatedTestStepExecution execution) {
        if (isStepRunning(execution)) {
            automatedExecutionDAO.updateExecution(execution);
            TestStepsInstance testStepsInstance;
            if (AutomatedTestStepExecutionStatus.DONE_PASSED.equals(execution.getStatus())) {
                testStepsInstance = automatedExecutionDAO.updateTestStepInstanceStatus(execution.getTestStepsInstanceId(), TestStepsInstanceStatus.getSTATUS_DONE());
            } else {
                testStepsInstance = automatedExecutionDAO.updateTestStepInstanceStatus(execution.getTestStepsInstanceId(), TestStepsInstanceStatus.getSTATUS_ACTIVATED());
            }
            automatedExecutionDAO.sendNotification(identity.getUsername(), execution, testStepsInstance);
        } else {
            throw new NotRunningException("The step was not running anymore : status not updated");
        }
    }

    @Override
    public AutomatedTestStepExecution getExecution(Integer testStepInstanceId) {
        AutomatedTestStepExecution execution = automatedExecutionDAO.getExecution(testStepInstanceId);
        return checkRunTimeout(execution);
    }

    @Override
    public boolean isStepRunning(AutomatedTestStepExecution execution) {
        execution = getExecution(execution.getTestStepsInstanceId());
        return execution != null && AutomatedTestStepExecutionStatus.RUNNING.equals(execution.getStatus());
    }

    @Override
    public Map<AutomatedTestStepInstanceData, Boolean> getUpToDateStatusForAllInstanceInput(TestStepsInstance stepInstance) {
        Map<AutomatedTestStepInstanceData, Boolean> executionUpToDateMap = new HashMap<>();
        List<AutomatedTestStepInstanceInput> instanceInputsList = inputDataService.getInstanceInputForTestInstance(stepInstance.getTestInstance());
        AutomatedTestStepExecution execution = getExecution(stepInstance.getId());
        if (instanceInputsList != null && execution != null && !AutomatedTestStepExecutionStatus.DONE_UNDEFINED.equals(execution.getStatus())) {
            Date executionDate = execution.getDate();
            for (AutomatedTestStepInstanceInput instanceInput : instanceInputsList) {
                AutomatedTestStepInstanceData inputData = new AutomatedTestStepInstanceData(instanceInput.getId(), stepInstance.getId(), instanceInput.getKey(), instanceInput.getLabel(), instanceInput.getDescription(), instanceInput.getRequired());
                try {
                    TestStepsData data = testInstanceDataDAO.getTestStepDataById(instanceInput.getTestStepDataId());
                    if (data != null) {
                        inputData.setFileName(data.getValue());
                        inputData.setTestStepDataId(data.getId());
                        inputData.setUserId(data.getLastModifierId());
                        SimpleDateFormat desiredFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
                        inputData.setLastUpdate(desiredFormat.format(data.getLastChanged()));
                    }
                    executionUpToDateMap.put(inputData, isExecutionOutDated(data, executionDate));
                } catch (TestStepDataNotFoundException e) {
                    // do nothing
                }
            }
        }
        return executionUpToDateMap;
    }

    @Override
    public boolean isRunOutDated(Integer testStepInstanceId) {
        Date latestUpload = inputDataService.getLatestUploadDate(testStepInstanceId);
        AutomatedTestStepExecution execution = getExecution(testStepInstanceId);
        if (latestUpload != null && execution != null) {
            Date executionDate = execution.getDate();
            return executionDate != null && latestUpload.after(executionDate);
        }
        return false;
    }

    private boolean isExecutionOutDated(TestStepsData data, Date executionDate) {
        if (data == null || data.getLastChanged() == null || executionDate == null) {
            return false;
        }
        return data.getLastChanged().after(executionDate);
    }

    private void createOrUpdateExecution(TestStepsInstance stepInstance, AutomatedTestStepExecution execution) {
        if (automatedExecutionDAO.getExecution(stepInstance.getId()) == null) {
            automatedExecutionDAO.createExecution(execution);
        } else {
            automatedExecutionDAO.updateExecution(execution);
        }
    }

    private AutomatedTestStepExecution checkRunTimeout(AutomatedTestStepExecution execution) {
        if (execution != null && execution.getDate() != null
                && AutomatedTestStepExecutionStatus.RUNNING.equals(execution.getStatus())) {
            Timestamp currentTime = new Timestamp(System.currentTimeMillis());
            Timestamp expirationTime = new Timestamp(execution.getDate().getTime()
                    + TimeUnit.MINUTES.toMillis(EXECUTION_VALIDITY_TIME_IN_MINUTES));
            if (currentTime.after(expirationTime)) {
                execution.setStatus(AutomatedTestStepExecutionStatus.TIMED_OUT);
                automatedExecutionDAO.updateExecution(execution);
                tokenService.deleteTokenByTestStepInstance(execution.getTestStepsInstanceId());
            }
        }
        return execution;
    }

    private synchronized Semaphore aquireSemaphore(Integer testStepInstanceId) throws InterruptedException {
        Semaphore semaphore = computeIfAbsent(testStepInstanceId);
        semaphore.acquire();
        return semaphore;
    }

    private synchronized Semaphore computeIfAbsent(Integer testStepInstanceId) {
        Semaphore semaphore;
        if (!INSTANCE_SEMAPHORE.containsKey(testStepInstanceId)) {
            semaphore = new Semaphore(1);
            INSTANCE_SEMAPHORE.put(testStepInstanceId, semaphore);
        } else {
            semaphore = INSTANCE_SEMAPHORE.get(testStepInstanceId);
        }
        return semaphore;
    }

    private MaestroRequest buildMaestroScript(String tokenValue, TestStepsInstance stepInstance) {
        List<InputContent> inputContentList = buildInputContentList(stepInstance);
        TestStepDomain testStep = stepInstance.getTestSteps().asDomain();
        Script script = automatedTestStepService.readScript(testStep.getId(), testStep.getTestScriptID());
        return new MaestroRequest(GAZELLE_TOKEN + tokenValue, script, inputContentList);
    }

    private List<InputContent> buildInputContentList(TestStepsInstance stepInstance) {
        List<AutomatedTestStepInstanceData> inputDataList = inputDataService.getInputDataFromTestStepInstance(stepInstance);
        List<InputContent> inputContentList = new ArrayList<>();
        for (AutomatedTestStepInstanceData inputData : inputDataList) {
            if (inputData.getTestStepDataId() != null && inputData.getFileName() != null) {
                byte [] binaryContent = inputDataService.readInputData(inputData).getBytes(StandardCharsets.UTF_8);
                String base64String = StringUtils.newStringUtf8(Base64.encodeBase64(binaryContent, false));
                inputContentList.add(new InputContent(inputData.getKey(), base64String));
            }
        }
        return inputContentList;
    }
}
