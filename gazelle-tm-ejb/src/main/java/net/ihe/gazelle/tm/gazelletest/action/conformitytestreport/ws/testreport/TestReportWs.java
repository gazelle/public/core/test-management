package net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.ws.testreport;

import net.ihe.gazelle.ssov7.authn.interlay.adapter.GazelleIdentityImpl;
import net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.domain.ReportGenerator;
import net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.exceptions.UnauthorizedException;
import net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.model.ConformityTestReportModel;
import net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.model.SystemListModel;
import net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.model.TestingSessionListModel;
import net.ihe.gazelle.token.wsclient.InvalidTokenException;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;

@Stateless
@Local
@Name("testReportWsApi")
public class TestReportWs implements TestReportWsApi {

    private static final Logger LOG = LoggerFactory.getLogger(TestReportWs.class);

    @Override
    public Response getTestSessions(HttpServletRequest servletRequest, String open) {
        LOG.info("Executing getTestSessions(tokenString, isOpen)");
        GazelleIdentityImpl identity = getIdentity();

        if (open != null && !(open.equals("false") || open.equals("true"))) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        Boolean isOpen = null;
        if (open != null) {
            isOpen = Boolean.valueOf(open);
        }

        if (identity.isLoggedIn()) {
            try {
                ReportGenerator reportGenerator = new ReportGenerator(identity);
                TestingSessionListModel report = reportGenerator.exportTestingSessionInfos(identity, isOpen);

                return Response.status(Response.Status.OK).entity(report).build();
            } catch (InvalidTokenException e) {
                return Response.status(Response.Status.UNAUTHORIZED).build();
            } catch (Exception e) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build(); // Unknown error
            } finally {
                getIdentity().logout();
            }
        } else {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }

    @Override
    public Response getSuts(HttpServletRequest servletRequest, String testSessionId, String organizationId) {
        LOG.info("Executing getSuts(tokenString, testSessionId, organizationId)");
        GazelleIdentityImpl identity = getIdentity();

        if (testSessionId == null || testSessionId.isEmpty()) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        try {
            identity.isLoggedIn();
            Integer testSessionInt = Integer.valueOf(testSessionId);
            Integer orgInt = null;
            if (organizationId != null) {
                orgInt = Integer.valueOf(organizationId);
            }

            ReportGenerator reportGenerator = new ReportGenerator(identity);
            SystemListModel report = reportGenerator.exportSystemInfos(identity, testSessionInt, orgInt);
            return Response.status(Response.Status.OK).entity(report).build();
        } catch (InvalidTokenException e) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        } catch (UnauthorizedException | NullPointerException e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } catch (NumberFormatException e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } catch (Exception e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build(); // Unknown error
        } finally {
            identity.logout();
        }
    }

    @Override
    public Response getTestReports(HttpServletRequest servletRequest, String testSessionId, String sutId) {
        LOG.info("Executing getSuts(tokenString, testSessionId, sutId)");
        GazelleIdentityImpl identity = getIdentity();

        if (testSessionId == null || sutId == null || testSessionId.isEmpty() || sutId.isEmpty()) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        try {
            identity.isLoggedIn();
            Integer testSessionInt = Integer.valueOf(testSessionId);
            Integer sutInt = Integer.valueOf(sutId);

            ReportGenerator reportGenerator = new ReportGenerator(identity);
            ConformityTestReportModel report = reportGenerator.exportSystemReportAsXML(identity, testSessionInt, sutInt);
            return Response.status(Response.Status.OK).entity(report).build();
        } catch (InvalidTokenException e) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        } catch (UnauthorizedException e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } catch (NumberFormatException e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } catch (Exception e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build(); // Unknown error
        } finally {
            identity.logout();
        }
    }

    private GazelleIdentityImpl getIdentity() {
        return GazelleIdentityImpl.instance();
    }
}