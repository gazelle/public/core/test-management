package net.ihe.gazelle.tm.testexecution;

public enum TestingDepthStatus {
    THOROUGH("Thorough","gazelle.tm.Thorough","gzl-label gzl-label-purple"),
    SUPPORTIVE("Supportive","gazelle.tm.Supportive","gzl-label gzl-label-supportive");

    private String name;
    private String labelToDisplay;
    private String cssStyle;

    TestingDepthStatus(String name,String labelToDisplay, String cssStyle) {
        this.name=name;
        this.labelToDisplay = labelToDisplay;
        this.cssStyle = cssStyle;
    }

    public String getLabelToDisplay() {
        return labelToDisplay;
    }

    public String getStatusStyleClass() {
        return cssStyle;
    }

    public String getName() {
        return name;
    }

    public static TestingDepthStatus getStatusByKeyword(String name) {
        TestingDepthStatus[] testingDepthStatuses = values();
        if(name!=null) {
            for (int i = 0; i < testingDepthStatuses.length; i++) {
                if (testingDepthStatuses[i].getName().equals(name)) {
                    return testingDepthStatuses[i];
                }
            }
        }
        return null;
    }
}
