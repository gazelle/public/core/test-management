package net.ihe.gazelle.menu;

import net.ihe.gazelle.common.pages.Authorization;
import net.ihe.gazelle.common.pages.AuthorizationAnd;
import net.ihe.gazelle.common.pages.AuthorizationOr;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.authn.interlay.adapter.GazelleIdentityImpl;
import net.ihe.gazelle.ssov7.gum.client.application.Role;
import net.ihe.gazelle.ssov7.gum.client.application.User;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.tm.application.action.ApplicationManager;
import net.ihe.gazelle.tm.gazelletest.model.instance.MonitorInSession;
import net.ihe.gazelle.tm.session.TestingSessionService;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.tm.systems.model.TestingSessionAdmin;
import org.jboss.seam.Component;

import java.util.ArrayList;
import java.util.List;

public class Authorizations {


   private static TestingSessionService getTestingSessionService() {
      return (TestingSessionService) Component.getInstance("testingSessionService");
   }

   public static final Authorization ALL = new Authorization() {
      @Override
      public boolean isGranted(Object... context) {
         return true;
      }
   };

   public static final Authorization NONE = new Authorization() {
      @Override
      public boolean isGranted(Object... context) {
         return false;
      }
   };

   // Application modes authorizations

   public static final Authorization GMM = new Authorization() {
      @Override
      public boolean isGranted(Object... context) {
         return ApplicationManager.instance().isMasterModel();
      }
   };

   public static final Authorization NOT_GMM = new Authorization() {
      @Override
      public boolean isGranted(Object... context) {
         return !GMM.isGranted(context);
      }
   };

   public static final Authorization TM = new Authorization() {
      @Override
      public boolean isGranted(Object... context) {
         return ApplicationManager.instance().isTestManagement();
      }
   };

   public static final Authorization NOT_TM = new Authorization() {
      @Override
      public boolean isGranted(Object... context) {
         return !TM.isGranted(context);
      }
   };

   public static final Authorization PR = new Authorization() {
      @Override
      public boolean isGranted(Object... context) {
         return ApplicationManager.instance().isProductRegistry();
      }
   };

   public static final Authorization NOT_PR = new Authorization() {
      @Override
      public boolean isGranted(Object... context) {
         return !PR.isGranted(context);
      }
   };

   public static final Authorization ONLY_GMM = new AuthorizationAnd(GMM, NOT_TM, NOT_PR);

   public static final Authorization NOT_ONLY_GMM = new AuthorizationOr(TM, PR);

   public static final Authorization TESTS_DISPLAYED_TO_NOT_LOGGED = new Authorization() {
      @Override
      public boolean isGranted(Object... context) {
         return ApplicationManager.instance().isTestsDisplayAsNotLoggedIn();
      }
   };

   // User and role based authorizations

   public static final Authorization LOGGED = new Authorization() {
      @Override
      public boolean isGranted(Object... context) {
         return GazelleIdentityImpl.instance().isLoggedIn();
      }
   };

   public static final Authorization NOT_LOGGED = new Authorization() {
      @Override
      public boolean isGranted(Object... context) {
         return !LOGGED.isGranted(context);
      }
   };

   public static final Authorization ADMIN = new Authorization() {
      @Override
      public boolean isGranted(Object... context) {
         return GazelleIdentityImpl.instance().hasRole(Role.ADMIN);
      }
   };

   public static final Authorization VENDOR_ADMIN = new Authorization() {
      @Override
      public boolean isGranted(Object... context) {
         return GazelleIdentityImpl.instance().hasRole(Role.VENDOR_ADMIN);
      }
   };

   public static final Authorization VENDOR = new Authorization() {
      @Override
      public boolean isGranted(Object... context) {
         return GazelleIdentityImpl.instance().hasRole(Role.VENDOR);
      }
   };

   public static final Authorization MONITOR = new Authorization() {
      @Override
      public boolean isGranted(Object... context) {
         return GazelleIdentityImpl.instance().hasRole(Role.MONITOR);
      }
   };

   public static final Authorization TESTING_SESSION_ADMIN = new Authorization() {
      @Override
      public boolean isGranted(Object... context) {
         return GazelleIdentityImpl.instance().hasRole(Role.TESTING_SESSION_ADMIN);
      }
   };

   public static final Authorization TESTING_SESSION_ADMIN_OF_CURRENT_TESTING_SESSION = new AuthorizationAnd(
         TESTING_SESSION_ADMIN,
         new Authorization() {
            @Override
            public boolean isGranted(Object... context) {
               return isUserAdminOfTestingSession(
                     GazelleIdentityImpl.instance(),
                     getTestingSessionService().getUserTestingSession()
               );
            }
         }
   );

   public static final Authorization PROJECT_MANAGER = new Authorization() {
      @Override
      public boolean isGranted(Object... context) {
         return GazelleIdentityImpl.instance().hasRole(Role.PROJECT_MANAGER);
      }
   };

   public static final Authorization ACCOUNTING = new Authorization() {
      @Override
      public boolean isGranted(Object... context) {
         return GazelleIdentityImpl.instance().hasRole(Role.ACCOUNTING);
      }
   };

   public static final Authorization EDITOR_ROLE = new AuthorizationOr(
         ADMIN,
         new Authorization() {
            @Override
            public boolean isGranted(Object... context) {
               return GazelleIdentityImpl.instance().hasRole(Role.TESTS_EDITOR);
            }
         }
   );

   public static final Authorization EDITOR = new AuthorizationAnd(
         GMM,
         EDITOR_ROLE
   );

   public static final Authorization REGISTRATION_SUPER_ADMIN = new AuthorizationOr(
         PROJECT_MANAGER,
         ACCOUNTING,
         ADMIN
   );

   public static final Authorization REGISTRATION_ADMIN = new AuthorizationOr(
         VENDOR_ADMIN,
         REGISTRATION_SUPER_ADMIN
   );

   public static final Authorization REGISTRATION_EDITOR = new AuthorizationOr(
         VENDOR,
         MONITOR,
         REGISTRATION_ADMIN
   );

   public static final Authorization OBJECT_VIEWER = new AuthorizationOr(
         EDITOR_ROLE,
         MONITOR
   );

   public static final Authorization MONITOR_OR_VENDOR_OR_VENDOR_ADMIN = new AuthorizationOr(
         MONITOR, VENDOR, VENDOR_ADMIN
   );

   public static final Authorization MONITOR_OF_CURRENT_TESTING_SESSION = new Authorization() {
      @Override
      public boolean isGranted(Object... context) {
         return MonitorInSession.isMonitorForSelectedSession(
               GazelleIdentityImpl.instance().getUsername(), getTestingSessionService().getUserTestingSession()
         );
      }
   };

   public static final Authorization MONITOR_OR_TESTING_SESSION_ADMIN_OF_CURRENT_TESTING_SESSION = new AuthorizationOr(
         MONITOR_OF_CURRENT_TESTING_SESSION,
         TESTING_SESSION_ADMIN_OF_CURRENT_TESTING_SESSION
   );

   public static final Authorization VENDOR_OR_VENDOR_ADMIN = new AuthorizationOr(VENDOR, VENDOR_ADMIN);

   public static final Authorization ACCOUNTING_OR_TESTING_SESSION_ADMIN_OF_CURRENT_TESTING_SESSION =
         new AuthorizationOr(
               ADMIN,
               ACCOUNTING,
               TESTING_SESSION_ADMIN_OF_CURRENT_TESTING_SESSION
         );

   public static final Authorization ADMIN_OR_MONITOR = new AuthorizationOr(ADMIN, MONITOR);

   public static final Authorization ADMIN_OR_MONITOR_OR_TESTING_SESSION_ADMIN_OF_CURRENT_TESTING_SESSION =
         new AuthorizationOr(ADMIN, MONITOR_OR_TESTING_SESSION_ADMIN_OF_CURRENT_TESTING_SESSION);

   public static final Authorization ADMIN_OR_PROJECT_MANAGER = new AuthorizationOr(ADMIN, PROJECT_MANAGER);

   public static final Authorization ADMIN_OR_PROJECT_MANAGER_OR_TESTING_SESSION_ADMIN_OF_CURRENT_TESTING_SESSION =
         new AuthorizationOr(ADMIN, PROJECT_MANAGER, TESTING_SESSION_ADMIN_OF_CURRENT_TESTING_SESSION);

   public static final Authorization ADMIN_OR_VENDOR_ADMIN = new AuthorizationOr(ADMIN, VENDOR_ADMIN);

   public static final Authorization ADMIN_OR_VENDOR = new AuthorizationOr(ADMIN, VENDOR, VENDOR_ADMIN);

   public static final Authorization ADMIN_OR_VENDOR_OR_TESTING_SESSION_ADMIN_OF_CURRENT_TESTING_SESSION =
         new AuthorizationOr(ADMIN_OR_VENDOR, TESTING_SESSION_ADMIN_OF_CURRENT_TESTING_SESSION);

   public static final Authorization ADMIN_OR_ACCOUNTING = new AuthorizationOr(ADMIN, ACCOUNTING);

   public static final Authorization ADMIN_OR_EDITOR = new AuthorizationOr(ADMIN, EDITOR);

   public static final Authorization ADMIN_OR_ACCOUNTING_OR_TESTING_SESSION_ADMIN_OF_CURRENT_TESTING_SESSION =
         new AuthorizationOr(
               ADMIN, ACCOUNTING, TESTING_SESSION_ADMIN_OF_CURRENT_TESTING_SESSION);

   public static final Authorization ADMIN_OR_MONITOR_OR_VENDOR = new AuthorizationOr(ADMIN_OR_MONITOR, VENDOR);

   public static final Authorization ADMIN_OR_TESTING_SESSION_ADMIN_OF_CURRENT_TESTING_SESSION = new AuthorizationOr(
         ADMIN, TESTING_SESSION_ADMIN_OF_CURRENT_TESTING_SESSION);

   public static final Authorization ANY_VENDOR_OR_ADMIN_OR_TEST_SESSION_MANAGER_OF_CURRENT_TESTING_SESSION =
         new AuthorizationOr(ADMIN_OR_VENDOR, VENDOR_ADMIN, TESTING_SESSION_ADMIN_OF_CURRENT_TESTING_SESSION);

   public static final Authorization ANY_VENDOR_OR_ADMIN_OR_MONITOR_OR_TEST_SESSION_MANAGER_OF_CURRENT_TESTING_SESSION =
         new AuthorizationOr(ADMIN_OR_VENDOR, VENDOR_ADMIN, MONITOR, TESTING_SESSION_ADMIN_OF_CURRENT_TESTING_SESSION);

   public static final Authorization ANY_VENDOR_OR_ADMIN_OR_MONITOR_OR_TEST_SESSION_MANAGER_OF_CURRENT_TESTING_SESSION_OR_EDITOR = new AuthorizationOr(
         ADMIN_OR_VENDOR, VENDOR_ADMIN, MONITOR, TESTING_SESSION_ADMIN_OF_CURRENT_TESTING_SESSION, EDITOR);

   public static final Authorization ANY_VENDOR_OR_ADMIN_OR_MONITOR_OR_TEST_SESSION_MANAGER_OF_CURRENT_TESTING_SESSION_OR_EDITOR_OR_TEST_DISPLAYED_TO_NOT_LOGGED = new AuthorizationOr(
           ADMIN_OR_VENDOR, VENDOR_ADMIN, MONITOR, TESTING_SESSION_ADMIN_OF_CURRENT_TESTING_SESSION, EDITOR, TESTS_DISPLAYED_TO_NOT_LOGGED);

   public static final Authorization ANY_EXCEPT_ACCOUNTING = new AuthorizationOr(
         ANY_VENDOR_OR_ADMIN_OR_MONITOR_OR_TEST_SESSION_MANAGER_OF_CURRENT_TESTING_SESSION_OR_EDITOR, NOT_LOGGED);

   public static final Authorization PROJECT_MANAGER_OR_VENDOR_ADMIN = new AuthorizationOr(
         PROJECT_MANAGER, VENDOR_ADMIN
   );
   public static final Authorization VENDOR_OR_TESTING_SESSION_ADMIN_OF_CURRENT_TESTING_SESSION = new AuthorizationOr(
         VENDOR, TESTING_SESSION_ADMIN_OF_CURRENT_TESTING_SESSION);
   public static final Authorization ADMIN_OR_VENDOR_ADMIN_OR_TESTING_SESSION_ADMIN_OF_CURRENT_TESTING_SESSION =
         new AuthorizationOr(ADMIN_OR_VENDOR_ADMIN, TESTING_SESSION_ADMIN_OF_CURRENT_TESTING_SESSION);

   // Testing-session authorizations

   public static final Authorization SESSION_WITH_CONTRACT = new Authorization() {
      @Override
      public boolean isGranted(Object... context) {
         TestingSession testingSession = getTestingSessionService().getUserTestingSession();
         return testingSession != null && testingSession.isContractRequired();
      }
   };

   public static final Authorization SESSION_WITH_PRECAT = new Authorization() {
      @Override
      public boolean isGranted(Object... context) {
         TestingSession testingSession = getTestingSessionService().getUserTestingSession();
         return testingSession != null && testingSession.isEnablePrecatTests();
      }
   };


   public static final Authorization SESSION_WITH_PARTICIPANTS_REGISTRATION = new AuthorizationOr(
         ADMIN,
         new Authorization() {
            @Override
            public boolean isGranted(Object... context) {
               TestingSession testingSession = getTestingSessionService().getUserTestingSession();
               return testingSession != null && testingSession.getAllowParticipantRegistration();
            }
         }
   );

   public static final Authorization SESSION_WITH_INTEROPERABILITY = new Authorization() {
      @Override
      public boolean isGranted(Object... context) {
         TestingSession testingSession = getTestingSessionService().getUserTestingSession();
         return testingSession != null && testingSession.isTestingInteroperability();
      }
   };

   public static final Authorization SESSION_WITH_MONITORS = new Authorization() {
      @Override
      public boolean isGranted(Object... context) {
         TestingSession testingSession = getTestingSessionService().getUserTestingSession();
         return testingSession != null && !testingSession.isSessionWithoutMonitors();
      }
   };

   public static final Authorization SESSION_WITH_PATIENTS = new Authorization() {
      @Override
      public boolean isGranted(Object... context) {
         TestingSession testingSession = getTestingSessionService().getUserTestingSession();
         return testingSession != null && !testingSession.getDisablePatientGenerationAndSharing();
      }
   };

   public static final Authorization SESSION_WITH_SAMPLE_SEARCH = new Authorization() {
      @Override
      public boolean isGranted(Object... context) {
         TestingSession testingSession = getTestingSessionService().getUserTestingSession();
         return testingSession != null && !testingSession.isHideAdvancedSampleSearchToVendors();
      }
   };

   public static final Authorization SAMPLE_SEARCH_USERS = new AuthorizationOr(
         ADMIN,
         EDITOR_ROLE,
         MONITOR,
         TESTING_SESSION_ADMIN_OF_CURRENT_TESTING_SESSION
   );

   public static final Authorization SAMPLE_SEARCH = new AuthorizationOr(
         SESSION_WITH_SAMPLE_SEARCH,
         SAMPLE_SEARCH_USERS
   );

   public static final Authorization PREPARATION_SPECIAL_GMM = new AuthorizationAnd(GMM, ADMIN_OR_EDITOR);

   public static final Authorization PREPARATION_SPECIAL_TM = new AuthorizationAnd(TM,
         ANY_VENDOR_OR_ADMIN_OR_MONITOR_OR_TEST_SESSION_MANAGER_OF_CURRENT_TESTING_SESSION);

   public static final Authorization PREPARATION_SPECIAL = new AuthorizationOr(PREPARATION_SPECIAL_GMM,
         PREPARATION_SPECIAL_TM);


   public static final Authorization DISPLAY_TESTS = new AuthorizationOr(LOGGED, TESTS_DISPLAYED_TO_NOT_LOGGED);
   public static final Authorization ADD_NEW_INSTITUTION = new AuthorizationOr(ADMIN, PROJECT_MANAGER);

   private static boolean isUserAdminOfTestingSession(GazelleIdentity identity, TestingSession testingSession) {
      UserService userService = (UserService) Component.getInstance("gumUserService");
      if (identity.isLoggedIn() && testingSession != null) {
         List<User> testingSessionAdmins = new ArrayList<>();
         List<TestingSessionAdmin> userIdList = testingSession.getTestingSessionAdmins();
         for (TestingSessionAdmin sessionAdmin : userIdList) {
            User user = userService.getUserById(sessionAdmin.getUserId());
            testingSessionAdmins.add(user);
         }
         for (User tsa : testingSessionAdmins) {
            if (tsa.getId().equals(identity.getUsername())) {
               return true;
            }
         }
      }
      return false;
   }

   private Authorizations() {
      // Hide constructor, this class is exclusivly composed of static members.
   }
}
