package net.ihe.gazelle.users.action;

/**
 */
public enum EmailType implements
        EmailTemplate {

    /**
     * Field TO_USER_ON_USER_REGISTERING_WITHOUT_INSTITUTION.
     */
    TO_USER_ON_USER_REGISTERING_WITHOUT_INSTITUTION("/email/toUserOnUserRegisteringWithoutInstitution.xhtml"),

    /**
     * Field TO_INSTITUTION_ADMIN_ON_USER_REGISTERING.
     */
    TO_INSTITUTION_ADMIN_ON_USER_REGISTERING("/email/toInstitutionAdminOnUserRegistering.xhtml"),

    /**
     * Field TO_ADMIN_ON_USER_REGISTERING.
     */
    TO_ADMIN_ON_USER_REGISTERING("/email/toAdminOnUserRegistering.xhtml"),

    /**
     * Field TO_USER_ON_USER_REGISTERING_WITH_INSTITUTION.
     */
    TO_USER_ON_USER_REGISTERING_WITH_INSTITUTION("/email/toUserOnUserRegisteringWithInstitution.xhtml"),

    /**
     * Field TO_USER_ON_USER_REGISTERING_WITH_INSTITUTION_WITHOUT_ADMIN.
     */
    TO_USER_ON_USER_REGISTERING_WITH_INSTITUTION_WITHOUT_ADMIN("/email/toUserOnUserRegisteringWithInstitutionWithoutAdmin.xhtml"),

    /**
     * Field TO_USER_ON_USER_ACTIVATED_BY_INSTITUTION_ADMIN.
     */
    TO_USER_ON_USER_ACTIVATED_BY_INSTITUTION_ADMIN("/email/toUserOnUserActivatedByInstitutionAdmin.xhtml"),

    /**
     * Field TO_USER_ON_USER_REGISTERED_BY_INSTITUTION_ADMIN.
     */
    TO_USER_ON_USER_REGISTERED_BY_INSTITUTION_ADMIN("/email/toUserOnUserRegisteredByInstitutionAdmin.xhtml"),

    /**
     * Field TO_USER_TO_CHNAGE_EMAIL.
     */
    TO_USER_TO_CHANGE_EMAIL("/email/toUserToChangeEmail.xhtml"),

    /**
     * Field TO_USER_TO_CHNAGE_EMAIL.
     */
    TO_USER_NOTIFICATION("/email/toUserNotification.xhtml"),

    /**
     * Field TO_INSTITUTION_ADMIN_ON_PR_UNMATCHES.
     */
    TO_INSTITUTION_ADMIN_ON_PR_UNMATCHES("/email/prToInstitutionAdminOnUnmatches.xhtml"),

    /**
     * Field TO_INSTITUTION_ADMIN_ON_PR_UNREACHABLE.
     */
    TO_INSTITUTION_ADMIN_ON_PR_UNREACHABLE("/email/prToInstitutionAdminOnUnreachable.xhtml"),

    /**
     * Field TO_INSTITUTION_ADMIN_ON_PR_UNREFERENCE.
     */
    TO_INSTITUTION_ADMIN_ON_PR_UNREFERENCE("/email/prToInstitutionAdminOnUnreference.xhtml"),

    /**
     * Field TO_ADMINS_ON_CRAWLER_END.
     */
    TO_ADMINS_ON_CRAWLER_END("/email/prToAdminsOnCrawlerEnd.xhtml"),

    /**
     * Field TO_INSTITUTION_ADMIN_ON_PR_ADMIN_STATUS.
     */
    TO_INSTITUTION_ADMIN_ON_PR_ADMIN_STATUS("/email/prToInstitutionAdminOnAdminStatus.xhtml"),

    /**
     * Field TO_ADMIN_TO_TEST_EMAIL.
     */
    TO_ADMIN_TO_TEST_EMAIL("/email/toAdminToTestEmail.xhtml");

    /**
     * Field pageName.
     */
    private String pageName;

    /**
     * Constructor for EmailType.
     *
     * @param pageName String
     */
    EmailType(String pageName) {
        this.pageName = pageName;
    }

    /**
     * Method getPageName.
     *
     * @return String
     * @see net.ihe.gazelle.users.action.EmailTemplate#getPageName()
     */
    @Override
    public String getPageName() {
        return pageName;
    }
}
