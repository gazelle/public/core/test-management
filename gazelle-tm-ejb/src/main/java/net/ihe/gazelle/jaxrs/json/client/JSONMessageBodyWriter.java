package net.ihe.gazelle.jaxrs.json.client;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

/**
 * Resteasy Json message body writer.
 */
public class JSONMessageBodyWriter implements MessageBodyWriter<Object> {

    @Override
    public boolean isWriteable(Class<?> aClass, Type type, Annotation[] annotations, MediaType mediaType) {
        // returns true if media type is application/json
        if (mediaType == null) {
            return false;
        } else if (String.class.equals(type)) {
            return false;
        } else {
            String subtype = mediaType.getSubtype();
            boolean isApplicationMediaType = "application".equals(mediaType.getType());
            return isApplicationMediaType && "json".equalsIgnoreCase(subtype) || subtype.endsWith("+json") || subtype.equalsIgnoreCase("x-ndjson") || mediaType.isWildcardSubtype() && (mediaType.isWildcardType() || isApplicationMediaType);
        }
    }

    @Override
    public long getSize(Object o, Class<?> aClass, Type type, Annotation[] annotations, MediaType mediaType) {
        // delegates to ObjectMapperInstance
        return ObjectMapperInstance.getSize(o);
    }

    @Override
    public void writeTo(Object o, Class<?> aClass, Type type, Annotation[] annotations, MediaType mediaType, MultivaluedMap<String, Object> multivaluedMap, OutputStream outputStream) throws IOException, WebApplicationException {
        // delegates to ObjectMapperInstance
        ObjectMapperInstance.writeTo(o, outputStream);
    }
}
