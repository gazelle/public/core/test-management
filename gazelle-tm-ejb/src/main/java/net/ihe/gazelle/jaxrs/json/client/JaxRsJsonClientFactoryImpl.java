package net.ihe.gazelle.jaxrs.json.client;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.jboss.resteasy.client.ProxyFactory;
import org.jboss.resteasy.plugins.providers.RegisterBuiltin;
import org.jboss.resteasy.spi.ResteasyProviderFactory;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.annotation.PostConstruct;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * JAXRS JSON client factory.
 */
@Name("jaxRsJsonClientFactory")
@Scope(ScopeType.APPLICATION)
@AutoCreate
public class JaxRsJsonClientFactoryImpl implements JaxRsJsonClientFactory {

    /**
     * Cache of clients
     */
    private final LoadingCache<ClientKey, Object> cache = CacheBuilder.newBuilder()
            .maximumSize(1000)
            .expireAfterAccess(1, TimeUnit.DAYS)
            .build(new ClientKeyCacheLoader());

    /**
     * Init factory.
     */
    @PostConstruct
    public void init() {
        // register JSON body reader and writer
        ResteasyProviderFactory.getInstance().addMessageBodyReader(JSONMessageBodyReader.class);
        ResteasyProviderFactory.getInstance().addMessageBodyWriter(JSONMessageBodyWriter.class);
        // Configure JAXRS Resteasy
        RegisterBuiltin.register(ResteasyProviderFactory.getInstance());
    }

    @Override
    public <T> T getClient(Class<T> contract, String baseUrl) {
        try {
            // retrieve client from cache
            return (T) cache.get(new ClientKey(contract, baseUrl));
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    private static class ClientKeyCacheLoader extends CacheLoader<ClientKey, Object> {
        @Override
        public Object load(ClientKey key) {
            return createClient(key);
        }

        private Object createClient(ClientKey key) {
            return ProxyFactory.create(key.getContract(), key.getBaseUrl());
        }
    }
}
