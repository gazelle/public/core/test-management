package net.ihe.gazelle.jaxrs.json.client;

import javax.ejb.Local;

/**
 * JAXRS JSON client factory local.
 */
@Local
public interface JaxRsJsonClientFactory {
    /**
     * Gets a JAXRS JSON client.
     *
     * @param <T>      contract type
     * @param contract contract
     * @param baseUrl  server base url
     * @return client
     */
    <T> T getClient(Class<T> contract, String baseUrl);
}
