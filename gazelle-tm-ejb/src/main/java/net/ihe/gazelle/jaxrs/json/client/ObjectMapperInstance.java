package net.ihe.gazelle.jaxrs.json.client;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;

/**
 * Object mapper tool instance.
 */
public class ObjectMapperInstance {
    /**
     * Jackson ObjectMapper instance.
     * Do not fail on unknown properties
     * Do not include null/empty values
     */
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper()
            .setSerializationInclusion(JsonInclude.Include.NON_EMPTY)
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    private ObjectMapperInstance() {
        // static class
    }

    /**
     * Gets JSON size.
     *
     * @param o serialized object
     * @return JSON string size
     */
    public static long getSize(Object o) {
        try {
            return OBJECT_MAPPER.writeValueAsString(o).length();
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Write JSON to stream.
     *
     * @param o            serialized object
     * @param outputStream output stream
     * @throws IOException io exception
     */
    public static void writeTo(Object o, OutputStream outputStream) throws IOException {
        OBJECT_MAPPER.writeValue(outputStream, o);
    }

    /**
     * Read object from JSON stream.
     *
     * @param type         object type
     * @param genericType  generic type
     * @param entityStream entity stream
     * @return deserialized object
     * @throws IOException io exception
     */
    public static Object readFrom(Class<Object> type, Type genericType, InputStream entityStream) throws IOException {
        JavaType javaType = OBJECT_MAPPER.getTypeFactory().constructType(genericType != null ? genericType : type);
        return OBJECT_MAPPER.readValue(entityStream, javaType);
    }
}
