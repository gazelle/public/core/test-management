package net.ihe.gazelle.tf.action.converter;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.tf.model.Standard;
import net.ihe.gazelle.tf.model.StandardQuery;
import net.ihe.gazelle.tf.model.Transaction;
import net.ihe.gazelle.tf.model.export.StandardIE;

import javax.persistence.EntityManager;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class StandardToXmlConverter {

    public String standardToXml(List<Standard> standards) throws JAXBException {

        StandardIE standardIE = this.listToIE(standards);

        JAXBContext jaxbContext = JAXBContext.newInstance(StandardIE.class, Standard.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
        StringWriter sw = new StringWriter();
        jaxbMarshaller.marshal(standardIE, sw);
        return sw.toString();
    }

    private StandardIE listToIE(List<Standard> standards){
        return new StandardIE(standards);
    }


    public StandardIE extractAndCheckStandards(String xmlString) throws JAXBException{
        StandardIE standardIE = xmlToStandards(xmlString);
        standardIE = checkUnknownTransactions(standardIE);
        standardIE = searchForDuplicate(standardIE);
        return standardIE;
    }

    public StandardIE xmlToStandards(String xmlString) throws JAXBException {

        JAXBContext jaxbContext = JAXBContext.newInstance(StandardIE.class, Standard.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

        Reader reader = new InputStreamReader(new ByteArrayInputStream
                (xmlString.getBytes(StandardCharsets.UTF_8)), StandardCharsets.UTF_8);
        return (StandardIE) unmarshaller.unmarshal(reader);
    }

    private StandardIE checkUnknownTransactions(StandardIE standardIE){
        List<Standard> standards = standardIE.getStandards();
        Transaction transaction;
        List<Transaction> transactions;
        for (Standard standard : standards){
            transactions = new ArrayList<>();
            if (standard.getTransactionKeywords() != null){
                for (String transactionKeyword : standard.getTransactionKeywords()) {
                    if (transactionKeyword != null && !transactionKeyword.isEmpty()){
                        transaction = Transaction.GetTransactionByKeyword(transactionKeyword);
                        if (transaction == null){
                            standardIE.addUnknownTransaction(transactionKeyword);
                        } else {
                            transactions.add(transaction);
                        }
                    }
                }
            }
            standard.setTransactions(transactions);
        }
        return standardIE;
    }


    private StandardIE searchForDuplicate(StandardIE standardIE){

        EntityManager entityManager = EntityManagerService.provideEntityManager();
        StandardQuery standardQuery = new StandardQuery(entityManager);
        List<Standard> standardsFromBase = standardQuery.getList();
        boolean duplicate;

        List<Standard> standardsToImport = standardIE.getStandards();
        Iterator iterator = standardsToImport.iterator();
        while (iterator.hasNext()) {
            Standard standard = ((Standard)iterator.next());
            duplicate = false;
            for (Standard standardFromBase : standardsFromBase){
                if (match(standard, standardFromBase)){
                    duplicate = true;
                }
            }
            if (duplicate){
                iterator.remove();
                standardIE.addDuplicatedStandard(standard);
            }
        }
        standardIE.setStandards(standardsToImport);
        return standardIE;
    }

    public boolean match(Standard standard1, Standard standard2){

        if (!areNullableStringsEquals(standard1.getKeyword(), standard2.getKeyword())
                || !areNullableStringsEquals(standard1.getVersion(), standard2.getVersion())
                || !areNullableStringsEquals(standard1.getUrl(), standard2.getUrl())){
            return false;
        }
        if (standard1.getNetworkCommunicationType() != null && standard2.getNetworkCommunicationType() != null){
            if (!standard1.getNetworkCommunicationType().equals(standard2.getNetworkCommunicationType())){
                return false;
            }
        } else if ((standard1.getNetworkCommunicationType() == null || standard2.getNetworkCommunicationType() == null) &&
                (standard1.getNetworkCommunicationType() != null || standard2.getNetworkCommunicationType() != null)){
            return false;
        }
        return true;
    }

    private boolean areNullableStringsEquals(String string1, String string2){
        if (string1 != null){
            return string1.equals(string2);
        } else {
            return string2 == null;
        }
    }
}
