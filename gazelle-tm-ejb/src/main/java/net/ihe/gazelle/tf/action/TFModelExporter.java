package net.ihe.gazelle.tf.action;

import net.ihe.gazelle.documents.converter.DocumentToXmlConverter;
import net.ihe.gazelle.objects.model.ObjectType;
import net.ihe.gazelle.tf.action.converter.Hl7MessageProfileToXmlConverter;
import net.ihe.gazelle.tf.action.converter.SamplesTypeToXmlConverter;
import net.ihe.gazelle.tf.action.converter.StandardToXmlConverter;
import net.ihe.gazelle.tf.action.converter.TFRuleToXmlConverter;
import net.ihe.gazelle.tf.auditMessage.converter.AuditMessageToXmlConverter;
import net.ihe.gazelle.tf.model.Document;
import net.ihe.gazelle.tf.model.Hl7MessageProfile;
import net.ihe.gazelle.tf.model.Standard;
import net.ihe.gazelle.tf.model.auditMessage.AuditMessage;
import net.ihe.gazelle.tf.model.constraints.AipoRule;
import net.ihe.gazelle.tm.configurations.action.converter.OIDRootDefinitionToXmlConverter;
import net.ihe.gazelle.tm.configurations.converter.ConfigurationTypeMappedWithAIPOToXmlConverter;
import net.ihe.gazelle.tm.configurations.model.ConfigurationTypeMappedWithAIPO;
import net.ihe.gazelle.tm.configurations.model.OIDRootDefinition;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * Static class that allows to export any model element from its Java representation and trigger the download of an XML File.
 */
public class TFModelExporter {

    private static final Logger LOG = LoggerFactory.getLogger(TFModelExporter.class);

    private static final String XML_CONTENT_TYPE = "text/xml; charset=UTF-8";

    /**
     * Export a list of TF Rules
     *
     * @param rules list of rules to export.
     * @throws JAXBException if the marshalling cannot be done.
     */
    public static void exportTFRules(List<AipoRule> rules) throws JAXBException {
        TFRuleToXmlConverter tfRuleToXmlConverter = new TFRuleToXmlConverter();
        String xmlRules = tfRuleToXmlConverter.rulesToXml(rules);
        downloadStringContent(xmlRules, XML_CONTENT_TYPE, "tfRules.xml");
    }

    /**
     * Export a list of Standards.
     *
     * @param standards list of Standards to export.
     * @throws JAXBException if the marshalling cannot be done.
     */
    public static void exportStandards(List<Standard> standards) throws JAXBException {
        StandardToXmlConverter standardToXmlConverter = new StandardToXmlConverter();
        String xmlStandards = standardToXmlConverter.standardToXml(standards);
        downloadStringContent(xmlStandards, XML_CONTENT_TYPE, "standards.xml");
    }

    /**
     * Export a list of HL7 Message Profiles.
     *
     * @param profiles list of profiles to export
     * @throws JAXBException if the marshalling cannot be done.
     */
    public static void exportHl7MessageProfiles(List<Hl7MessageProfile> profiles) throws JAXBException {
        Hl7MessageProfileToXmlConverter converter = new Hl7MessageProfileToXmlConverter();
        String xmlProfiles = converter.hl7MessageProfilesToXml(profiles);
        downloadStringContent(xmlProfiles, XML_CONTENT_TYPE, "hl7MessageProfiles.xml");
    }

    /**
     * Export a list of configurations.
     *
     * @param list list of configurations to export.
     * @throws JAXBException if the marshalling cannot be done.
     */
    public static void exportConfigurations(List<ConfigurationTypeMappedWithAIPO> list) throws JAXBException {
        ConfigurationTypeMappedWithAIPOToXmlConverter converter = new ConfigurationTypeMappedWithAIPOToXmlConverter();
        String xmlConfigurations = converter.configurationsToXml(list);
        downloadStringContent(xmlConfigurations, XML_CONTENT_TYPE, "configurations.xml");
    }

    /**
     * Export a list of Documents.
     *
     * @param documents list of Documents to export.
     * @throws JAXBException if the marshalling cannot be done.
     */
    public static void exportDocuments(List<Document> documents) throws JAXBException {
        DocumentToXmlConverter converter = new DocumentToXmlConverter();
        String xmlDocuments = converter.documentsToXml(documents);
        downloadStringContent(xmlDocuments, XML_CONTENT_TYPE, "documents.xml");
    }

    /**
     * Export a list of Audit Message.
     *
     * @param auditMessageList list of Audit Message to export.
     * @throws JAXBException if the marshalling cannot be done.
     */
    public static void exportAuditMessage(List<AuditMessage> auditMessageList) throws JAXBException {
        AuditMessageToXmlConverter converter = new AuditMessageToXmlConverter();
        String xmlAuditMessages = converter.auditMessagesToXml(auditMessageList);
        downloadStringContent(xmlAuditMessages, XML_CONTENT_TYPE, "auditMessages.xml");
    }

    /**
     * Export a list of Object Type.
     *
     * @param objectTypes list of Object Type to export.
     * @throws JAXBException if the marshalling cannot be done.
     */
    public static void exportObjectType(List<ObjectType> objectTypes) throws JAXBException {
        SamplesTypeToXmlConverter converter = new SamplesTypeToXmlConverter();
        String samplesTypeToXml = converter.samplesTypeToXml(objectTypes);
        downloadStringContent(samplesTypeToXml, XML_CONTENT_TYPE, "objectTypes.xml");
    }

    /**
     * Export a list of OID Root Definition.
     *
     * @param oidRootDefinitions list of OID Root Definition to export.
     * @throws JAXBException if the marshalling cannot be done.
     */
    public static void exportOIDRootDefinition(List<OIDRootDefinition> oidRootDefinitions) throws JAXBException {
        OIDRootDefinitionToXmlConverter converter = new OIDRootDefinitionToXmlConverter();
        String oidRootDefinitionToXml = converter.oidRootDefinitionToXml(oidRootDefinitions);
        downloadStringContent(oidRootDefinitionToXml, XML_CONTENT_TYPE, "oidRootDefinitions.xml");
    }

    /**
     * Trigger the download of an file from string content.
     *
     * @param content     literal value of the file's content.
     * @param contentType content type of the file.
     * @param description description of the file (used as file name)
     */
    private static void downloadStringContent(String content, String contentType, String description) {
        final FacesContext fc = FacesContext.getCurrentInstance();
        final ExternalContext extCtx = fc.getExternalContext();
        final HttpServletResponse response = (HttpServletResponse) extCtx.getResponse();

        response.setContentType(contentType);
        response.setHeader("Content-Disposition", "attachment; filename=\"" + description + "\"");
        response.setContentLength(content.getBytes(StandardCharsets.UTF_8).length);

        InputStream messageStream = new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8));
        ServletOutputStream servletOutputStream = null;
        try {
            servletOutputStream = response.getOutputStream();

            final byte[] buf = new byte[8192];
            while (true) {

                final int length = messageStream.read(buf);
                if (length < 0) {
                    break;
                }
                servletOutputStream.write(buf, 0, length);
            }
            servletOutputStream.flush();
        } catch (final IOException e) {
            LOG.error("Failed to download Model elements : ", e);
            try {
                IOUtils.write("Failed to download file " + ExceptionUtils.getFullStackTrace(e), servletOutputStream);
            } catch (final IOException e1) {
                LOG.error("Failed to send error to browser : ", e);
            }
        } finally {
            IOUtils.closeQuietly(messageStream);
            if (servletOutputStream != null) {
                IOUtils.closeQuietly(servletOutputStream);
            }
            fc.responseComplete();
        }
    }
}
