package net.ihe.gazelle.tf.auditMessage.converter;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.tf.model.*;
import net.ihe.gazelle.tf.model.auditMessage.AuditMessage;
import net.ihe.gazelle.tf.model.auditMessage.AuditMessageQuery;
import net.ihe.gazelle.tf.model.auditMessage.export.AuditMessageIE;

import javax.persistence.EntityManager;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.List;

public class AuditMessageToXmlConverter {

    public String auditMessagesToXml(List<AuditMessage> auditMessages) throws JAXBException {

        AuditMessageIE auditMessageIE = this.listToIE(auditMessages);

        JAXBContext jaxbContext = JAXBContext.newInstance(AuditMessageIE.class, AuditMessage.class,
               Transaction.class, Actor.class, DocumentSection.class, Document.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        StringWriter sw = new StringWriter();
        jaxbMarshaller.marshal(auditMessageIE, sw);
        return sw.toString();
    }

    private AuditMessageIE listToIE(List<AuditMessage> auditMessages){
        return new AuditMessageIE(auditMessages);
    }

    public AuditMessageIE extractAndCheckAuditMessages(String xmlAuditMessages) throws JAXBException {
        AuditMessageIE auditMessageIE = xmlToAuditMessages(xmlAuditMessages);
        auditMessageIE = checkUnknownElements(auditMessageIE);
        auditMessageIE = searchForDuplicate(auditMessageIE);
        return auditMessageIE;
    }

    public AuditMessageIE xmlToAuditMessages(String xmlString) throws JAXBException {

        JAXBContext jaxbContext = JAXBContext.newInstance(AuditMessageIE.class, AuditMessage.class,
                Transaction.class, Actor.class, DocumentSection.class, Document.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

        Reader reader = new InputStreamReader(new ByteArrayInputStream
                (xmlString.getBytes(StandardCharsets.UTF_8)), StandardCharsets.UTF_8);
        return (AuditMessageIE) unmarshaller.unmarshal(reader);
    }

    private AuditMessageIE checkUnknownElements(AuditMessageIE auditMessageIE){

        List<AuditMessage> auditMessages = auditMessageIE.getAuditMessages();
        Iterator iterator = auditMessages.iterator();
        while (iterator.hasNext()) {
            AuditMessage auditMessage = ((AuditMessage)iterator.next());
            if (getModelElementsFromKeyword(auditMessageIE, auditMessage) == null){
                iterator.remove();
                auditMessageIE.addIgnoredAuditMessage(auditMessage);
            }
        }
        auditMessageIE.setAuditMessages(auditMessages);
        return auditMessageIE;
    }

    private AuditMessage getModelElementsFromKeyword(AuditMessageIE auditMessageIE, AuditMessage auditMessage){
        String transactionKeyword = null;
        String actorKeyword = null;
        boolean sectionIsNull = true;

        Transaction transaction = null;
        Actor actor = null;
        DocumentSection documentSection = null;

        if (auditMessage.getTransactionKeyword() != null) {
            transactionKeyword = auditMessage.getTransactionKeyword();
            transaction = Transaction.GetTransactionByKeyword(transactionKeyword);
            if (transaction == null){
                auditMessageIE.addUnknownTransaction(transactionKeyword);
            }
        }
        if (auditMessage.getIssuingActor() != null){
            actorKeyword = auditMessage.getIssuingActor().getKeyword();
            actor = Actor.findActorWithKeyword(actorKeyword);
            if (actor == null){
                auditMessageIE.addUnknownActor(actorKeyword);
            }
        }


        if (auditMessage.getDocumentSection() != null) {
            sectionIsNull = false;
            DocumentSectionQuery query = new DocumentSectionQuery();
            query.section().eq(auditMessage.getDocumentSection().getSection());
            query.type().eq(auditMessage.getDocumentSection().getType());
            if (auditMessage.getDocumentSection().getDocument() != null) {
                query.document().document_md5_hash_code().eq(auditMessage.getDocumentSection().getDocument().getDocument_md5_hash_code());
            }
            documentSection = query.getUniqueResult();
            if (documentSection == null) {
                auditMessageIE.addUnknownSection(auditMessage.getDocumentSection().getSectionHumanized());
            }
        }

        if ((actorKeyword != null && actor == null)
                || (transactionKeyword != null && transaction == null)
                || (!sectionIsNull && documentSection == null)){
            return null;
        }

        auditMessage.setIssuingActor(actor);
        auditMessage.setAuditedTransaction(transaction);
        auditMessage.setDocumentSection(documentSection);
        return auditMessage;
    }

    private AuditMessageIE searchForDuplicate(AuditMessageIE auditMessageIE){
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        AuditMessageQuery auditMessageQuery = new AuditMessageQuery(entityManager);
        List<AuditMessage> auditMessagesFromBase = auditMessageQuery.getList();
        boolean duplicate;

        List<AuditMessage> auditMessages = auditMessageIE.getAuditMessages();
        Iterator iterator = auditMessages.iterator();
        while (iterator.hasNext()) {
            AuditMessage auditMessage = ((AuditMessage)iterator.next());
            duplicate = false;
            for (AuditMessage auditMessageFromBase : auditMessagesFromBase){
                if (match(auditMessage, auditMessageFromBase)){
                    duplicate = true;
                }
            }
            if (duplicate){
                iterator.remove();
                auditMessageIE.addDuplicatedAuditMessage(auditMessage);
            }
        }
        auditMessageIE.setAuditMessages(auditMessages);
        return auditMessageIE;
    }

    private boolean match(AuditMessage auditMessage1, AuditMessage auditMessage2){

        if (auditMessage1.getAuditedEvent() != auditMessage2.getAuditedEvent()){
            return false;
        }
        if (!equalsNullableStrings(auditMessage1.getOid(), auditMessage2.getOid())){
            return false;
        }
        if (auditMessage1.getAuditedTransaction() != null){
            if (auditMessage2.getAuditedTransaction() != null){
                if (!equalsNullableStrings(auditMessage1.getAuditedTransaction().getKeyword(), auditMessage2.getAuditedTransaction().getKeyword())){
                    return false;
                }
            } else {
                return false;
            }
        }
        if (auditMessage1.getIssuingActor() != null){
            if (auditMessage2.getIssuingActor() != null){
                if (!equalsNullableStrings(auditMessage1.getIssuingActor().getKeyword(), auditMessage2.getIssuingActor().getKeyword())){
                    return false;
                }
            } else {
                return false;
            }
        }
        if (!equalsNullableStrings(auditMessage1.getEventCodeType(), auditMessage2.getEventCodeType())){
            return false;
        }

        if (auditMessage1.getDocumentSection() != null){
            if (auditMessage2.getDocumentSection() != null){
                if (!equalsNullableStrings(auditMessage1.getDocumentSection().getSection(), auditMessage2.getDocumentSection().getSection())){
                    return false;
                }
                if (auditMessage1.getDocumentSection().getType() != auditMessage2.getDocumentSection().getType()){
                    return false;
                }
                if (auditMessage1.getDocumentSection().getDocument() != null){
                    if (auditMessage2.getDocumentSection().getDocument() != null){
                        if (!equalsNullableStrings(auditMessage1.getDocumentSection().getDocument().getDocument_md5_hash_code(),
                                auditMessage2.getDocumentSection().getDocument().getDocument_md5_hash_code())){
                            return false;
                        }
                    } else {
                        return false;
                    }
                }
            } else {
                return false;
            }
        }

        return true;

    }

    private boolean equalsNullableStrings(String s1, String s2){
        if (s1 != null){
            return s1.equals(s2);
        } else {
            return s2 == null;
        }
    }
}
