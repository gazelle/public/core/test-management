package net.ihe.gazelle.documents.converter;

import net.ihe.gazelle.tf.model.*;
import net.ihe.gazelle.tf.model.export.DocumentIE;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DocumentToXmlConverter {

    public String documentsToXml(List<Document> documents) throws JAXBException {

        DocumentIE documentIE = new DocumentIE(documents);

        JAXBContext jaxbContext = JAXBContext.newInstance(DocumentIE.class, Document.class,
                Domain.class, DocumentSection.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        StringWriter sw = new StringWriter();
        jaxbMarshaller.marshal(documentIE, sw);
        return sw.toString();
    }

    public DocumentIE extractAndCheckDocuments(String xmlDocuments) throws JAXBException{
        DocumentIE documentIE = xmlToDocuments(xmlDocuments);
        documentIE = checkUnknownDomains(documentIE);
        documentIE = searchForDuplicate(documentIE);
        return documentIE;
    }


    private DocumentIE xmlToDocuments(String xmlString) throws JAXBException {

        JAXBContext jaxbContext = JAXBContext.newInstance(DocumentIE.class, Document.class,
                Domain.class, DocumentSection.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

        Reader reader = new InputStreamReader(new ByteArrayInputStream
                (xmlString.getBytes(StandardCharsets.UTF_8)), StandardCharsets.UTF_8);
        return (DocumentIE) unmarshaller.unmarshal(reader);
    }

    private DocumentIE checkUnknownDomains(DocumentIE documentIE){

        List<Document> documents = documentIE.getDocuments();
        Domain domain;

        Iterator iterator = documents.iterator();
        while (iterator.hasNext()) {
            Document document = ((Document)iterator.next());

            if (document.getDomain() != null){
                domain = Domain.getDomainByKeyword(document.getDomain().getKeyword());
                if (domain == null){
                    documentIE.addUnknownDomain(document.getDomain().getKeyword());
                    iterator.remove();
                    documentIE.addIgnoredDocument(document);
                } else {
                    document.setDomain(domain);
                }
            }
        }

        documentIE.setDocuments(documents);
        return documentIE;
    }

    private DocumentIE searchForDuplicate(DocumentIE documentIE){

        List<Document> documents = documentIE.getDocuments();
        Iterator documentIterator = documents.iterator();
        while (documentIterator.hasNext()) {
            Document document = ((Document)documentIterator.next());

            DocumentQuery query = new DocumentQuery();
            query.document_md5_hash_code().eq(document.getDocument_md5_hash_code());
            Document documentFromBase = query.getUniqueResult();
            if (documentFromBase != null){
                documentFromBase.setDocumentSection(new ArrayList<DocumentSection>());
                documentIE.addDuplicatedDocument(documentFromBase);
                documentIterator.remove();
            }
        }

        List<DocumentSection> sections = documentIE.getDocumentSections();
        Iterator sectionIterator = sections.iterator();
        while (sectionIterator.hasNext()){
            DocumentSection section = (DocumentSection) sectionIterator.next();

            if (section.getDocument() != null){
                Document ignoredDocument = documentIE.getIgnoredDocument(section.getDocument().getDocument_md5_hash_code());
                if (ignoredDocument != null){
                    if (ignoredDocument.getDocumentSection() == null){
                        ignoredDocument.setDocumentSection(new ArrayList<DocumentSection>());
                    }
                    ignoredDocument.getDocumentSection().add(section);
                    sectionIterator.remove();
                } else {
                    Document document = documentIE.getDocument(section.getDocument().getDocument_md5_hash_code());
                    if (document != null){
                        DocumentSectionQuery query = new DocumentSectionQuery();
                        query.section().eq(section.getSection());
                        query.type().eq(section.getType());
                        if (section.getDocument() != null){
                            query.document().document_md5_hash_code().eq(section.getDocument().getDocument_md5_hash_code());
                        }
                        DocumentSection documentSectionInBase = query.getUniqueResult();
                        if (documentSectionInBase == null){
                            if (document.getDocumentSection() != null){
                                document.getDocumentSection().add(section);
                            } else {
                                document.setDocumentSection(new ArrayList<DocumentSection>());
                                document.getDocumentSection().add(section);
                            }
                        } else {
                            documentIE.addIgnoredSection(section);
                        }
                        sectionIterator.remove();
                    } else {
                        sectionIterator.remove();
                        documentIE.addIgnoredSection(section);
                    }
                }
            } else {
                sectionIterator.remove();
                documentIE.addIgnoredSection(section);
            }
        }
        documentIE.setDocuments(documents);
        documentIE.setDocumentSections(sections);
        return documentIE;
    }
}
