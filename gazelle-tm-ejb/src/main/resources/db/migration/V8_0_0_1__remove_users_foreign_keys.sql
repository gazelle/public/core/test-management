---
-- Migration script for TM 8.0.0 (GUM Step 2)
-- Written by: Valentin Lorand, Claude Lusseau, Clément Lagorce
-- Date: 2023-08-11
----

-- Drop old and unused tables
DROP TABLE IF EXISTS public.tm_ctt_token; -- Should be removed in TM:5.16.0
DROP TABLE IF EXISTS public.tm_simulator; -- Should be removed in TM:X.X.X (EU-CAT + NA-CAT)

-- Drop old foreign keys
ALTER TABLE public.tm_user_comment DROP CONSTRAINT IF EXISTS fkf90fedf196e9f6ac; -- Should be removed in TM:3.3.28 (NA-CAT)

-- Drop related views
DROP VIEW IF EXISTS x_user_role; -- Used by tm_system and usr_user_role tables
DROP VIEW IF EXISTS x_system_contacts; -- Used by tm_system
DROP VIEW IF EXISTS x_system_owner; -- Used by tm_system

-- tm_testing_session_admin table
ALTER TABLE public.tm_testing_session_admin DROP CONSTRAINT IF EXISTS fk_3iy6socfnp2sr6mrru70yb39a;
ALTER TABLE public.tm_testing_session_admin DROP CONSTRAINT IF EXISTS fk471f873196e9f6ac; -- From old platforms
ALTER TABLE public.tm_testing_session_admin ALTER COLUMN user_id TYPE VARCHAR;
UPDATE public.tm_testing_session_admin as t1 set "user_id" = t2.username from usr_users as t2 where t2.id = t1.user_id::integer;

-- tm_system table
ALTER TABLE public.tm_system DROP CONSTRAINT IF EXISTS fk_60phploi6or05gwfh426rvwhd;
ALTER TABLE public.tm_system DROP CONSTRAINT IF EXISTS fkd66ed6b583952d20; -- From old platforms
ALTER TABLE public.tm_system ALTER COLUMN owner_user_id TYPE VARCHAR;
UPDATE public.tm_system as t1 set "owner_user_id" = t2.username from usr_users as t2 where t2.id = t1.owner_user_id::integer;

-- tm_monitor_in_session table
ALTER TABLE public.tm_monitor_in_session DROP CONSTRAINT IF EXISTS fk_9dmu3nvkx0blel6oqbqaph93r;
ALTER TABLE public.tm_monitor_in_session DROP CONSTRAINT IF EXISTS fk7692a90796e9f6ac; -- From old platforms
ALTER TABLE public.tm_monitor_in_session ALTER COLUMN user_id TYPE VARCHAR;
UPDATE public.tm_monitor_in_session as t1 set "user_id" = t2.username from usr_users as t2 where t2.id = t1.user_id::integer;

-- tm_system_in_session_user table
ALTER TABLE public.tm_system_in_session_user DROP CONSTRAINT IF EXISTS fk_pqb7g2f0lw5upbumnaauvopwv;
ALTER TABLE public.tm_system_in_session_user DROP CONSTRAINT IF EXISTS fk3942132496e9f6ac; -- From old platforms
ALTER TABLE public.tm_system_in_session_user ALTER COLUMN user_id TYPE VARCHAR;
UPDATE public.tm_system_in_session_user as t1 set "user_id" = t2.username from usr_users as t2 where t2.id = t1.user_id::integer;

-- usr_messages_usr_users table
ALTER TABLE public.usr_messages_usr_users DROP CONSTRAINT IF EXISTS fk_s9eac53wxfkvq1gk2wjdurn38;
ALTER TABLE public.usr_messages_usr_users DROP CONSTRAINT IF EXISTS fk96e554f58fdacf4f;  -- From old platforms
ALTER TABLE public.usr_messages_usr_users ALTER COLUMN users_id TYPE VARCHAR;
UPDATE public.usr_messages_usr_users as t1 set "users_id" = t2.username from usr_users as t2 where t2.id = t1.users_id::integer;

-- DROP Foreign key Constraints
--ALTER TABLE usr_users DROP CONSTRAINT IF EXISTS fka4b7379d2fde29a8;
--ALTER TABLE usr_user_role DROP CONSTRAINT IF EXISTS fk8003cb7f96e9f6ac;

-- ALTER data types
--ALTER TABLE usr_users ALTER COLUMN id SET DATA TYPE VARCHAR(255);

-- ADD user_id column in some tables
ALTER TABLE tm_test_instance_event ADD COLUMN user_id VARCHAR(255);