ALTER TABLE tm_testing_session ADD COLUMN name character varying(255) UNIQUE;
ALTER TABLE tm_testing_session ADD COLUMN type_zone_year_backup character varying(255);
UPDATE tm_testing_session SET type_zone_year_backup = CONCAT(type, ' ', zone, ' ', year) WHERE type_zone_year_backup IS NULL;
UPDATE tm_testing_session SET name = concat(description, ' (', type_zone_year_backup, ')') WHERE name IS NULL;
ALTER TABLE tm_testing_session DROP COLUMN IF EXISTS type;
ALTER TABLE tm_testing_session DROP COLUMN IF EXISTS zone;
ALTER TABLE tm_testing_session DROP COLUMN IF EXISTS year;
