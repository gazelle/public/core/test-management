package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.stub;

import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;

public class GazelleIdentityStub implements GazelleIdentity {

    private String userId;

    @Override
    public boolean isLoggedIn() {
        return true;
    }

    @Override
    public String getUsername() {
        return userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String getFirstName() {
        return "";
    }

    @Override
    public String getLastName() {
        return "";
    }

    @Override
    public String getEmail() {
        return "";
    }

    @Override
    public String getOrganisationKeyword() {
        return "";
    }

    @Override
    public String getDisplayName() {
        return "";
    }

    @Override
    public boolean hasRole(String s) {
        return false;
    }

    @Override
    public boolean hasPermission(String s, String s1, Object... objects) {
        return false;
    }

    @Override
    public boolean hasPermission(Object o, String s) {
        return false;
    }
}
