package net.ihe.gazelle.tm.gazelletest.dao;


import net.ihe.gazelle.tm.gazelletest.action.TestStepDataNotFoundException;
import net.ihe.gazelle.tm.gazelletest.model.instance.DataType;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestStepsData;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;


public class TestInstanceDataDAOInMemoryTest {

    private static final String PERSISTENCE_UNIT_NAME_CONST = "PersistenceUnitTest";

    private static EntityManager entityManager;

    private TestInstanceDataDAO testInstanceDataDAO;


    @BeforeClass
    public static void initializeDataBase() {

        entityManager = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME_CONST).createEntityManager();
        entityManager.getTransaction().begin();


        setUpDateBaseForTest(entityManager);
        entityManager.getTransaction().commit();

    }

    private static void setUpDateBaseForTest(EntityManager entityManager) {
        // Add DataType
        DataType dataType = new DataType(null, "file", "file uploaded by user");
        entityManager.merge(dataType);

        TestStepsData testStepsData = new TestStepsData(null, new DataType(1, "file", "file uploaded by user"), "test", "testFile.xml");
        testStepsData.setLastModifierId("user1");
        entityManager.merge(testStepsData);

        TestStepsData testStepsData1 = new TestStepsData(null, new DataType(1, "file", "file uploaded by user"), "test", "testFile2.xml");
        testStepsData1.setLastModifierId("user2");
        entityManager.merge(testStepsData1);
    }

    /**
     * Initialization method to create EntityManager and JPADAO test implementation.
     */
    @Before
    public void initializeEntityManager() {
        entityManager.getTransaction().begin();
        testInstanceDataDAO = new TestInstanceDataDAOImpl(entityManager);
        // Mockito.when(entityManager.merge(testInstanceMock)).thenReturn(testInstanceMock);
    }


    /**
     * Close the transaction corresponding to the test case when it is over.
     */
    @After
    public void commitTransaction() {
        if (entityManager.getTransaction().isActive()) {
            entityManager.getTransaction().commit();
        }
    }

    /**
     * Close the connection to the database after all tests are executed.
     */
    @AfterClass
    public static void closeDatabase() {
        entityManager.close();
    }


    @Test
    public void createTestStepData() throws TestStepDataNotFoundException {
        TestStepsData testStepsDataToSave = new TestStepsData(null, new DataType(1, "file", "file uploaded by user"), "test", "test");
        TestStepsData savedTestStepsData = testInstanceDataDAO.createTestStepData(testStepsDataToSave);
        TestStepsData retrievedTestInstanceDataDAO = testInstanceDataDAO.getTestStepDataById(3);
        Assert.assertEquals("test", savedTestStepsData.getValue());
        Assert.assertEquals("test", savedTestStepsData.getComment());
        Assert.assertEquals(3, savedTestStepsData.getId().intValue());
    }

    @Test
    public void instantiateTestStepDataExisting() {

        TestStepsData testStepsData = testInstanceDataDAO.instantiateTestStepDataFile("junit", "testFile.xml");
        Assert.assertEquals("testFile.xml", testStepsData.getValue());
        Assert.assertEquals("junit", testStepsData.getLastModifierId());
    }

}