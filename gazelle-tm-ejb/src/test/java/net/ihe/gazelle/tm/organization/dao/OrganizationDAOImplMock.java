package net.ihe.gazelle.tm.organization.dao;

import net.ihe.gazelle.tm.gazelletest.action.conformityTestReport.mocks.ObjectMocks;
import net.ihe.gazelle.tm.organization.OrganizationServiceDAO;
import net.ihe.gazelle.users.model.Address;
import net.ihe.gazelle.users.model.DelegatedOrganization;
import net.ihe.gazelle.users.model.Institution;
import net.ihe.gazelle.users.model.PersonFunction;

import javax.persistence.PersistenceException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class OrganizationDAOImplMock implements OrganizationServiceDAO {

    private final ObjectMocks objectMocks = new ObjectMocks(1);
    @Override
    public List<PersonFunction> getRequiredRolesToCompleteRegistration() {
        return null;
    }

    @Override
    public boolean hasOrganisationBillingContact(Institution institution) {
        return false;
    }

    @Override
    public Institution getCurrentOrganization() {
        return null;
    }

    @Override
    public List<Institution> getAllInstitution() {
        List<Institution> institutions = new ArrayList<>();
        // Add institution
        institutions.add(objectMocks.createInstitutionWithMandatoryFields());
        institutions.add(objectMocks.createDelegatedInstitutionWithMandatoryFields());
        // Add institution with keyword NULL
        Institution institution = objectMocks.createInstitutionWithMandatoryFields();
        institution.setKeyword("NULL");
        institutions.add(institution);

        return institutions;
    }

    @Override
    public Institution getInstitutionWithKeyword(String keyword) {
        if ("notExists".equals(keyword)) {
            throw new NoSuchElementException("Organization does not exist");
        }

        if ("notDelegated".equals(keyword))
            throw new NoSuchElementException("Organization is not delegated");

        return objectMocks.createInstitutionWithMandatoryFieldsByKeyword(keyword);
    }

    @Override
    public Institution getInstitutionById(Integer id) {
        //do nothing for UT
        return null;
    }

    @Override
    public Institution findOrganisationByName(String name) {
        return new Institution("Kereval1", "Ker1", "http://kereval.com");
    }

    @Override
    public DelegatedOrganization getDelegatedOrganizationById(Integer organizationId) {
        if (Integer.valueOf(1).equals(organizationId)) {
            DelegatedOrganization delegatedOrganization = new DelegatedOrganization();
            delegatedOrganization.setId(1);
            delegatedOrganization.setKeyword("KER");
            delegatedOrganization.setExternalId("KER");
            delegatedOrganization.setName("Organization name");
            return delegatedOrganization;
        }
        throw new NoSuchElementException("Delegated organization not found");
    }

    @Override
    public DelegatedOrganization getDelegatedOrganizationByKeyword(String keyword) {
        DelegatedOrganization delegatedOrganization = new DelegatedOrganization();
        delegatedOrganization.setId(1);
        delegatedOrganization.setKeyword("TEST_FALSE");
        delegatedOrganization.setExternalId("KER");
        delegatedOrganization.setIdpId("idp");
        delegatedOrganization.setName("Organization name");
        return delegatedOrganization;
    }

    @Override
    public void createInstitution(Institution institution, Address address) {
        // do nothing for UT
    }

    @Override
    public void createDelegatedOrganization(DelegatedOrganization delegatedOrganization) {
        // do nothing for UT
    }

    @Override
    public boolean isOrganizationExisting(String id) {
        return "exists".equals(id);
    }

    @Override
    public Institution updateOrganizationName(Institution institution, String name) {
        institution.setName(name);
        if ("duplicateName".equals(name))
            throw new PersistenceException("Duplicate key value violates unique constraint\n Detail: Key (name)=(" +
                    name + ") already exists");
        return institution;
    }

    @Override
    public Institution migrateLocalOrganizationToDelegated(Institution institution) {
        // do nothing for UT
        return institution;
    }

    @Override
    public boolean isThereAtLeastOneOrganization() {
        // placeholder for mock
        return false;
    }
}