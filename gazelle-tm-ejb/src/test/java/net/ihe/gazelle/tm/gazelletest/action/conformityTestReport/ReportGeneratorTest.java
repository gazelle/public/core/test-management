package net.ihe.gazelle.tm.gazelletest.action.conformityTestReport;

import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.tm.gazelletest.action.conformityTestReport.mocks.ConformityTestReportDAOMock;
import net.ihe.gazelle.tm.gazelletest.action.conformityTestReport.mocks.GazelleIdentityMock;
import net.ihe.gazelle.tm.gazelletest.action.conformityTestReport.mocks.ObjectMocks;
import net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.dao.ConformityTestReportDAO;
import net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.domain.ReportGenerator;
import net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.exceptions.UnauthorizedException;
import net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.model.ConformityTestReportModel;
import net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.model.InstitutionListModel;
import net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.model.SystemListModel;
import net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.model.TestingSessionListModel;
import net.ihe.gazelle.tm.systems.model.System;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.token.wsclient.InvalidTokenException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;

public class ReportGeneratorTest {

    private ReportGenerator spiedReportGenerator;

    @Before
    public void init() {
        ConformityTestReportDAO conformityTestReportDAOMock = new ConformityTestReportDAOMock();
        ReportGenerator reportGenerator = new ReportGenerator(conformityTestReportDAOMock);
        spiedReportGenerator =  Mockito.spy(reportGenerator);
    }

    public GazelleIdentityMock identityMockAdmin() {
        return GazelleIdentityMock.builder()
                .withUsername("testusername")
                .withOrganization("Ker1")
                .withRoles(new ArrayList<>(Collections.singletonList("admin_role")))
                .build();
    }

    public GazelleIdentityMock identityMock() {
        return GazelleIdentityMock.builder()
                .withUsername("testusername")
                .withOrganization("Ker1")
                .withRoles(new ArrayList<>(Collections.singletonList("vendor_role")))
                .build();
    }

    @Test
    public void exportOrganizationsNotAdminTest() {
        InstitutionListModel institutionListModel = null;
        try {
            institutionListModel = spiedReportGenerator.exportOrganisations(identityMock());
        } catch (InvalidTokenException e) {
            Assert.assertNull(institutionListModel);
        }
    }

    @Test
    public void exportOrganizationsNotAdminTest2() throws InvalidTokenException, ParseException {
        InstitutionListModel institutionListModel = spiedReportGenerator.exportOrganisations(identityMockAdmin());

        Assert.assertNotNull(institutionListModel);
        Assert.assertEquals("The report shall contain 1 institutions", 1, institutionListModel.getInstitutions().size());
    }

    @Test
    public void exportOrganizationsAdminTest() {
        InstitutionListModel institutionListModel = null;
        try {
            institutionListModel = spiedReportGenerator.exportOrganisations(identityMock());
        } catch (InvalidTokenException e) {
            Assert.assertNull(institutionListModel);
        }
    }

    @Test
    public void exportOrganizationsAdminTest2() throws InvalidTokenException, ParseException {
        InstitutionListModel institutionListModel = spiedReportGenerator.exportOrganisations(identityMockAdmin());

        Assert.assertNotNull(institutionListModel);
        Assert.assertEquals("The report shall contain 1 institutions", 1, institutionListModel.getInstitutions().size());
    }

    @Test
    public void exportTestingSessionInfosNotAdminTest() {
        TestingSessionListModel testingSessionListModel = null;
        try {
            testingSessionListModel = spiedReportGenerator.exportTestingSessionInfos(identityMock(), true);
        } catch (InvalidTokenException e) {
            Assert.assertNull(testingSessionListModel);
        }
    }

    @Test
    public void exportTestingSessionInfosNotAdminTest2() throws InvalidTokenException, ParseException {
        TestingSessionListModel testingSessionListModel = spiedReportGenerator.exportTestingSessionInfos(identityMockAdmin(), true);

        Assert.assertNotNull(testingSessionListModel);
        Assert.assertEquals("The report shall contain 1 testingSession", 1, testingSessionListModel.getTestingSession().size());
    }

    @Test
    public void exportTestingSessionInfosAdminTest() {
        TestingSessionListModel testingSessionListModel = null;
        try {
            testingSessionListModel = spiedReportGenerator.exportTestingSessionInfos(identityMock(), true);
        } catch (InvalidTokenException e) {
            Assert.assertNull(testingSessionListModel);
        }
    }

    @Test
    public void exportTestingSessionInfosAdminTest2() throws InvalidTokenException, ParseException {
        TestingSessionListModel testingSessionListModel = spiedReportGenerator.exportTestingSessionInfos(identityMockAdmin(), true);

        Assert.assertNotNull(testingSessionListModel);
        Assert.assertEquals("The report shall contain 1 testingSession", 1, testingSessionListModel.getTestingSession().size());
    }

    @Test
    public void exportSystemInfosNotAdminTest() {
        SystemListModel systemListModel = null;
        try {
            systemListModel = spiedReportGenerator.exportSystemInfos(identityMock(),  1, 1);
        } catch (InvalidTokenException | UnauthorizedException e) {
            Assert.assertNull(systemListModel);
        }
    }

    @Test
    public void exportSystemInfosNotAdminTest2() throws InvalidTokenException, UnauthorizedException, ParseException {
        SystemListModel systemListModel = spiedReportGenerator.exportSystemInfos(identityMockAdmin(), 1, 1);

        Assert.assertNotNull(systemListModel);
        Assert.assertEquals("The report shall contain 2 system", 2, systemListModel.getSystems().size());
    }

    @Test
    public void exportSystemInfosAdminTest() {
        SystemListModel systemListModel = null;
        try {
            systemListModel = spiedReportGenerator.exportSystemInfos(identityMock(), 1, 1);
        } catch (InvalidTokenException | UnauthorizedException e) {
            Assert.assertNull(systemListModel);
        }
    }

    @Test
    public void exportSystemInfosAdminTest2() throws InvalidTokenException, UnauthorizedException, ParseException {
        SystemListModel systemListModel = spiedReportGenerator.exportSystemInfos(identityMockAdmin(), 1, 1);

        Assert.assertNotNull(systemListModel);
        Assert.assertEquals("The report shall contain 2 system", 2, systemListModel.getSystems().size());
    }

    @Test
    public void exportexportSystemReportAsXMLNotAdminTest() {
        ConformityTestReportModel conformityTestReportModel = null;
        try {
            conformityTestReportModel = spiedReportGenerator.exportSystemReportAsXML(identityMock(), 1, 1);
        } catch (InvalidTokenException | UnauthorizedException e) {
            Assert.assertNull(conformityTestReportModel);
        }
    }

    @Test
    public void exportexportSystemReportAsXMLNotAdminTest2() throws InvalidTokenException, UnauthorizedException {
        ObjectMocks mock = new ObjectMocks(1);
        SystemInSession systemInSession = mock.createSystemIneSessionWithMandatoryFields();
        System system = mock.createSystemWithMandatoryFields();
        System spiedSystem = Mockito.spy(system);
        systemInSession.setSystem(spiedSystem);
        Mockito.doReturn(mock.createInstitutionWithMandatoryFields()).when(spiedSystem).getUniqueInstitution();
        ConformityTestReportModel conformityTestReportModel = spiedReportGenerator.getReportForSystem(systemInSession);

        Mockito.doReturn(conformityTestReportModel).when(spiedReportGenerator).getReportForSystem(Mockito.<SystemInSession>any());
        conformityTestReportModel = spiedReportGenerator.exportSystemReportAsXML(identityMockAdmin(), 1, 1);

        Assert.assertNotNull(conformityTestReportModel);
        Assert.assertEquals("The report shall contain 2 system", 2, conformityTestReportModel.getAipoModelList().size());
    }

    @Test
    public void exportexportSystemReportAsXMLAdminTest() {
        ConformityTestReportModel conformityTestReportModel = null;
        try {

            ObjectMocks mock = new ObjectMocks(1);
            SystemInSession systemInSession = mock.createSystemIneSessionWithMandatoryFields();
            System system = mock.createSystemWithMandatoryFields();
            System spiedSystem = Mockito.spy(system);
            systemInSession.setSystem(spiedSystem);
            Mockito.doReturn(mock.createInstitutionWithMandatoryFields()).when(spiedSystem).getUniqueInstitution();
            conformityTestReportModel = spiedReportGenerator.getReportForSystem(systemInSession);

            Mockito.doReturn(conformityTestReportModel).when(spiedReportGenerator).getReportForSystem(Mockito.<SystemInSession>any());
            conformityTestReportModel = spiedReportGenerator.exportSystemReportAsXML(identityMock(), 1, 1);
        } catch (InvalidTokenException | UnauthorizedException e) {
            Assert.assertNotNull(conformityTestReportModel);
        }
    }

    @Test
    public void exportexportSystemReportAsXMLAdminTest2() throws InvalidTokenException, UnauthorizedException {
        ObjectMocks mock = new ObjectMocks(1);
        SystemInSession systemInSession = mock.createSystemIneSessionWithMandatoryFields();
        System system = mock.createSystemWithMandatoryFields();
        System spiedSystem = Mockito.spy(system);
        systemInSession.setSystem(spiedSystem);
        Mockito.doReturn(mock.createInstitutionWithMandatoryFields()).when(spiedSystem).getUniqueInstitution();
        ConformityTestReportModel conformityTestReportModel = spiedReportGenerator.getReportForSystem(systemInSession);

        Mockito.doReturn(conformityTestReportModel).when(spiedReportGenerator).getReportForSystem(Mockito.<SystemInSession>any());
        conformityTestReportModel = spiedReportGenerator.exportSystemReportAsXML(identityMockAdmin(), 1, 1);

        Assert.assertNotNull(conformityTestReportModel);
        Assert.assertEquals("The report shall contain 2 system", 2, conformityTestReportModel.getAipoModelList().size());
    }
}