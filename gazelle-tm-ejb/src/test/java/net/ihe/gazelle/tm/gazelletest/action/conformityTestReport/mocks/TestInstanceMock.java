package net.ihe.gazelle.tm.gazelletest.action.conformityTestReport.mocks;

import net.ihe.gazelle.tm.gazelletest.model.instance.MonitorInSession;
import net.ihe.gazelle.tm.gazelletest.model.instance.Status;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;

public class TestInstanceMock extends TestInstance {
    private MonitorInSession monitorInSession;
    private Status lastStatus;

    @Override
    public void setMonitorInSession(MonitorInSession monitorInSession) {
        this.monitorInSession = monitorInSession;
    }

    @Override
    public MonitorInSession getMonitorInSession() {
        return this.monitorInSession;
    }

    @Override
    public void setLastStatus(Status status) {
        this.lastStatus = status;
    }

    @Override
    public Status getLastStatus() {
        return this.lastStatus;
    }
}
