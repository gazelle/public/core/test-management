package net.ihe.gazelle.tm.messages;

import net.ihe.gazelle.ssov7.gum.client.application.User;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.ssov7.gum.client.interlay.client.SSOGumUserClient;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.messages.model.Message;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class NotificationServiceTest {


    @Test
    public void createMessageTest() {
        UserService userService = new SSOGumUserClient();

        TestInstanceStatusMessageSource testInstanceStatusMessageSource = new TestInstanceStatusMessageSource(userService);

        TestInstance testInstance = new TestInstance();
        testInstance.setId(1);

        List<User> concernedUsers = new ArrayList<>();
        User concernedUser = new User();
        concernedUser.setId("username2");
        concernedUsers.add(concernedUser);

        Message message = NotificationService.createMessage(testInstanceStatusMessageSource, testInstance, "username", concernedUsers,
                "completed", "gazelle.tm.testing.status.completed", "gazelle.tm.testing.status.running", "2");
        Assert.assertNotNull(message);
        Assert.assertEquals("username", message.getAuthor());
        Assert.assertEquals(concernedUsers.size(), message.getUsers().size());
        Assert.assertEquals("gazelle.message.testinstance.status", message.getType());
        Assert.assertEquals("testInstance.seam?id=1", message.getLink());
    }

}
