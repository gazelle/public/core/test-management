package net.ihe.gazelle.tm.gazelletest.action;

import net.ihe.gazelle.tm.gazelletest.dao.TestInstanceDataDAO;
import net.ihe.gazelle.tm.gazelletest.dao.TestInstanceTokenDAO;
import net.ihe.gazelle.tm.gazelletest.model.instance.*;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.concurrent.TimeUnit;


@RunWith(PowerMockRunner.class)
@PrepareForTest(DataType.class)
public class TestInstanceFilesServiceTestForLogsTest {

    private static final String TOKEN_VALUE = "fb6a3a0c-7390-41c8-b11a-1ba796097d77";
    private static final String USER_NAME = "toto";
    private static final Integer TEST_INSTANCE_ID = 1234;

    @Mock
    private TestInstanceDataDAO testInstanceDataDAOMock;

    @Mock
    private TestInstanceTokenDAO testInstanceTokenDAOMock;

    @Mock
    private TestInstance testInstanceMock;
    @Mock
    private TestingSession testingSessionMock ;

    private String FILE_NAME_REPORT = "test-report-" + TOKEN_VALUE + ".xml";
    private String FILE_NAME_Logs = "test-logs-" + TOKEN_VALUE + ".zip";

    @Mock
    private TestStepsData testStepsData;
    private Timestamp creationDateToken;
    @Mock
    private TestInstanceTokenService testInstanceTokenService;

    private TestInstanceFilesService testInstanceFilesService;
    @Mock
    private TestInstanceToken testInstanceToken = new TestInstanceToken(TOKEN_VALUE, USER_NAME, TEST_INSTANCE_ID,
            new Timestamp(System.currentTimeMillis() + TimeUnit.HOURS.toMillis(1)));
    private byte[] file = new byte[5];
    private byte[] emptyFile = new byte[0];

    @Before
    public void setUp() throws Exception {

        testInstanceToken = Mockito.mock((TestInstanceToken.class));
        testInstanceMock = Mockito.mock(TestInstance.class);
        testingSessionMock = Mockito.mock(TestingSession.class);
        testInstanceDataDAOMock = Mockito.mock(TestInstanceDataDAO.class);
        testInstanceTokenDAOMock = Mockito.mock(TestInstanceTokenDAO.class);
        testInstanceTokenService = Mockito.mock(TestInstanceTokenService.class);
        testStepsData = Mockito.mock(TestStepsData.class);

        creationDateToken = new Timestamp(System.currentTimeMillis());

        testInstanceFilesService = new TestInstanceFilesService();
        testInstanceFilesService.setTestInstanceDataDAO(testInstanceDataDAOMock);
        testInstanceFilesService.setTestInstanceTokenService(testInstanceTokenService);

        Mockito.when(testInstanceTokenService.getTokenByValue(TOKEN_VALUE)).thenReturn(testInstanceToken);
        Mockito.when(testInstanceDataDAOMock.instantiateTestStepDataFile(USER_NAME, FILE_NAME_Logs)).thenReturn(testStepsData);

        Mockito.when(testInstanceDataDAOMock.getTestInstance(TEST_INSTANCE_ID)).thenReturn(testInstanceMock);

        Mockito.doNothing().when(testInstanceDataDAOMock).saveFileInTestStepsData(1, file);
        Mockito.when(testInstanceTokenService.isTokenValid(testInstanceToken)).thenReturn(true);

        Mockito.doNothing().when(testInstanceDataDAOMock).saveFileInTestStepsData(1, file);

/**
 * Mock on testInstanceToken
 */
        Mockito.doReturn(USER_NAME).when(testInstanceToken).getUserId();
        Mockito.doReturn(TOKEN_VALUE).when(testInstanceToken).getValue();
        Mockito.doReturn(creationDateToken).when(testInstanceToken).getExpirationDateTime();
        Mockito.doReturn(TEST_INSTANCE_ID).when(testInstanceToken).getTestStepsInstanceId();
/**
 * Mock on TesStepData
 */
        Mockito.doReturn(1).when(testStepsData).getId();
        Mockito.doReturn("Test").when(testStepsData).getValue();
        Mockito.doReturn(new DataType(1, "file", "file")).when(testStepsData).getDataType();

        Mockito.doReturn(testStepsData).when(testInstanceDataDAOMock).instantiateTestStepDataFile(USER_NAME, FILE_NAME_Logs);
        Mockito.doReturn(testStepsData).when(testInstanceDataDAOMock).createTestStepData(testStepsData);

    }


    @Test
    public void saveTestLogsInTestInstance() {
        try {
            PowerMockito.mockStatic(DataType.class);
            BDDMockito.given(DataType.getDataTypeByKeyword(DataType.FILE_DT)).willReturn(new DataType(1, "file", "file"));
            Mockito.doReturn(Status.STARTED).when(testInstanceMock).getLastStatus();
            Mockito.doReturn(testingSessionMock).when(testInstanceMock).getTestingSession();
            Mockito.doReturn(false).when(testingSessionMock).getSessionClosed();
            Mockito.doReturn(1).when(testInstanceMock).getId();
            Mockito.when(testInstanceTokenService.isTokenValid(testInstanceToken)).thenReturn(true);
            testInstanceFilesService.saveTestLogsInTestInstance(TOKEN_VALUE, file);
        } catch (Exception e) {
            Assert.fail();
        }
    }

    @Test(expected = TestInstanceNotFoundException.class)
    public void saveTestLogsWithNoTestInstanceFound() throws InvalidTokenException, EmptyFileException, TestInstanceClosedException, TestInstanceNotFoundException, IOException, TokenNotFoundException {

        Mockito.doThrow(new TestInstanceNotFoundException("No test instance has been found for id : " + 1234)).when(testInstanceDataDAOMock).updateTestInstanceAndTestStepDataForLogs(testStepsData,1234,USER_NAME);
        PowerMockito.mockStatic(DataType.class);
        BDDMockito.given(DataType.getDataTypeByKeyword(DataType.FILE_DT)).willReturn(new DataType(1, "file", "file"));
        Mockito.doReturn(Status.STARTED).when(testInstanceMock).getLastStatus();
        Mockito.doReturn(testingSessionMock).when(testInstanceMock).getTestingSession();
        Mockito.doReturn(false).when(testingSessionMock).getSessionClosed();
        Mockito.doReturn(1234).when(testInstanceMock).getId();
        Mockito.when(testInstanceTokenService.isTokenValid(testInstanceToken)).thenReturn(true);
        testInstanceFilesService.saveTestLogsInTestInstance(TOKEN_VALUE, file);
    }

    /**
     * Test verify the exception for a expired token, it's expected to throws an InvalidTokenException
     *
     * @throws TokenNotFoundException
     * @throws InvalidTokenException
     * @throws EmptyFileException
     * @throws TestInstanceClosedException
     * @throws TestInstanceNotFoundException
     * @throws IOException
     */
    @Test(expected = InvalidTokenException.class)
    public void saveLogsWithExpiredToken() throws TokenNotFoundException, InvalidTokenException, EmptyFileException, TestInstanceClosedException,
            TestInstanceNotFoundException, IOException {
        Mockito.when(testInstanceTokenService.isTokenValid(testInstanceToken)).thenReturn(false);
        testInstanceFilesService.saveTestLogsInTestInstance(TOKEN_VALUE, file);
    }

    /**
     * Test verify the exception for a expired token, it's expected to throws an InvalidTokenException
     *
     * @throws TokenNotFoundException
     * @throws InvalidTokenException
     * @throws EmptyFileException
     * @throws TestInstanceClosedException
     * @throws TestInstanceNotFoundException
     * @throws IOException
     */
    @Test(expected = InvalidTokenException.class)
    public void saveReportWithNoTokenFound() throws TokenNotFoundException, InvalidTokenException, EmptyFileException, TestInstanceClosedException,
            TestInstanceNotFoundException, IOException {
        Mockito.when(testInstanceTokenService.getTokenByValue(TOKEN_VALUE)).thenThrow(new TokenNotFoundException("No token found using the token value provided") );
        testInstanceFilesService.saveTestLogsInTestInstance(TOKEN_VALUE, file);
    }
    /**
     * Test verify the exception TestInstance Closed , it's expected to throws an TestInstanceClosedException
     *
     * @throws TokenNotFoundException
     * @throws InvalidTokenException
     * @throws EmptyFileException
     * @throws TestInstanceClosedException
     * @throws TestInstanceNotFoundException
     * @throws IOException
     */
    @Test(expected = TestInstanceClosedException.class)
    public void saveLogsWithFinishedTestInstance() throws TestInstanceNotFoundException, InvalidTokenException, EmptyFileException,
            TestInstanceClosedException, IOException, TokenNotFoundException {
        Mockito.doReturn(testingSessionMock).when(testInstanceMock).getTestingSession();
        Mockito.doReturn(false).when(testingSessionMock).getSessionClosed();
        Mockito.doReturn(Status.FAILED).when(testInstanceMock).getLastStatus();
        Mockito.when(testInstanceTokenService.isTokenValid(testInstanceToken)).thenReturn(true);
        testInstanceFilesService.saveTestLogsInTestInstance(TOKEN_VALUE, file);

    }

    /**
     * Test verify the exception with null file as parameter , it's expected to throws an EmptyFileException
     *
     * @throws TokenNotFoundException
     * @throws InvalidTokenException
     * @throws EmptyFileException
     * @throws TestInstanceClosedException
     * @throws TestInstanceNotFoundException
     * @throws IOException
     */
    @Test(expected = EmptyFileException.class)
    public void saveLogsWithNullFile() throws InvalidTokenException, EmptyFileException, TestInstanceClosedException, TestInstanceNotFoundException
            , IOException {
        testInstanceFilesService.saveTestLogsInTestInstance(TOKEN_VALUE, null);
    }


    /**
     * Test verify the exception with null file as parameter , it's expected to throws an EmptyFileException
     *
     * @throws TokenNotFoundException
     * @throws InvalidTokenException
     * @throws EmptyFileException
     * @throws TestInstanceClosedException
     * @throws TestInstanceNotFoundException
     * @throws IOException
     */
    @Test(expected = EmptyFileException.class)
    public void saveLogsWithEmptyFile() throws InvalidTokenException, EmptyFileException, TestInstanceClosedException,
            TestInstanceNotFoundException, IOException {
        testInstanceFilesService.saveTestLogsInTestInstance(TOKEN_VALUE, emptyFile);

    }

    /**
     * Test verify the exception with empty Token as parameter , it's expected to throws an EmptyFileException
     *
     * @throws TokenNotFoundException
     * @throws InvalidTokenException
     * @throws EmptyFileException
     * @throws TestInstanceClosedException
     * @throws TestInstanceNotFoundException
     * @throws IOException
     */
    @Test(expected = InvalidTokenException.class)
    public void saveLogsWithNullToken() throws InvalidTokenException, EmptyFileException, TestInstanceClosedException,
            TestInstanceNotFoundException, IOException {
        testInstanceFilesService.saveTestLogsInTestInstance("", file);
    }

    /**
     * Test verify the exception with null Token as parameter , it's expected to throws an EmptyFileException
     *
     * @throws TokenNotFoundException
     * @throws InvalidTokenException
     * @throws EmptyFileException
     * @throws TestInstanceClosedException
     * @throws TestInstanceNotFoundException
     * @throws IOException
     */
    @Test(expected = InvalidTokenException.class)
    public void saveLogsWithEmptyToken() throws InvalidTokenException, EmptyFileException, TestInstanceClosedException,
            TestInstanceNotFoundException, IOException {
        testInstanceFilesService.saveTestLogsInTestInstance(null, file);
    }


}