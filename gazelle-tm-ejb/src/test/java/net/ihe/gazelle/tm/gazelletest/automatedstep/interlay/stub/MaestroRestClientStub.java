package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.stub;

import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script.MaestroRequest;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.execution.client.MaestroRestClient;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.execution.client.StepRunClientException;
import net.ihe.gazelle.tm.gazelletest.model.instance.Status;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestStepsInstance;

public class MaestroRestClientStub implements MaestroRestClient {

    @Override
    public void runAutomatedTestStep(MaestroRequest script, TestStepsInstance stepInstance) throws StepRunClientException {
        if (Status.ABORTED.equals(stepInstance.getStatus())) {
            throw new StepRunClientException();
        }
    }
}
