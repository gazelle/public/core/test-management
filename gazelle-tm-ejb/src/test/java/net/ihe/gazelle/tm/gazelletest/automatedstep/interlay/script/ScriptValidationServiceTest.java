package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.script;

import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script.Script;
import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script.ScriptParsingException;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.script.validation.ScriptValidationService;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.script.validation.ScriptValidationServiceImpl;
import org.junit.Test;

import java.io.*;
import java.nio.charset.StandardCharsets;

import static org.testng.AssertJUnit.*;

public class ScriptValidationServiceTest {

    private final ScriptValidationService service = new ScriptValidationServiceImpl();

    @Test
    public void testValidationOK() {
        String result = service.validate(readJsonScript("script_ok"));
        assertNull(result);
    }

    @Test
    public void testValidationKO() {
        String result = service.validate(readJsonScript("script_ko"));
        assertNotNull(result);
        assertTrue(result.contains("#: required key [name] not found"));
        assertTrue(result.contains("#/inputs/0/type: expected type: String, found: Integer"));
    }

    @Test
    public void testParseOK() {
        Script script = service.parse(readJsonScript("script_ok"));
        assertEquals("TEST_OOTS_XVAL_INTEGRATION", script.getName());
    }

    @Test
    public void testParseKO() {
        try {
            service.parse(readJsonScript("script_ko"));
        } catch (ScriptParsingException e) {
            assertEquals(e.getMessage(), "Unable to parse uploaded script");
            assertTrue(e.getCause().getMessage().contains("badKey"));
        }
    }

    private String readJsonScript(String fileName) {
        InputStream inputStream = getClass().getResourceAsStream("/automation/" + fileName + ".json");
        StringBuilder textBuilder = new StringBuilder();
        try (Reader reader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))) {
            int c = 0;
            while ((c = reader.read()) != -1) {
                textBuilder.append((char) c);
            }
            return textBuilder.toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
