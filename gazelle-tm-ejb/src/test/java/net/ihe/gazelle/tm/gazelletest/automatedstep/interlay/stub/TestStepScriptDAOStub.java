package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.stub;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script.Script;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.script.dao.TestStepScriptDAO;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.script.dto.ScriptDTO;
import net.ihe.gazelle.tm.gazelletest.domain.TestStepDomain;

import java.util.HashMap;
import java.util.Map;

public class TestStepScriptDAOStub implements TestStepScriptDAO {

    private final Map<String, String> testDao;

    public TestStepScriptDAOStub() {
        testDao = new HashMap<>();
    }

    @Override
    public void saveScript(TestStepDomain testStep, Script script) {
        String fileName = testStep.getId() + testStep.getTestScriptID() + ".json";
        try {
            ObjectMapper mapper = new ObjectMapper();
            testDao.put(fileName, mapper.writeValueAsString(new ScriptDTO(script)));
        } catch (JsonProcessingException e) {
            // empty
        }
    }

    @Override
    public String readScript(Integer stepId, String scriptName) {
        String fileName = stepId + scriptName + ".json";
        return testDao.get(fileName);
    }

    @Override
    public void deleteScript(TestStepDomain testStep) {
        String fileName = testStep.getId() + testStep.getTestScriptID() + ".json";
        testDao.remove(fileName);
    }

    public Map<String, String> getTestDao() {
        return testDao;
    }
}
