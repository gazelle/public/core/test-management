package net.ihe.gazelle.tm.gazelletest.action;

import net.ihe.gazelle.tm.gazelletest.dao.TestInstanceDataDAO;
import net.ihe.gazelle.tm.gazelletest.model.instance.DataType;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstanceToken;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestStepsData;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.concurrent.TimeUnit;


@RunWith(PowerMockRunner.class)
@PrepareForTest(DataType.class)
public class TestInstanceFilesServiceTestForReportsTest {

    private static final String TOKEN_VALUE = "fb6a3a0c-7390-41c8-b11a-1ba796097d77";
    private static final String USER_NAME = "toto";
    private static final Integer TEST_STEP_INSTANCE_ID = 1234;

    @Mock
    private TestInstanceDataDAO testInstanceDataDAOMock;


    @Mock
    private TestInstance testInstanceMock;

    @Mock
    private TestingSession testingSessionMock;

    private String FILE_NAME_REPORT = "test-report-" + TOKEN_VALUE + ".xml";

    @Mock
    private TestStepsData testStepsData;
    private Timestamp creationDateToken;
    @Mock
    private TestInstanceTokenService testInstanceTokenService;

    private TestInstanceFilesService testInstanceFilesService;
    @Mock
    private TestInstanceToken testInstanceToken = new TestInstanceToken(TOKEN_VALUE, USER_NAME, TEST_STEP_INSTANCE_ID, new Timestamp(System.currentTimeMillis() + TimeUnit.HOURS.toMillis(1)));
    @Mock
    private byte[] file = new byte[8];
    private byte[] emptyFile = new byte[0];

    String badReport = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<testReport xmlns=\"http://testreport.gazelle.ihe.net/\" uuid=\"42fddba0\" dateTime=\"2021-02-05\">\n" +
            "    <testService>\n" +
            "        <serviceIdentification version=\"1.0.0\">Continua Test Tool</serviceIdentification>\n" +
            "        <disclaimer>Disclaimer : this is a test disclaimer</disclaimer>\n" +
            "    </testService>\n" +
            "    <systemUnderTest>\n" +
            "        <systemIdentification version=\"2.4\">name of the SUT</systemIdentification>\n" +
            "        <macAddress>5E:XX:5E:A2:AF:1A</macAddress>\n" +
            "        <ipAddress>80.105.1247.34</ipAddress>\n" +
            "    </systemUnderTest>\n" +
            "    <resultOverview>\n" +
            "        <testSuiteName>Device Test Suite</testSuiteName>\n" +
            "        <status>BLOCKED</status>\n" +
            "        <numberOfTests total=\"-5\" run=\"3\" passed=\"3\" failed=\"0\" skipped=\"0\" undefined=\"0\"/>\n" +
            "        <urlToTestSuiteResult>http://localhost/invalidURI#f%  error</urlToTestSuiteResult>\n" +
            "    </resultOverview>\n" +
            "</testReport>";

    private String goodReport = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<testReport xmlns=\"http://testreport.gazelle.ihe.net/\" uuid=\"b569b553-7236-452a-b1e1-357a93c94e64\" " +
            "dateTime=\"2021-02-14T19:30:00\">\n" +
            "    <testService>\n" +
            "        <serviceIdentification version=\"2.3.1\">Another test service</serviceIdentification>\n" +
            "        <disclaimer>Disclaimer : this is a test disclaimer</disclaimer>\n" +
            "    </testService>\n" +
            "    <systemUnderTest>\n" +
            "        <systemIdentification version=\"unknown\">SUTName</systemIdentification>\n" +
            "    </systemUnderTest>\n" +
            "    <resultOverview>\n" +
            "        <testSuiteName>Device test suite</testSuiteName>\n" +
            "        <status>PASSED</status>\n" +
            "        <numberOfTests total=\"2\" run=\"1\" passed=\"1\" failed=\"0\" skipped=\"1\" undefined=\"0\"/>\n" +
            "    </resultOverview>\n" +
            "</testReport>";

    @Before
    public void setUp() throws Exception {

        testInstanceToken = Mockito.mock((TestInstanceToken.class));
        testInstanceMock = Mockito.mock(TestInstance.class);
        testingSessionMock = Mockito.mock(TestingSession.class);
        testInstanceDataDAOMock = Mockito.mock(TestInstanceDataDAO.class);

        testInstanceTokenService = Mockito.mock(TestInstanceTokenService.class);
        testStepsData = Mockito.mock(TestStepsData.class);

        creationDateToken = new Timestamp(System.currentTimeMillis());

        testInstanceFilesService = new TestInstanceFilesService();
        testInstanceFilesService.setTestInstanceDataDAO(testInstanceDataDAOMock);
        testInstanceFilesService.setTestInstanceTokenService(testInstanceTokenService);

        Mockito.when(testInstanceTokenService.getTokenByValue(TOKEN_VALUE)).thenReturn(testInstanceToken);
        Mockito.when(testInstanceDataDAOMock.instantiateTestStepDataFile(USER_NAME, FILE_NAME_REPORT)).thenReturn(testStepsData);

        Mockito.when(testInstanceDataDAOMock.getTestInstance(TEST_STEP_INSTANCE_ID)).thenReturn(testInstanceMock);

        Mockito.doNothing().when(testInstanceDataDAOMock).saveFileInTestStepsData(1, file);
        Mockito.when(testInstanceTokenService.isTokenValid(testInstanceToken)).thenReturn(true);

        Mockito.doNothing().when(testInstanceDataDAOMock).saveFileInTestStepsData(1, file);


/**
 * Mock on testInstanceToken
 */
        Mockito.doReturn(USER_NAME).when(testInstanceToken).getUserId();
        Mockito.doReturn(TOKEN_VALUE).when(testInstanceToken).getValue();
        Mockito.doReturn(creationDateToken).when(testInstanceToken).getExpirationDateTime();
        Mockito.doReturn(1).when(testInstanceToken).getTestStepsInstanceId();
/**
 * Mock on TesStepData
 */
        Mockito.doReturn(1).when(testStepsData).getId();
        Mockito.doReturn("Test").when(testStepsData).getValue();
        Mockito.doReturn(new DataType(1, "file", "file")).when(testStepsData).getDataType();

        Mockito.doReturn(testStepsData).when(testInstanceDataDAOMock).instantiateTestStepDataFile(USER_NAME, FILE_NAME_REPORT);
        Mockito.doReturn(testStepsData).when(testInstanceDataDAOMock).createTestStepData(testStepsData);


    }

    /**
     * Test verify the exception for a expired token, it's expected to throws an InvalidTokenException
     *
     * @throws TokenNotFoundException
     * @throws InvalidTokenException
     * @throws EmptyFileException
     * @throws TestInstanceClosedException
     * @throws TestInstanceNotFoundException
     * @throws IOException
     */
    @Test(expected = InvalidTokenException.class)
    public void saveReportWithExpiredToken() throws TokenNotFoundException, InvalidTokenException, EmptyFileException, TestInstanceClosedException,
            TestInstanceNotFoundException, IOException, InvalidTestReportException {
        Mockito.when(testInstanceTokenService.isTokenValid(testInstanceToken)).thenReturn(false);
        testInstanceFilesService.saveTestReportInTestInstance(TOKEN_VALUE, file);
    }

    /**
     * Test verify the exception for a expired token, it's expected to throws an InvalidTokenException
     *
     * @throws TokenNotFoundException
     * @throws InvalidTokenException
     * @throws EmptyFileException
     * @throws TestInstanceClosedException
     * @throws TestInstanceNotFoundException
     * @throws IOException
     */
    @Test(expected = InvalidTokenException.class)
    public void saveReportWithNoTokenFound() throws TokenNotFoundException, InvalidTokenException, EmptyFileException, TestInstanceClosedException,
            TestInstanceNotFoundException, IOException, InvalidTestReportException {
        file = goodReport.getBytes(StandardCharsets.UTF_8);
        Mockito.when(testInstanceTokenService.getTokenByValue(TOKEN_VALUE)).thenThrow(new TokenNotFoundException("No token found using the " +
                "token value provided"));
        testInstanceFilesService.saveTestReportInTestInstance(TOKEN_VALUE, file);
    }

    /**
     * Test verify the exception with null file as parameter , it's expected to throws an EmptyFileException
     *
     * @throws TokenNotFoundException
     * @throws InvalidTokenException
     * @throws EmptyFileException
     * @throws TestInstanceClosedException
     * @throws TestInstanceNotFoundException
     * @throws IOException
     */
    @Test(expected = EmptyFileException.class)
    public void saveReportWithNullFile() throws InvalidTokenException, EmptyFileException, TestInstanceClosedException, TestInstanceNotFoundException
            , IOException, InvalidTestReportException {
        testInstanceFilesService.saveTestReportInTestInstance(TOKEN_VALUE, null);
    }

    /**
     * Test verify the exception with null file as parameter , it's expected to throws an EmptyFileException
     *
     * @throws TokenNotFoundException
     * @throws InvalidTokenException
     * @throws EmptyFileException
     * @throws TestInstanceClosedException
     * @throws TestInstanceNotFoundException
     * @throws IOException
     */
    @Test(expected = EmptyFileException.class)
    public void saveReportWithEmptyFile() throws InvalidTokenException, EmptyFileException, TestInstanceClosedException,
            TestInstanceNotFoundException, IOException, InvalidTestReportException {
        testInstanceFilesService.saveTestReportInTestInstance(TOKEN_VALUE, emptyFile);

    }

    /**
     * Test verify the exception with empty Token as parameter , it's expected to throws an EmptyFileException
     *
     * @throws TokenNotFoundException
     * @throws InvalidTokenException
     * @throws EmptyFileException
     * @throws TestInstanceClosedException
     * @throws TestInstanceNotFoundException
     * @throws IOException
     */
    @Test(expected = InvalidTokenException.class)
    public void saveReportWithNullToken() throws InvalidTokenException, EmptyFileException, TestInstanceClosedException,
            TestInstanceNotFoundException, IOException, InvalidTestReportException {
        testInstanceFilesService.saveTestReportInTestInstance("", file);
    }

    /**
     * Test verify the exception with null Token as parameter , it's expected to throws an EmptyFileException
     *
     * @throws TokenNotFoundException
     * @throws InvalidTokenException
     * @throws EmptyFileException
     * @throws TestInstanceClosedException
     * @throws TestInstanceNotFoundException
     * @throws IOException
     */
    @Test(expected = InvalidTokenException.class)
    public void saveReportWithEmptyToken() throws InvalidTokenException, EmptyFileException, TestInstanceClosedException,
            TestInstanceNotFoundException, IOException {
        testInstanceFilesService.saveTestLogsInTestInstance(null, file);
    }


}