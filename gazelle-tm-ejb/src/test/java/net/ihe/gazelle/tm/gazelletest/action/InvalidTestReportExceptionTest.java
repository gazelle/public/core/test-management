package net.ihe.gazelle.tm.gazelletest.action;

import org.junit.Test;

import static org.junit.Assert.*;

public class InvalidTestReportExceptionTest {
    /**
     * Dummy Test for coverage
     * @throws InvalidTestReportException testException
     */
    @Test(expected = InvalidTestReportException.class)
    public void generateException() throws InvalidTestReportException {
        throw new InvalidTestReportException("Test");
    }
    /**
     * Dummy Test for coverage
     * @throws InvalidTestReportException testException
     */
    @Test(expected = InvalidTestReportException.class)
    public void generateException1() throws InvalidTestReportException {
        throw new InvalidTestReportException("Test",new IllegalArgumentException("test"));
    }

    /**
     * Dummy Test for coverage
     *
     * @throws InvalidTestReportException testException
     */
    @Test(expected = InvalidTestReportException.class)
    public void generateException2() throws InvalidTestReportException {
        throw new InvalidTestReportException( new IllegalArgumentException("test"));
    }

    /**
     * Dummy Test for coverage
     *
     * @throws InvalidTestReportException testException
     */
    @Test(expected = InvalidTestReportException.class)
    public void generateException3() throws InvalidTestReportException {
        throw new InvalidTestReportException( );
    }


}