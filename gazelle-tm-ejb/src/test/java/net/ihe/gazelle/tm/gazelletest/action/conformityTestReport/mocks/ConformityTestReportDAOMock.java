package net.ihe.gazelle.tm.gazelletest.action.conformityTestReport.mocks;

import net.ihe.gazelle.tf.model.factories.ActorIntegrationProfileOptionFactory;
import net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.dao.ConformityTestReportDAO;
import net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.model.TestInstanceReport;
import net.ihe.gazelle.tm.gazelletest.bean.SAPResultDetailItem;
import net.ihe.gazelle.tm.gazelletest.bean.SAPResultDetailTestRoleItem;
import net.ihe.gazelle.tm.gazelletest.model.definition.MetaTest;
import net.ihe.gazelle.tm.gazelletest.model.definition.factories.TestRolesFactory;
import net.ihe.gazelle.tm.gazelletest.model.instance.Status;
import net.ihe.gazelle.tm.gazelletest.model.instance.SystemAIPOResultForATestingSession;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.systems.model.System;
import net.ihe.gazelle.tm.systems.model.SystemActorProfiles;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.users.model.Institution;

import java.util.ArrayList;
import java.util.List;

public class ConformityTestReportDAOMock implements ConformityTestReportDAO {

    @Override
    public List<SAPResultDetailItem> computeDetail(SystemAIPOResultForATestingSession systemAIPOResult) {
        ObjectMocks mock = new ObjectMocks(1);
        List<SAPResultDetailItem> list = new ArrayList<>();
        // Create a meta test
        MetaTest metaTest = new MetaTest();
        metaTest.setKeyword("MetaTestDescription");
        metaTest.setKeyword("MetaTestKeyword");
        SAPResultDetailMetaTestItemMock sapResultDetailItem = new SAPResultDetailMetaTestItemMock(systemAIPOResult.getTestSession(), metaTest);
        sapResultDetailItem.setTestRolesList(mock.createTestRolesWithMandatoryFields());
        list.add(sapResultDetailItem);
        SAPResultDetailTestRoleItem sapResultDetailItem1 =
                new SAPResultDetailTestRoleItemMock(TestRolesFactory.createTestRolesWithMandatoryFields().get(0),
                        systemAIPOResult);
        sapResultDetailItem1.tryAddTestInstance(mock.createTestInstance(Status.COMPLETED));
        list.add(sapResultDetailItem1);
        SAPResultDetailItem sapResultDetailItem2 = new SAPResultDetailTestRoleItem(TestRolesFactory.createTestRolesWithMandatoryFields().get(0),
                systemAIPOResult);
        list.add(sapResultDetailItem2);

        SAPResultDetailItem sapResultDetailItem3 = new SAPResultDetailTestRoleItem(TestRolesFactory.createTestRolesWithMandatoryFields().get(0),
                systemAIPOResult);
        list.add(sapResultDetailItem3);
        SAPResultDetailItem sapResultDetailItem4 = new SAPResultDetailTestRoleItem(TestRolesFactory.createTestRolesWithMandatoryFields().get(0),
                systemAIPOResult);
        list.add(sapResultDetailItem4);
        return list;
    }

    @Override
    public List<SAPResultDetailItem> computeDetail(SystemAIPOResultForATestingSession systemAIPOResult, boolean isInteroperability) {
        return computeDetail(systemAIPOResult);
    }

    @Override
    public List<TestInstanceReport> generateTestInstanceItem(List<TestInstance> tiList) {
        List<TestInstanceReport> testInstanceReportList = new ArrayList<>();
        for (TestInstance testInstance : tiList) {
            TestInstanceReport testInstanceReport = new TestInstanceReport();
            if (testInstance.getMonitorInSession() != null) {
                testInstanceReport.setMonitorName(testInstance.getMonitorInSession().getUserId());
            } else {
                testInstanceReport.setMonitorName("undefined");
            }
            if (testInstance.getLastStatus().toString() != null) {
                testInstanceReport.setResult(testInstance.getLastStatus().toString());
            }
            testInstanceReport.setTestInstanceId(testInstance.getId().toString());
            testInstanceReport.setPermalink("https://www.gazelle.ihe-europe.net" + "testInstance.seam?id=" + testInstance.getId().toString());
            testInstanceReport.setResult(testInstance.getLastStatus().toString());
            testInstanceReport.setStartDate(testInstance.getStartDate());
            testInstanceReport.setTestInstanceId(testInstance.getId().toString());
            testInstanceReportList.add(testInstanceReport);
        }
        return testInstanceReportList;
    }

    @Override
    public List<SystemAIPOResultForATestingSession> getResultOfSystem(System system, TestingSession testingSession) {
        List<SystemAIPOResultForATestingSession> list = new ArrayList<>();
        SystemAIPOResultForATestingSession systemAIPO = new SystemAIPOResultForATestingSession();
        SystemActorProfiles systemActorProfile = new SystemActorProfiles();
        systemActorProfile.setSystem(system);
        systemActorProfile.setActorIntegrationProfileOption(ActorIntegrationProfileOptionFactory.createActorIntegrationProfileOptionWithIntegrationProfileOption());
        systemAIPO.setSystemActorProfile(systemActorProfile);
        list.add(systemAIPO);
        systemAIPO = new SystemAIPOResultForATestingSession();
        systemActorProfile = new SystemActorProfiles();
        systemActorProfile.setSystem(system);
        systemActorProfile.setActorIntegrationProfileOption(ActorIntegrationProfileOptionFactory.createActorIntegrationProfileOptionWithIntegrationProfileOption());
        systemAIPO.setSystemActorProfile(systemActorProfile);
        list.add(systemAIPO);
        return list;
    }

    @Override
    public List<Institution> getInstitutionQueryList() {
        List<Institution> institutions = new ArrayList<>();
        ObjectMocks mock = new ObjectMocks(1);
        Institution institution = mock.createInstitutionWithMandatoryFields();
        institutions.add(institution);
        return institutions;
    }

    @Override
    public List<Institution> getInstituionList() {
        List<Institution> institutions = new ArrayList<>();
        ObjectMocks mock = new ObjectMocks(1);
        Institution institution = mock.createInstitutionWithMandatoryFields();
        institutions.add(institution);
        return institutions;
    }

    @Override
    public Institution findInstituion(String name) {
        ObjectMocks mock = new ObjectMocks(1);
        return mock.createInstitutionWithMandatoryFieldsByName(name);
    }

    @Override
    public Institution findInstitutionById(Integer id) {
        ObjectMocks mock = new ObjectMocks(1);
        return mock.createInstitutionWithMandatoryFields();
    }

    @Override
    public List<SystemInSession> getSystemsInSessionForSession(TestingSession session) {
        ObjectMocks mock = new ObjectMocks(1);
        SystemInSession session1 = mock.createSystemIneSessionWithMandatoryFields(session);
        SystemInSession session2 = mock.createSystemIneSessionWithMandatoryFields(session);
        List<SystemInSession> sessions = new ArrayList<>();
        sessions.add(session1);
        sessions.add(session2);
        return sessions;
    }

    @Override
    public List<SystemInSession> getSystemsInSessionForCompanyNameForSession(String name, TestingSession session) {
        ObjectMocks mock = new ObjectMocks(1);
        SystemInSession session1 = mock.createSystemIneSessionWithMandatoryFields(name, session);
        SystemInSession session2 = mock.createSystemIneSessionWithMandatoryFields(name, session);
        List<SystemInSession> sessions = new ArrayList<>();
        sessions.add(session1);
        sessions.add(session2);
        return sessions;
    }

    @Override
    public SystemInSession getSystemInSessionForSession(System inSystem, TestingSession inTestingSession) {
        ObjectMocks mock = new ObjectMocks(1);
        return mock.createSystemIneSessionWithMandatoryFields();
    }

    @Override
    public String getCompanyNameOfInstitutionOfSelectedSystemInSession(SystemInSession sis) {
        return "Kereval1";
    }

    @Override
    public String getCompanyNameOfInstitutionOfSelectedSystem(System system) {
        return "Kereval1";
    }

    @Override
    public List<TestingSession> allSessions() {
        ObjectMocks mock = new ObjectMocks(1);
        List<TestingSession> sessions = new ArrayList<>();
        sessions.add(mock.provideTestingSessionWithMandatoryFields());
        return sessions;
    }

    @Override
    public List<TestingSession> getTestingSessionList() {
        ObjectMocks mock = new ObjectMocks(1);
        List<TestingSession> sessions = new ArrayList<>();
        sessions.add(mock.provideTestingSessionWithMandatoryFields());
        return sessions;
    }

    @Override
    public List<TestingSession> allSessionsWhereCompanyWasHere(String name) {
        ObjectMocks mock = new ObjectMocks(1);
        List<TestingSession> sessions = new ArrayList<>();
        sessions.add(mock.provideTestingSessionWithMandatoryFields(name));
        return sessions;
    }

    @Override
    public TestingSession getTestingSessionById(Integer id) {
        ObjectMocks mock = new ObjectMocks(1);
        return mock.provideTestingSessionWithMandatoryFields(id);
    }
}
