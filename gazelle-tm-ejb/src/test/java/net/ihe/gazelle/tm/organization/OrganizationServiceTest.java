package net.ihe.gazelle.tm.organization;

import net.ihe.gazelle.ssov7.gum.client.interlay.ws.OrganizationResource;
import net.ihe.gazelle.tm.organization.dao.OrganizationDAOImplMock;
import net.ihe.gazelle.tm.organization.exception.OrganizationCreationException;
import net.ihe.gazelle.users.model.Institution;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.PersistenceException;
import java.util.List;
import java.util.NoSuchElementException;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static junitx.framework.Assert.assertFalse;
import static junitx.framework.Assert.assertNotEquals;

public class OrganizationServiceTest {

    OrganizationService service;

    @Before
    public void init() {
        service = new OrganizationServiceImpl(new OrganizationDAOImplMock());
    }

    @Test
    public void testSearchForOrganization() {
        List<Institution> institutions = service.searchForOrganizations(null, null,null,null);
        assertEquals(2, institutions.size());
        Institution institution = institutions.get(0);
        assertEquals("Ker1", institution.getKeyword());
        assertEquals("Kereval1", institution.getName());
        assertEquals("http://kereval.com", institution.getUrl());
    }

    @Test
    public void testSearchForOrganizationWithName() {
        List<Institution> institutions = service.searchForOrganizations("Kereval1", null, null, null);
        assertEquals(1, institutions.size());
        Institution institution = institutions.get(0);
        assertEquals("Ker1", institution.getKeyword());
        assertEquals("Kereval1", institution.getName());
        assertEquals("http://kereval.com", institution.getUrl());
    }

    @Test
    public void testSearchForDelegatedOrganization() {
        List<Institution> institutions = service.searchForOrganizations(null, null,null,false);
        assertEquals(1, institutions.size());
        Institution institution = institutions.get(0);
        assertEquals("Ker1", institution.getKeyword());
        assertEquals("Kereval1", institution.getName());
        assertEquals("http://kereval.com", institution.getUrl());

        institutions = service.searchForOrganizations(null,null,null,true);
        assertEquals(1, institutions.size());
        institution = institutions.get(0);
        assertEquals("Delegated_Ker2", institution.getKeyword());
        assertEquals("Delegated Kereval2", institution.getName());
        assertEquals("http://kereval.com2", institution.getUrl());

        institutions = service.searchForOrganizations(null,null,"idpId",false);
        assertEquals(0, institutions.size());
        institutions = service.searchForOrganizations(null,null,"test",true);
        assertEquals(0, institutions.size());
        institutions = service.searchForOrganizations(null,"externalId","idpId",null);
        assertEquals(1, institutions.size());
        assertEquals("Delegated_Ker2", institution.getKeyword());
    }

    @Test
    public void okCreateOrganizationTest() {
        OrganizationResource organization = new OrganizationResource("notExists", "name", "https://test.fr");
        service.createOrganization(organization);
        assertTrue(true);
    }

    @Test
    public void koCreateOrganizationTest() {
        // Bad id
        OrganizationResource organization = new OrganizationResource(null, "name", "https://test.fr");
        testExceptionOnOrganizationCreation(organization);

        // Bad name
        organization = new OrganizationResource("id1", null, "test");
        testExceptionOnOrganizationCreation(organization);

        // Bad url
        organization = new OrganizationResource("idtestko", "name", null);
        testExceptionOnOrganizationCreation(organization);

        // Already exists
        organization = new OrganizationResource("exists", "name", "test");
        testExceptionOnOrganizationCreation(organization);
    }

    @Test(expected = NoSuchElementException.class)
    public void getOrganizationByIdNotFound() {
        service.getOrganizationById("notExists");
    }

    @Test
    public void getOrganizationByIdFound() {
        Institution institution = service.getOrganizationById("exists");
        assertEquals("exists", institution.getKeyword());
    }

    @Test
    public void isOrganizationDelegatedReturnFoundTest() {
        Institution institution = new Institution();
        institution.setId(1);
        institution.setKeyword("KER");
        assertTrue(service.isOrganizationDelegated(institution));
    }

    @Test
    public void isOrganizationDelegatedNotFoundTest() {
        Institution institution = new Institution();
        institution.setId(1);
        institution.setKeyword("KOR");
        assertFalse(service.isOrganizationDelegated(institution));
    }

    @Test
    public void createDelegatedOrganizationTestOK() {
        // Bad external Id
        OrganizationResource delegatedOrganization = new OrganizationResource("Test_delegation", "Test delegation", "", null, "Test_IDP");
        testExceptionOnOrganizationCreation(delegatedOrganization);

        // Bad IDP name
        delegatedOrganization = new OrganizationResource("Test_delegation", "Test delegation", "TEST", "externalId", null);
        testExceptionOnOrganizationCreation(delegatedOrganization);

        // Already exists
        delegatedOrganization = new OrganizationResource("exists", "exists", "TEST", "externalId", "Test_IDP");
        testExceptionOnOrganizationCreation(delegatedOrganization);
    }

    @Test(expected = IllegalArgumentException.class)
    public void isOrganizationDelegatedThrowsIllegalArgumentExceptionTest() {
        service.isOrganizationDelegated(null);
    }


    @Test
    public void testUpdateDelegatedOrganizationName() {
        Institution institution = new Institution();
        institution.setName("newName");
        institution.setKeyword("Delegated_Ker2");
        Institution institutionToUpdate = service.getOrganizationById("Delegated_Ker2");
        Institution institutionUpdated = service.updateOrganization("Delegated_Ker2", institution);
        assertEquals(institution.getKeyword(), institutionUpdated.getKeyword());
        assertNotEquals(institutionToUpdate.getName(), institutionUpdated.getName());
        assertEquals(institution.getName(), institutionUpdated.getName());
    }

    @Test
    public void testUpdateDelegatedOrganizationNameWithSpace() {
        Institution institution = new Institution();
        institution.setName("name  ");
        institution.setKeyword("Delegated_Ker2");
        Institution institutionToUpdate = service.getOrganizationById("Delegated_Ker2");
        Institution institutionUpdated = service.updateOrganization("Delegated_Ker2", institution);
        assertEquals(institution.getKeyword(), institutionUpdated.getKeyword());
        assertNotEquals(institutionToUpdate.getName(), institutionUpdated.getName());
        assertEquals(institution.getName(), institutionUpdated.getName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdateDelegatedOrganizationNameNull() {
        service.updateOrganization("Test_delegation", null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdateDelegatedOrganizationIdNull() {
        service.updateOrganization("Test_delegation", null);
    }

    @Test(expected = PersistenceException.class)
    public void testUpdateDelegatedOrganizationNameNotUnique() {
        Institution institution = new Institution();
        institution.setName("duplicateName");
        institution.setKeyword("Delegated_Ker2");
        service.updateOrganization("Test_delegation", institution);
    }

    @Test(expected = NoSuchElementException.class)
    public void testUpdateNotDelegatedOrganizationName() {
        Institution institution = service.getDelegatedOrganizationByKeyword("Test_delegation");
        service.updateOrganization("notDelegated", institution);
    }

    private void testExceptionOnOrganizationCreation(OrganizationResource organization) {
        try {
            service.createOrganization(organization);
        } catch (IllegalArgumentException e) {
            assertEquals("Some of the Organization fields are null.", e.getMessage());
        } catch (OrganizationCreationException e) {
            assertEquals(String.format("The organization with keyword " + organization.getId() + ", already exists.", organization.getId()), e.getMessage());
        }
    }

}
