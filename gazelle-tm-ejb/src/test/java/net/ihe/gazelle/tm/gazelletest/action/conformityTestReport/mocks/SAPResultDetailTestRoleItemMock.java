package net.ihe.gazelle.tm.gazelletest.action.conformityTestReport.mocks;

import net.ihe.gazelle.tm.gazelletest.bean.SAPResultDetailTestRoleItem;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestRoles;
import net.ihe.gazelle.tm.gazelletest.model.instance.Status;
import net.ihe.gazelle.tm.gazelletest.model.instance.SystemAIPOResultForATestingSession;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.tee.model.TestInstanceExecutionStatusEnum;

import java.util.ArrayList;
import java.util.List;

public class SAPResultDetailTestRoleItemMock extends SAPResultDetailTestRoleItem {


    public SAPResultDetailTestRoleItemMock(TestRoles testRoles, SystemAIPOResultForATestingSession systemAIPOResult) {
        super(testRoles, systemAIPOResult);
    }

    @Override
    public void tryAddTestInstance(TestInstance testInstance) {
        Status status = testInstance.getLastStatus();
        TestInstanceExecutionStatusEnum execStatus = (null != testInstance.getExecutionStatus()) ? testInstance
                .getExecutionStatus().getKey() : null;
        List<TestInstance> list = instances.get(status);
        if (execStatus == null) {
            execStatus = TestInstanceExecutionStatusEnum.ACTIVE;
        }
        List<TestInstance> listByExecStat = instancesByExecutionStatus.get(execStatus);
        listByExecStat = new ArrayList<TestInstance>();
        instancesByExecutionStatus.put(execStatus, listByExecStat);

        listByExecStat.add(testInstance);
        if (list == null) {
            list = new ArrayList<TestInstance>();
            instances.put(status, list);
        }
        list.add(testInstance);
    }


}
