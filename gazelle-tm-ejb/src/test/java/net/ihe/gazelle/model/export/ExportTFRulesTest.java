package net.ihe.gazelle.model.export;

import net.ihe.gazelle.tf.action.converter.TFRuleToXmlConverter;
import net.ihe.gazelle.tf.model.Actor;
import net.ihe.gazelle.tf.model.IntegrationProfile;
import net.ihe.gazelle.tf.model.IntegrationProfileOption;
import net.ihe.gazelle.tf.model.constraints.AipoCriterion;
import net.ihe.gazelle.tf.model.constraints.AipoList;
import net.ihe.gazelle.tf.model.constraints.AipoRule;
import net.ihe.gazelle.tf.model.constraints.AipoSingle;
import net.ihe.gazelle.tf.model.constraints.export.AipoRulesIE;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static net.ihe.gazelle.files.FilesUtils.loadFile;

public class ExportTFRulesTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExportTFRulesTest.class);

    private List<AipoRule> aipoRules;
    private int idCounter;

    @Before
    public void setUp() throws Exception {

        this.aipoRules = new ArrayList<>();
        this.idCounter = 0;

        // Prepare a rule to export
        AipoRule aipoRule1 = new AipoRule();
        aipoRule1.setId(getId());
        aipoRule1.setComment("This is the first Rule of our list");
        aipoRule1.setCause(createTestAipo(aipoRule1));
        aipoRule1.setConsequence(createTestAipoListSimple(aipoRule1, false));

        aipoRules.add(aipoRule1);

        AipoRule aipoRule2 = new AipoRule();
        aipoRule2.setId(getId());
        aipoRule2.setComment("This is the Second Rule of our list");
        aipoRule2.setConsequence(createTestAipo(aipoRule2));
        aipoRule2.setCause(createTestAipoList(aipoRule2, false));

        aipoRules.add(aipoRule2);
    }

    private int getId(){
        return idCounter ++;
    }

    private AipoSingle createTestAipo(AipoRule aipoRule){
        AipoSingle aipoSingle = new AipoSingle();
        int id = getId();
        aipoSingle.setActor(new Actor("KWActor" + id, "NameActor" + id));
        aipoSingle.setIntegrationProfile(new IntegrationProfile(null,
                "KWIP" + id, "NameIP" + id, "Unused Description" + id));
        aipoSingle.setOption(new IntegrationProfileOption("KWOption" + id, "NameOption" + id,
                "Unused Description" + id));
        aipoSingle.setAipoRule(aipoRule);
        List<AipoSingle> aipoSingleList = aipoRule.getAipoRules();
        if (aipoSingleList == null){
            aipoSingleList = new ArrayList<>();
        }
        aipoSingleList.add(aipoSingle);
        aipoRule.setAipoRules(aipoSingleList);
        return aipoSingle;
    }

    private AipoList createTestAipoListSimple(AipoRule aipoRule, boolean or){
        AipoList aipoList = new AipoList();
        aipoList.setId(getId());
        aipoList.setOr(or);
        List<AipoCriterion> aipoCriterions = new ArrayList<>();
        aipoCriterions.add(createTestAipo(aipoRule));
        aipoCriterions.add(createTestAipo(aipoRule));
        aipoCriterions.add(createTestAipo(aipoRule));
        aipoList.setAipoCriterions(aipoCriterions);
        return aipoList;
    }

    private AipoList createTestAipoList(AipoRule aipoRule, boolean or){
        AipoList aipoList = new AipoList();
        aipoList.setId(getId());
        aipoList.setOr(or);
        List<AipoCriterion> aipoCriterions = new ArrayList<>();
        aipoCriterions.add(createTestAipo(aipoRule));
        aipoCriterions.add(createTestAipoListSimple(aipoRule, !or));
        aipoList.setAipoCriterions(aipoCriterions);
        return aipoList;
    }

    private boolean checkRule(AipoRule aipoRule, AipoRule expectedRule){

        if (aipoRule.getComment().equals(expectedRule)){
            Assert.fail("Imported Rule comment does not correspond to expected objects");
        }

        return (checkAipoCriterion(aipoRule.getCause(), expectedRule.getCause())
                && checkAipoCriterion(aipoRule.getConsequence(), expectedRule.getConsequence()));
    }

    private  boolean checkAipoCriterion(AipoCriterion aipoCriterion, AipoCriterion expectedCriterion){
        if (aipoCriterion instanceof AipoList && expectedCriterion instanceof AipoList){
            return checkAipoList((AipoList) aipoCriterion, (AipoList) expectedCriterion);
        } else if (aipoCriterion instanceof  AipoSingle && expectedCriterion instanceof AipoSingle){
            return checkAipoSingle((AipoSingle) aipoCriterion, (AipoSingle) expectedCriterion);
        }
        return false;
    }

    private boolean checkAipoList(AipoList aipoList, AipoList expectedList){

        boolean globalCheck = true;
        for (AipoCriterion aipoCriterion : aipoList.getAipoCriterions()){
            boolean criterionCheck = false;
            for (AipoCriterion expectedCriterion : expectedList.getAipoCriterions()){
                if (checkAipoCriterion(aipoCriterion, expectedCriterion)){
                    criterionCheck = true;
                }
            }
            if (!criterionCheck){
                globalCheck = false;
            }
        }
        return globalCheck;
    }

    private boolean checkAipoSingle(AipoSingle aipoSingle, AipoSingle expectedSingle){

        return ((aipoSingle.getActorKeyword().equals(expectedSingle.getActorKeyword()))
                && (aipoSingle.getIntegrationProfileKeyword().equals(expectedSingle.getIntegrationProfileKeyword()))
                && (aipoSingle.getOptionKeyword().equals(expectedSingle.getOptionKeyword()))
                );
    }

    @After
    public void tearDown() throws Exception {
        aipoRules = null;
    }

    @Test
    public void tfRulesToXmlTest() {
        TFRuleToXmlConverter tfRuleToXmlConverter = new TFRuleToXmlConverter();
        String xmlRules = null;
        try {
            xmlRules = tfRuleToXmlConverter.rulesToXml(aipoRules);
        } catch (JAXBException e){
            LOGGER.error("", e);
            Assert.fail("Error converting rules to xml : " + e.getMessage());
        }

        if (xmlRules != null && !xmlRules.isEmpty()){
            File file = loadFile("/model/xmlRules.xml");
            try {
                String expectedResult = new String(FileUtils.readFileToByteArray(file), StandardCharsets.UTF_8);
                Assert.assertEquals("Result of xml transformation does not match expected format",
                        expectedResult, xmlRules);
            }catch (IOException e){
                LOGGER.error("", e);
                Assert.fail("Error reading file with expected result : " + e.getMessage());
            }
        } else {
            Assert.fail("Error converting rules to xml : result should not be null nor empty");
        }
    }

    @Test
    public void xmlToTFRulesTest() {

        File file = loadFile("/model/xmlRules.xml");
        String fileToImport = null;
        boolean match;

        try {
            fileToImport = new String(FileUtils.readFileToByteArray(file), StandardCharsets.UTF_8);
        }catch (IOException e){
            LOGGER.error("", e);
            Assert.fail("Error reading file with xml rules : " + e.getMessage());
        }

        TFRuleToXmlConverter tfRuleToXmlConverter = new TFRuleToXmlConverter();
        AipoRulesIE importedAipoRules = null;
        try {
            importedAipoRules = tfRuleToXmlConverter.xmlToTFRules(fileToImport);
        } catch (JAXBException e){
            LOGGER.error("", e);
            Assert.fail("Error converting xml to rules : " + e.getMessage());
        }
        if (importedAipoRules != null && importedAipoRules.getAipoRules() != null && !importedAipoRules.getAipoRules().isEmpty()
                && importedAipoRules.getAipoRules().size()== 2 ){
            for (AipoRule aipoRule : importedAipoRules.getAipoRules()){
                match = false;
                for (AipoRule expectedRule : aipoRules){
                    if (checkRule(aipoRule, expectedRule)){
                       match = true;
                    }
                }
                if (!match){
                    Assert.fail("Imported Rule does not correspond to expected objects");
                }
            }
        } else {
            Assert.fail("Error converting xml to rules : result should not be null nor empty");
        }
    }
}
