package net.ihe.gazelle.model.export;

import net.ihe.gazelle.objects.model.*;
import net.ihe.gazelle.tf.action.converter.SamplesTypeToXmlConverter;
import net.ihe.gazelle.tf.model.*;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static net.ihe.gazelle.files.FilesUtils.loadFile;

public class ExportSamplesTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExportSamplesTest.class);

    private List<ObjectType> objectTypeList;
    private int idCounter;

    /**
     * Set up for tests.
     */
    @Before
    public void setUp() {

        this.objectTypeList = new ArrayList<>();
        this.idCounter = 0;

        // Prepare ObjectType to export
        this.objectTypeList.add(createObjectType());
        this.objectTypeList.add(createObjectType());
    }

    /**
     * Get a new id for fake entities.
     *
     * @return an integer.
     */
    private int getId() {
        return idCounter++;
    }

    /**
     * Create a fake Object Type.
     *
     * @return an {@link ObjectType}
     */
    private ObjectType createObjectType() {

        Integer id = getId();

        //ObjectTypeStatus
        ObjectTypeStatus inObjectTypeStatus = createObjectTypeStatus();
        ObjectType newObjectType = new ObjectType("keyword" + id, "description" + id, "default_desc" + id, "instructions" + id,
                inObjectTypeStatus);
        Set<ObjectType> objectTypes = new HashSet<ObjectType>();
        objectTypes.add(newObjectType);
        inObjectTypeStatus.setObjectTypes(objectTypes);

        //ObjectAttributes
        Set<ObjectAttribute> objectAttributes = new HashSet<>();
        objectAttributes.add(createObjectAttribute(newObjectType));
        newObjectType.setObjectAttributes(objectAttributes);


        //ObjectCreator
        Set<ObjectCreator> objectCreators = new HashSet<>();
        objectCreators.add(createObjectCreator(newObjectType));
        newObjectType.setObjectCreators(objectCreators);

        //ObjectReader
        Set<ObjectReader> objectReaders = new HashSet<>();
        objectReaders.add(createObjectReader(newObjectType));
        newObjectType.setObjectReaders(objectReaders);


        //ObjectFile
        Set<ObjectFile> objectFiles = new HashSet<>();
        objectFiles.add(createObjectFiles(newObjectType));
        newObjectType.setObjectFiles(objectFiles);

        return newObjectType;
    }

    /**
     * Create an Object Type Status
     *
     * @return an {@link ObjectTypeStatus}
     */
    private ObjectTypeStatus createObjectTypeStatus() {

        Integer id = getId();
        return new ObjectTypeStatus("keyword" + id, "labelToDisplay" + getId(), "description" + id);
    }

    /**
     * Create an Object Attribute.
     *
     * @param objectType Object Type possessing the attribute.
     * @return an {@link ObjectAttribute}
     */
    private ObjectAttribute createObjectAttribute(ObjectType objectType) {

        Integer id = getId();
        ObjectAttribute newObjectAttribute = new ObjectAttribute(objectType, "keyword" + id, "description" + id);


        Set<ObjectInstanceAttribute> objectInstanceAttributes = new HashSet<>();
        objectInstanceAttributes.add(new ObjectInstanceAttribute());
        return newObjectAttribute;
    }

    /**
     * Create an Object Creator.
     *
     * @param objectType Object Type with this creator.
     * @return an {@link ObjectCreator}
     */
    private ObjectCreator createObjectCreator(ObjectType objectType) {

        Integer id = getId();
        ObjectCreator objectCreator = new ObjectCreator("description" + id, createAIPO(), objectType);
        //Set AIPO ???
        return objectCreator;
    }

    /**
     * Create an Object Reader.
     *
     * @param objectType Object Type with this reader.
     * @return an {@link ObjectReader}
     */
    private ObjectReader createObjectReader(ObjectType objectType) {

        Integer id = getId();
        ObjectReader objectReader = new ObjectReader("description" + id, createAIPO(), objectType);
        return objectReader;
    }

    /**
     * Create an AIPO
     *
     * @return an {@link ActorIntegrationProfileOption}
     */
    private ActorIntegrationProfileOption createAIPO() {
        ActorIntegrationProfileOption aipo = new ActorIntegrationProfileOption();
        int aipoId = getId();

        aipo.setId(aipoId);
        aipo.setActorIntegrationProfile(createAIP());
        aipo.setIntegrationProfileOption(new IntegrationProfileOption("KWOption" + aipoId,
                "NameOption" + aipoId, "Unused Description" + aipoId));
        aipo.setMaybeSupportive(true);
        return aipo;
    }

    /**
     * Create an AIP
     *
     * @return an {@link ActorIntegrationProfile}
     */
    private ActorIntegrationProfile createAIP() {
        ActorIntegrationProfile aip = new ActorIntegrationProfile();
        int aipId = getId();

        aip.setId(aipId);
        aip.setActor(new Actor("KWActor" + aipId, "NameActor" + aipId));
        aip.setIntegrationProfile(new IntegrationProfile(null,
                "KWIP" + aipId, "NameIP" + aipId, "Unused Description" + aipId));
        List<ProfileLink> profileLinks = new ArrayList<>();
        profileLinks.add(new ProfileLink());
        return aip;
    }

    /**
     * Create an Object File.
     *
     * @param objectType Object Type with this file.
     * @return an {@link ObjectFile}
     */
    private ObjectFile createObjectFiles(ObjectType objectType) {

        Integer id = getId();
        ObjectFile newObjectFile = new ObjectFile(objectType, "description" + id, createObjectFileType(), 0, 2, "tonton" + id);

        Set<ObjectFile> objectFiles = new HashSet<>();
        objectFiles.add(newObjectFile);
        newObjectFile.getType().setObjectFiles(objectFiles);
        return newObjectFile;
    }

    /**
     * Create an ObjectFileType.
     *
     * @return an {@link ObjectFileType}
     */
    private ObjectFileType createObjectFileType() {
        return new ObjectFileType("PDF", "NOT USED", ".pdf", false, true);
    }

    /**
     * Tear down method.
     */
    @After
    public void tearDown() {
        objectTypeList = null;
    }

    /**
     * Test converting ObjectType to XML.
     */
    @Test
    public void samplesTypeToXmlTest() {
        SamplesTypeToXmlConverter samplesTypeToXmlConverter = new SamplesTypeToXmlConverter();
        String xmlSamplesType = null;
        try {
            xmlSamplesType = samplesTypeToXmlConverter.samplesTypeToXml(this.objectTypeList);
        } catch (JAXBException e) {
            LOGGER.error("", e);
            Assert.fail("Error converting Samples' Type to xml : " + e.getMessage());
        }

        if (xmlSamplesType != null && !xmlSamplesType.isEmpty()) {
            File file = loadFile("/model/samplesType.xml");
            try {
                String expectedResult = new String(FileUtils.readFileToByteArray(file), StandardCharsets.UTF_8);
                Assert.assertEquals("Result of xml transformation does not match expected format",
                        expectedResult, xmlSamplesType);
            } catch (IOException e) {
                LOGGER.error("", e);
                Assert.fail("Error reading file with expected result : " + e.getMessage());
            }
        } else {
            Assert.fail("Error converting Samples' Type to xml : result should not be null nor empty");
        }
    }

    /**
     * Test converting XML to Object Types.
     */
    @Test
    public void xmlToObjectTypesTest() {

        File file = loadFile("/model/samplesType.xml");
        String fileToImport = null;
        boolean match;
        try {
            fileToImport = new String(FileUtils.readFileToByteArray(file), StandardCharsets.UTF_8);
        } catch (IOException e) {
            LOGGER.error("", e);
            Assert.fail("Error reading file with xml Samples Types : " + e.getMessage());
        }

        SamplesTypeToXmlConverter samplesTypeToXmlConverter = new SamplesTypeToXmlConverter();
        ObjectTypeIE importedSamplesType = null;
        try {
            importedSamplesType = samplesTypeToXmlConverter.xmlToSamplesType(fileToImport);
        } catch (JAXBException e) {
            LOGGER.error("", e);
            Assert.fail("Error converting xml to rules : " + e.getMessage());
        }
        if (!(importedSamplesType != null && importedSamplesType.getObjectTypes() != null && !importedSamplesType.getObjectTypes().isEmpty()
                && importedSamplesType.getObjectTypes().size() == 2)) {
            Assert.fail("Error converting xml to standard : result should not be null nor empty");
        }
    }

}
