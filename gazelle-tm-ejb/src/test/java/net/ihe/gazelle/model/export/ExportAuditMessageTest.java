package net.ihe.gazelle.model.export;

import net.ihe.gazelle.tf.auditMessage.converter.AuditMessageToXmlConverter;
import net.ihe.gazelle.tf.model.*;
import net.ihe.gazelle.tf.model.auditMessage.AuditMessage;
import net.ihe.gazelle.tf.model.auditMessage.TriggerEvent;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static net.ihe.gazelle.files.FilesUtils.loadFile;

public class ExportAuditMessageTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExportAuditMessageTest.class);

    private List<AuditMessage> auditMessages;
    private int idCounter;

    private int getId(){
        return idCounter ++;
    }

    @Before
    public void setUp() throws Exception {

        this.auditMessages = new ArrayList<>();
        this.idCounter = 0;

        auditMessages.add(createAuditMessage());
        auditMessages.add(createAuditMessage());
    }

    private AuditMessage createAuditMessage(){

        AuditMessage auditMessage = new AuditMessage();
        int id = getId();

        auditMessage.setId(id);
        auditMessage.setAuditedEvent(TriggerEvent.PATIENT_RECORD_EVENT);
        auditMessage.setOid("OID"+id);
        auditMessage.setAuditedTransaction(new Transaction("TKW" + id, "TName" + id , "TDesc" + id));
        auditMessage.setIssuingActor(new Actor("AKW" + id, "AName" + id));
        auditMessage.setDocumentSection(createDocumentSection());
        auditMessage.setComment("Comment" + id);
        auditMessage.setEventCodeType("ECT" + id);
        return auditMessage;
    }

    private DocumentSection createDocumentSection(){
        DocumentSection documentSection = new DocumentSection();
        int id = getId();
        documentSection.setId(id);
        documentSection.setSection("Section" + id);
        documentSection.setDocument(createDocument());
        documentSection.setType(DocumentSectionType.TITLE);
        return documentSection;
    }

    private Document createDocument(){
        Document document = new Document();
        int id = getId();

        document.setDocument_md5_hash_code("Hash"+id);
        return document;
    }

    @After
    public void tearDown() throws Exception {
        auditMessages = null;
    }

    @Test
    public void tfRulesToXmlTest() {
        AuditMessageToXmlConverter auditMessageToXmlConverter = new AuditMessageToXmlConverter();
        String xmlAuditMessage = null;
        try {
            xmlAuditMessage = auditMessageToXmlConverter.auditMessagesToXml(auditMessages);
        } catch (JAXBException e){
            LOGGER.error("", e);
            Assert.fail("Error converting rules to xml : " + e.getMessage());
        }

        if (xmlAuditMessage != null && !xmlAuditMessage.isEmpty()){
            File file = loadFile("/model/auditMessage.xml");
            try {
                String expectedResult = new String(FileUtils.readFileToByteArray(file), StandardCharsets.UTF_8);
                Assert.assertEquals("Result of xml transformation does not match expected format",
                        expectedResult, xmlAuditMessage);
            }catch (IOException e){
                LOGGER.error("", e);
                Assert.fail("Error reading file with expected result : " + e.getMessage());
            }
        } else {
            Assert.fail("Error converting rules to xml : result should not be null nor empty");
        }
    }
}
